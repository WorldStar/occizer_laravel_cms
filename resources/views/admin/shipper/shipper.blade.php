@extends('admin.layouts.default')

{{-- Page title --}}
@section('title')
    Shippers
    @parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <meta name="_token" content="{!! csrf_token() !!}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/select2/css/select2.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/select2/css/select2-bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css"
          href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css"
          href="{{ asset('assets/vendors/datatables/css/colReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css"
          href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css"
          href="{{ asset('assets/vendors/datatables/css/rowReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/scroller.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/pages/tables.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/shipper.css') }}">
@stop

{{-- Page content --}}
@section('content')

    <section class="content-header">
        <h1>Shippers</h1>
       	<ol class="breadcrumb">
            <li>
                <a href="{{ route('dashboard') }}">
                    <i class="livicon" data-name="home" data-size="14" data-loop="true"></i>
                    Dashboard
                </a>
            </li>
            <li class="active">shippers</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Second Data Table -->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="panel panel-danger table-edit">
                    
					<div class="panel-heading">
                        <h3 class="panel-title">
                                    <span class="title-font">
                                     <i class="livicon" data-name="car" data-size="15" data-c="#53FF22" data-hc="#53FF22"
                                        data-loop="true"></i>
                                    Shipper List</span>
                        </h3>
                    </div>
					
                    <div class="panel-body"> 
                        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>

                            <table class="table table-striped table-bordered table-hover dataTable no-footer sample_editable"
                                   id="shipper_table" role="grid">
                                <thead>
                                    <tr role="row">
                                        <th class="sorting_asc td-align" tabindex="0" aria-controls="shipper_table" rowspan="1"
                                            colspan="1" aria-label="
                                                ID
                                            : activate to sort column ascending">ID
                                        </th>
                                        <th class="sorting_asc td-align" tabindex="0" aria-controls="shipper_table" rowspan="1"
                                            colspan="1" aria-label="
                                                Name
                                            : activate to sort column ascending">Name
                                        </th>
                                        <th class="sorting td-align" tabindex="0" aria-controls="shipper_table" rowspan="1"
                                            colspan="1" aria-label="
                                               Email
                                            : activate to sort column ascending">Email
                                        </th>
                                        <th class="sorting td-align" tabindex="0" aria-controls="shipper_table" rowspan="1"
                                            colspan="1" aria-label="
                                               Phone Number
                                            : activate to sort column ascending">Phone Number
                                        </th>
                                        <th class="sorting td-align" tabindex="0" aria-controls="shipper_table" rowspan="1"
                                            colspan="1" aria-label="
                                                Contact Name
                                            : activate to sort column ascending">Contact Name
                                        </th>
                                        <th class="sorting td-align" tabindex="0" aria-controls="shipper_table" rowspan="1"
                                            colspan="1" aria-label="
                                                City
                                            : activate to sort column ascending">City
                                        </th>
                                        <?php if($_SESSION['userrole'] == 1){ ?>
                                            <th class="sorting td-align" tabindex="0" aria-controls="shipper_table" rowspan="1"
                                                colspan="1" aria-label="
                                                    status
                                                : activate to sort column ascending">Status
                                            </th>
                                            <th class="sorting del-field td-align" tabindex="0" aria-controls="shipper_table" rowspan="1"
                                                colspan="1" aria-label="
                                                    Delete
                                                : activate to sort column ascending">Delete
                                            </th>
                                        <?php }?>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                        <!-- END EXAMPLE TABLE PORTLET-->
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- content -->

    <!-- add modal -->
	<div class="modal fade" id="add_Modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content modal-size">
				<div class="modal-header modal-header-size">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel">Add Shipper </h4>
				</div>
				<div class="modal-body modal-body-size">
					<table class="table form-body modal-table-width" border="0">
						<tr class="th-border">
							<th class="th-border">Name:</th>
							<td class="th-border">
                                <input type="text" name="name" class="form-control" id="name"  value="" placeholder="name">
							</td>
						</tr>
						<tr>
							<th class="th-border">Email:</th>
							<td class="th-border">
                                <input  type="email" class="form-control" id="email"  value="" placeholder="example@example.com" required>
                                {{--<input type="email" name="email" class="form-control" pattern="^\w+([.-]?\w+)*@\w+([.-]?\w+)*(.\w{2,3})+$">--}}
							</td>
						</tr>
                        <tr>
                            <th class="th-border">Phone Number:</th>
                            <td class="th-border">
                                <input type="tel" name="phone_number" class="form-control" id="phone_number"  value="" placeholder="phone number">
                            </td>
                        </tr>
                        <tr>
                            <th class="th-border">Contact Name:</th>
                            <td class="th-border">
                                <input type="text" name="contact_name" class="form-control" id="contact_name"  value="" placeholder="contact name">
                            </td>
                        </tr>
                        <tr>
                            <th class="th-border">City:</th>
                            <td class="th-border">
                                <select class="form-control" id="city" name="city[]" multiple="multiple">
                                    <?php
                                    foreach($cities as $city){
                                        echo '<option value="'.$city->id.'">'.$city->name.'</option>';
                                    }
                                    ?>
                                </select>
                             </td>
                        </tr>
                    </table>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" class="form-control" data-dismiss="modal" onclick="add_shipper()">Add</button>
                    <button type="button" class="btn btn-default" class="form-control" data-dismiss="modal">Cancel</button>
				</div>
			</div>
		</div>
	</div>

    <!-- delete modal -->
    <div class="modal fade" id="delete_Modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content modal-size">
                    <div class="modal-body modal-header-size">
                        Notice!!!
                    </div>
                    <input type="hidden" id="delete_id" value="0">
                    <div class="modal-body">
                        Do you really want to delete this shipper?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" class="form-control" data-dismiss="modal" onclick="delete_shipper()">Delete</button>
                        <button type="button" class="btn btn-default" class="form-control" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
    </div>

    <!-- Message modal -->
    <div class="modal fade" id="message_Modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content modal-size">
                <div class="modal-body modal-header-size">
                    Notice!!!
                </div>
                <div class="modal-body" id="message_content">
                    Please input a shipper name.
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" class="form-control" data-dismiss="modal">Ok</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="active_Modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content modal-size">
                <div class="modal-body modal-header-size">
                    Notice!!!
                </div>
                <input type="hidden" id="shipper_id" value="0">
                <div class="modal-body" id="status_content">
                    Do you really want to active this shipper?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" class="form-control" data-dismiss="modal" onclick="active_shipper()">Ok</button>
                    <button type="button" class="btn btn-default" class="form-control" data-dismiss="modal">No</button>
                </div>
            </div>
        </div>
    </div>
    {{-- inactive product modal--}}
    <div class="modal fade" id="inactive_Modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content modal-size">
                <div class="modal-body modal-header-size">
                    Notice!!!
                </div>
                <input type="hidden" id="occ_id" value="0">
                <div class="modal-body" id="statua_content">
                    Do you really want to inactive this shipper?
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" class="form-control" data-dismiss="modal" onclick="inactive_shipper()">Ok</button>
                    <button type="button" class="btn btn-default" class="form-control" data-dismiss="modal">No</button>
                </div>
            </div>
        </div>
    </div>


@stop

{{-- page level scripts --}}
@section('footer_scripts')

    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.buttons.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.colReorder.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.responsive.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.rowReorder.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.colVis.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.html5.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.print.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.print.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/pdfmake.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/vfs_fonts.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.scroller.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/select2/js/select2.js') }}"></script>
    {{--<script type="text/javascript" src="{{ asset('assets/js/pages/table-editable.js') }}" ></script>--}}

    <script>
        /* display table by using Datatable method  */
        var table = null;
		$(function () {
			var nEditing = null;
            table = $('#shipper_table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! route("admin.shippers.data") !!}',
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'name', name: 'name'},
                    {data: 'email', name: 'email'},
                    {data: 'phone_number', name: 'phone_number'},
                    {data: 'contact_name', name: 'contact_name'},
                    {data: 'city_id', name: 'city_id'},
                    <?php if($_SESSION['userrole'] ==1){?>
                    {data: 'status', name: 'status'},
                    {data: 'delete', name: 'delete', orderable: false, searchable: false},
                    <?php }?>
                ]
            });
            table.on('draw', function () {
                $('.livicon').each(function () {
                    $(this).updateLivicon();
                });
            });

            /*  Cancel functionality in row */
            function restoreRow(table, nRow) {
                var aData = table.row(nRow).data();
                var jqTds = $('>td', nRow);

                for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
                    table.cell().data(aData[i], nRow, i, false);
                }
                table.draw(false);
            }
			/*  in Javascript add HTML(button): append('-')  */
                <?php if($_SESSION['userrole'] ==1) {?>
                $('#shipper_table_length').append('<button type="button" class="form-control button-sm add-btn" data-toggle="modal" data-target="#add_Modal">Add</button >');
                <?php }?>

            /*  When clicked on Delete button   */
            table.on('click', '.delete', function (e) {
                e.preventDefault();
                var nRow = $(this).parents('tr')[0];
                var aData = table.row(nRow).data();
                var jqTds = $('>td', nRow);
                var id = aData.id;
                console.log(id);
                $('#delete_id').val(id);
                $('#delete_Modal').modal('show');
            });

            /*
             When clicked on active button
             */
            table.on('click', '.active', function (e) {
                e.preventDefault();
                var nRow = $(this).parents('tr')[0];
                var aData = table.row(nRow).data();
                var jqTds = $('>td', nRow);
                var id = aData.id;
                console.log(id);
                $('#shipper_id').val(id);
                $('#active_Modal').modal('show');
            })

            /*
             When clicked on inactive button
             */
            table.on('click', '.inactive', function (e) {
                e.preventDefault();
                var nRow = $(this).parents('tr')[0];
                var aData = table.row(nRow).data();
                var jqTds = $('>td', nRow);
                var id = aData.id;
                console.log(id);
                /*  get row id to delete */
                $('#shipper_id').val(id);
                /*  display deletemodal */
                $('#inactive_Modal').modal('show');
            })
        });
    </script>

    /*  add and delete occasion */
    <script>
        //delete shipper
        function delete_shipper(){
            var id = $('#delete_id').val();
            $.ajax({
                type: "get",
                url: '/admin/shipper/' + id + '/delete',
                success: function (result) {
                    $('#message_content').html('Successfully deleted.');
                    $('#message_Modal').modal('show');
                    location.reload();
                },
                error: function (result) {
                    console.log(result)
                }
            });
        }

  /*   add shipper */
        function add_shipper () {
            var name = $("#name").val();
            var email = $("#email").val();
            var phone_number = $("#phone_number").val();
            var contact_name = $("#contact_name").val();
            var city = $("#city").val();

            if (name == "") {
                $('#message_content').html('Please input shipper name.');
                $('#message_Modal').modal('show');
                return;
            }

            if (email == "") {
                $('#message_content').html('Please input email.');
                $('#message_Modal').modal('show');
                return;
            }else{
                var regExp = /^[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*\.[a-zA-Z]{2,3}$/i;
                if ( !regExp.test( email) ) {
                    $('#message_content').html('Please input email as type of "example@example.com".');
                    $('#message_Modal').modal('show');
                    return;
                }
            }

            if (phone_number == "") {
                $('#message_content').html('Please input phone_number.');
                $('#message_Modal').modal('show');
                return;
            }else{
                var regExp = /^\d{2,3}-\d{3,4}-\d{4}$/;
                if ( !regExp.test(phone_number)) {
                    $('#message_content').html('Please input phone number correctly.');
                    $('#message_Modal').modal('show');
                    return;
                }
            }

            if (contact_name == "") {
                $('#message_content').html('Please input contact_name.');
                $('#message_Modal').modal('show');
                return;
            }

            if (city == "") {
                $('#message_content').html('Please select city.');
                $('#message_Modal').modal('show');
                return;
            }

            var data = {
                name: name,
                email: email,
                phone_number: phone_number,
                contact_name: contact_name,
                city: city,
                _token:$('meta[name=_token]').attr('content')
            };

            $.ajax({
                type: "get",
                url: '/admin/shipper/add_shipper',
                data: data,
                success: function (result) {
                    if(result == 1){
                        $('#message_content').html('This email was already in used.');
                        $('#message_Modal').modal('show');
                    } else {
                        table.draw(false);
                    }
                },
                error: function (result) {
                    console.log(result)
                }
            });
        }


        /* active and inactive shipper */
        function active_shipper(){
            var id = $('#shipper_id').val();
            // console.log(id);
            $.ajax({
                type: "get",
                url: '/admin/shipper/' + id + '/active_shipper',
                success: function (result) {
                    console.log('row ' + result + ' actived');
                    location.reload();
                },
                error: function (result) {
                    console.log(result)
                }
            });
        }

        function inactive_shipper(){
            var id = $('#shipper_id').val();
            console.log(id);

            $.ajax({
                type: "get",
                url: '/admin/shipper/' + id + '/inactive_shipper',
                success: function (result) {
                    console.log('row ' + result + ' inactived');
                    location.reload();
                },
                error: function (result) {
                    console.log(result)
                }
            });
        }
    </script>

@stop
