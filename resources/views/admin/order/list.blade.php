@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
Orders
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/colReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/rowReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/scroller.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/pages/tables.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/order.css') }}">
@stop

{{-- Page content --}}
@section('content')

    <section class="content-header">
        <h1>Orders</h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{ route('dashboard') }}">
                    <i class="livicon" data-name="home" data-size="14" data-loop="true"></i>
                    Dashboard
                </a>
            </li>
            <li class="active" >orders</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Second Data Table -->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="panel panel-danger table-edit">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                                    <span class="title-font">
                                        <i class="livicon" data-name="lab" data-c="#FBFF34" data-hc="#FBFF34" data-size="18" data-loop="true"></i>
                                        Order List
                                    </span>
                        </h3>
                    </div>
                    <div class="panel-body">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                        <div id="sample_editable_1_wrapper" class="">
                            <table class="table table-striped table-bordered table-hover dataTable no-footer sample_editable"
                                   id="order_table" role="grid">
                                <thead>
                                <tr role="row">
                                    <th class="sorting_asc th-align" tabindex="0" aria-controls="order_table" rowspan="1"
                                        colspan="1"  aria-label="
                                                   Product List
                                            : activate to sort column ascending"> Product List
                                    </th>
                                    <th class="sorting_asc th-align" style="align:center; valign:middle;" tabindex="0" aria-controls="order_table" rowspan="1"
                                        colspan="1"  aria-label="
                                                   Order ID
                                            : activate to sort column ascending">Order ID
                                    </th>
                                    <th class="sorting th-align" tabindex="0" aria-controls="order_table" rowspan="1"
                                        colspan="1" aria-label="
                                                Customer
                                            : activate to sort column ascending">Customer
                                    </th>
                                    <th class="sorting_asc th-align" tabindex="0" aria-controls="order_table" rowspan="1"
                                        colspan="1"  aria-label="
                                                   Total Price
                                            : activate to sort column ascending">Total Price
                                    </th>

                                    <?php if($_SESSION['userrole'] == 1){?>
                                    <th class="sorting th-align" tabindex="0" aria-controls="order_table" rowspan="1"
                                        colspan="1" aria-label="
                                                Vendor
                                            : activate to sort column ascending">Vendor
                                    </th>
                                    <th class="sorting th-align" tabindex="0" aria-controls="order_table" rowspan="1"
                                        colspan="1" aria-label="
                                               Shipper
                                            : activate to sort column ascending">Shipper
                                    </th>
                                    <?php }?>

                                    <th class="sorting_asc th-align" tabindex="0" aria-controls="order_table" rowspan="1"
                                        colspan="1"  aria-label="
                                                   Created Date
                                            : activate to sort column ascending">Created Date
                                    </th>
                                    <th class="sorting th-align" tabindex="0" aria-controls="order_table" rowspan="1"
                                        colspan="1" aria-label="
                                                Payment Method
                                            : activate to sort column ascending">Payment Method
                                    </th>
                                    <th class="sorting th-align" tabindex="0" aria-controls="order_table" rowspan="1"
                                        colspan="1" aria-label="
                                                Delivery Location
                                            : activate to sort column ascending">Delivery Location
                                    </th>
                                    <th class="sorting del-field th-align" tabindex="0" aria-controls="order_table" rowspan="1"
                                        colspan="1" aria-label="
                                                 Cancel
                                            : activate to sort column ascending">
                                    </th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                        <!-- END EXAMPLE TABLE PORTLET-->
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- content -->

    <!-- cancel modal -->
    <div class="modal fade" id="cancelModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content modal-size">
                <div class="modal-body modal-header-size">
                    Notice!!!
                </div>
                <input type="hidden" id="orderid" value="0">
                <div class="modal-body modal-body-size" id="modalcontent">
                    Do you really want to cancel this order?
                </div>
                <div class="modal-footer">
                    <input type="hidden" id="modal_order_id" value=""/>
                    <button type="button" class="btn btn-default" class="form-control" data-dismiss="modal" onclick="cancelOrder()">Ok</button>
                    <button type="button" class="btn btn-default" class="form-control" data-dismiss="modal">No</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="messageModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content modal-size">
                <div class="modal-body modal-header-size">
                    Notice!!!
                </div>
                <div class="modal-body modal-body-size" id="messagecontent">
                    This order of shopping cart cancelled.
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" class="form-control" data-dismiss="modal">OK</button>
                </div>
            </div>
        </div>
    </div>
@stop

{{-- page level scripts --}}
@section('footer_scripts')

    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.buttons.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.colReorder.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.responsive.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.rowReorder.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.colVis.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.html5.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.print.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.print.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/pdfmake.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/vfs_fonts.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.scroller.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/select2/js/select2.js') }}"></script>
    {{--<script type="text/javascript" src="{{ asset('assets/js/pages/table-editable.js') }}" ></script>--}}

    <script>
        // display table by using Datatable method
        var table = null;
        $(function () {
            var nEditing = null;
            var table = $('#order_table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! url("/admin/orders/get") !!}',
                columns: [
                    {data: 'productlist', name: 'productlist'},
                    {data: 'order_id', name: 'order_id'},
                    {data: 'customer_id', name: 'customer_id'},
                    {data: 'totalprice', name: 'totalprice'},
                    <?php if($_SESSION['userrole'] == 1){?>
                    {data: 'vendor_id', name: 'vendor_id'},
                    {data: 'shipper_id', name: 'shipper_id'},
                    <?php }?>
                    {data: 'created_at', name: 'created_at'},
                    {data: 'payment_method', name: 'payment_method'},
                    {data: 'delivery_location', name: 'delivery_location'},
                    {data: 'status', name: 'status', orderable: false, searchable: false}
                ]
            });

            table.on('draw', function () {
                console.log('asaa');
                $('.livicon').each(function () {
                    $(this).updateLivicon();
                });
            });
        })

        //cancel order
        function cancelModal(id){
            $("#modal_order_id").val(id);
            $('#cancelModal').modal('show');
            console.log(id);
        }

        function cancelOrder() {
            var id = $("#modal_order_id").val();
            var data = {
                id:id
            };
            $.ajax({
                type: "get",
                url: '/admin/order/'+id+'/cancel',
                data: data,
                success: function (result) {
                    $('#messageModal').modal('show');
                    console.log(result);
                    location.reload();
                },
                error: function (result) {
                }
            });
        }
    </script>
@stop
