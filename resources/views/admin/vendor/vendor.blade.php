@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
Vendors
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/colReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/rowReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/scroller.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/pages/tables.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/vendor.css') }}">
@stop

{{-- Page content --}}
@section('content')

<section class="content-header">
                <!--section starts-->
                <h1>Vendors</h1>
                <ol class="breadcrumb">
                    <li>
                        <a href="{{ route('dashboard') }}">
                            <i class="livicon" data-name="home" data-size="14" data-loop="true"></i>
                            Dashboard
                        </a>
                    </li>
                    <li>
                        <a href="#">vendors</a>
                    </li>
                    <li class="active">vendor list</li>
                </ol>
            </section>
            <!--section ends-->
            <section class="content">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-primary filterable">
                            <div class="panel-heading clearfix  ">
                                <div class="panel-title pull-left">
                                    <div class="caption title-font">
                                           <i class="livicon" data-name="user-flag" data-c="#FF5722" data-hc="#FF5722" data-size="14"
                                              data-loop="true"></i>
                                            Vendor List
                                    </div>
                                </div>
                            </div>
                            <div class="panel-body table-responsive">
                                <table class="table table-striped table-bordered th-align" id="table1">
                                    <thead>
                                        <tr>
                                            <th class="td-align">No</th>
                                            <th class="td-align">Photo</th>
                                            <th class="td-align">First Name</th>
                                            <th class="td-align">Last Name</th>
                                            <th class="td-align">Email </th>
                                            <th class="td-align">Country</th>
                                            <th class="td-align">City</th>
                                            <th class="td-align">Address</th>
                                            <th class="td-align">Facebook ID</th>
                                            <th class="td-align">Shipper</th>
                                            <th class="td-align">Delivery Cost</th>
                                            <th class="td-align">&nbsp;</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $i = 0;
                                    foreach($vendors as $vendor){
                                        $i++;

                                        $added_date = date('M d, Y', strtotime($vendor->created_at));

                                        $adress = $vendor->address;

                                        $country = DB::table('occ_countries')->where('id', $vendor->country_id)->first();
                                        $country_name = $country->name;

                                        $currency = $country->currency;
                                        $delivery_cost = ($vendor->delivery_cost).''.$currency;

                                        $city1 = explode(',', $vendor->city_id);
                                        $city = '';
                                        for($j = 0; $j < count($city1); $j++){
                                            $city2 = DB::table('occ_cities')->where('id', $city1[$j])->first();
                                            if($j == 0){
                                                $city = $city2->name;
                                            }else{
                                                $city .= ','.$city2->name;
                                            }
                                        }

                                        if( ($vendor->shipper_id) == 0){
                                            $shipper = 'Vendor Delivers.';
                                        }else{
                                            $shipper = DB::table('occ_shippers')->where('id', $vendor->shipper_id)->first();
                                            $shipper = $shipper->name;
                                        }

                                        $status = 'Active';
                                        if($vendor->status == 1){
                                            $status = 'Inactive';
                                        }

                                        $photo = $vendor->pic;
                                        if($vendor->pic == ''){
                                            $photo = 'default.png';
                                        }
                                    ?>

                                    <tr>
                                        <td>{{ $i }}</td>
                                        <td class="td-cursor" data-toggle="modal" data-target="#{{ $vendor->id }}">
                                            <img src="/uploads/users/{{ $photo  }}" class="photo-size" >
                                        </td>

                                        <div class="modal fade" id="{{ $vendor->id }}" role="dialog">
                                            <div class="modal-dialog vendor-photo-modal">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">&times;  </button>
                                                        <h4 class="modal-title">{{ $vendor->first_name }} {{ $vendor->last_name }}</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <img src="/uploads/users/{{ $photo }}" class="modal-photo">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <td class="td-cursor" onclick="vendorProfile('{{ $vendor->id }}')">{{ $vendor->first_name }}</td>
                                        <td class="td-cursor" onclick="vendorProfile('{{ $vendor->id }}')">{{ $vendor->last_name }}</td>
                                        <td>{{ $vendor->email }}</td>
                                        <td>{{ $country_name }}</td>
                                        <td>{{ $city }}</td>
                                        <td>{{ $adress }}</td>
                                        <td>{{ $vendor->fb_id }}</td>
                                        <td>{{ $shipper }}</td>
                                        <td>{{ $delivery_cost }}</td>
                                        <td class="ianctive_button" onclick="vendorInactive('{{ $vendor->id }}', '{{ $vendor->status }}')">{{ $status }}</td>
                                    </tr>
                                    <?php
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

<!-- Message modal -->
<div class="modal fade" id="vendorProfileModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content modal-size">
            <div class="modal-header modal-header-size">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Vendor Profile </h4>
            </div>
            <div class="modal-body">
                <div class="form-group has-success modal-items">
                    <label class="control-label" for="first_name">First Name</label>
                    <span type="text-align" class="form-control" id="first_name"></span>
                </div>
                <div class="form-group has-success modal-items">
                    <label class="control-label" for="last_name">Last Name</label>
                    <span type="text-align" class="form-control" id="last_name"></span>
                </div>
                <div class="form-group has-success modal-items">
                    <label class="control-label" for="email">Email</label>
                    <span type="text-align" class="form-control" id="email"></span>
                </div>
                <div class="form-group has-success modal-items">
                    <label class="control-label" for="gender">Gender</label>
                    <span type="text-align" class="form-control" id="gender"></span>
                </div>
                <div class="form-group has-success modal-items">
                    <label class="control-label" for="birth">Birthday</label>
                    <span type="text-align" class="form-control" id="birth"></span>
                </div>
                <div class="form-group has-success modal-items">
                    <label class="control-label" for="postal"> Postal Code</label>
                    <span type="text-align" class="form-control" id="postal"></span>
                </div>
                <div class="form-group has-success modal-items">
                    <label class="control-label" for="address">Company</label>
                    <span type="text-align" class="form-control" id="company"></span>
                </div>
                <div class="form-group has-success modal-items">
                    <label class="control-label" for="fb_id">Facebook ID</label>
                    <span type="text-align" class="form-control" id="fb_id"></span>
                </div>
                <div class="form-group has-success modal-items">
                    <label class="control-label" for="email2">Contact Email</label>
                    <span type="text-align" class="form-control" id="email2"></span>
                </div>
                <div class="form-group has-success modal-items">
                    <label class="control-label" for="phone">Contact Phone</label>
                    <span type="text-align" class="form-control" id="phone"></span>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- delete modal -->
<div class="modal fade" id="inactiveModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content modal-size">
            <div class="modal-body modal-header-size">
                Notice!!!
            </div>
            <input type="hidden" id="vendorid" value="0">
            <input type="hidden" id="status" value="0">
            <div class="modal-body" id="inactivecontent">
                Do you really want to in-active this vendor?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" class="form-control" data-dismiss="modal" onclick="inactiveProc()">Yes</button>
                <button type="button" class="btn btn-default" class="form-control" data-dismiss="modal">No</button>
            </div>
        </div>
    </div>
</div>

<!-- Message modal -->
<div class="modal fade" id="messageModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content modal-size">
            <div class="modal-body modal-header-size">
                Notice!!!
            </div>
            <div class="modal-body" id="cancelcontent">
                Successfully in-actived the vendor.
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" class="form-control" data-dismiss="modal">Ok</button>
            </div>
        </div>
    </div>
</div>

@stop

{{-- page level scripts --}}
@section('footer_scripts')

    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.buttons.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.colReorder.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.responsive.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.rowReorder.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.colVis.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.html5.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.print.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.bootstrap.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.print.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/pdfmake.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/vfs_fonts.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.scroller.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/table-advanced.js') }}" ></script>
    <script>
        function vendorProfile(id){
            $.ajax({
                type: "get",
                url: '/admin/vendor/get/' + id,
                success: function (result) {
                    var data = JSON.parse(result);
                    $('#first_name').html(data.first_name);
                    $('#last_name').html(data.last_name);
                    $('#email').html(data.email);
                    $('#gender').html(data.gender);
                    $('#birth').html(data.dob);
                    $('#company').html(data.address);
                    $('#fb_id').html(data.fb_id);
                    $('#email2').html(data.email2);
                    $('#postal').html(data.postal);
                    $('#phone').html(data.contactno);

                    $('#vendorProfileModal').modal('show');
                },
                error: function (result) {
                    console.log(result)
                }
            });
        }
        function vendorInactive(id, status){
            $('#vendorid').val(id);
            $('#status').val(status);
            console.log(status);
            if(status == 1){
                $('#inactivecontent').html("Do you really want to in-active this vendor?");
            }else{
                $('#inactivecontent').html("Do you really want to active this vendor?");
            }
            $("#inactiveModal").modal('show');
        }
        function inactiveProc(){
            var vendorid = $('#vendorid').val();
            var status = $('#status').val();
            $.ajax({
                type: "get",
                url: '/admin/vendor/inactive/' + vendorid+'/'+status,
                success: function (result) {
                    console.log(result);
                    console.log(result.status);
                    if(result.status == 0){
                        $('#messagecontent').html('Successfully in-actived the vendor.');
                    }else{
                        $('#messagecontent').html('Successfully actived the vendor.');
                    }
                    $('#messageModal').modal('show');
                    location.reload();
                },
                error: function (result) {
                    console.log(result);
                }
            });
        }
    </script>

@stop
