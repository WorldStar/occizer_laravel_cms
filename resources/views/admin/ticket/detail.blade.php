@extends('admin/layouts/default')
{{-- Page title --}}
@section('title')
    Ticket Details
    @parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/colReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/rowReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/scroller.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/pages/tables.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/ticket.css') }}">
@stop

{{-- Page content --}}
@section('content')

    <section class="content-header">
        <!--section starts-->
        <h1>Details</h1>
        <ol class="breadcrumb">
            <li>
                <a href="">
                    <i class="livicon" data-name="home" data-size="14" data-loop="true"></i>
                    Dashboard
                </a>
            </li>
            <li>
                <a href="#">tickets</a>
            </li>
        </ol>
    </section>

    {{-- order information--}}
    <div class="panel-body customer-style">

        <form role="form"  enctype="multipart/form-data">
            <div>
                <div class="panel-body detail-format">
                    <div class="form-group has-success">
                        <label class="control-label customer-title">Ticket Details</label>
                    </div>
                </div>
                <div >
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th class="sorting th-align" tabindex="0" aria-controls="paidtable" rowspan="1"
                                      colspan="1" aria-label="
                                                Product Name
                                            : activate to sort column ascending">Product Name
                            </th>
                            <th class="sorting th-align" tabindex="0" aria-controls="paidtable" rowspan="1"
                                     colspan="1" aria-label="
                                                Delivery Date
                                            : activate to sort column ascending">Delivery Date
                            </th>
                            <th class="sorting th-align" tabindex="0" aria-controls="paidtable" rowspan="1"
                                colspan="1" aria-label="
                                                Price
                                            : activate to sort column ascending">Price
                            </th>
                            <th class="sorting th-align" tabindex="0" aria-controls="paidtable" rowspan="1"
                                colspan="1" aria-label="
                                                Amount
                                            : activate to sort column ascending">Amount
                            </th>
                            <th class="sorting th-align" tabindex="0" aria-controls="paidtable" rowspan="1"
                                colspan="1" aria-label="
                                                Total Price
                                            : activate to sort column ascending">Total Price
                            </th>
                            <th class="sorting th-align" tabindex="0" aria-controls="paidtable" rowspan="1"
                                colspan="1" aria-label="
                                                Cancel
                                            : activate to sort column ascending">
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        foreach($products as $product){
                            if(!empty($product)){
                                // product name and total price;
                                $vendor = DB::table('users')->where('id', $product->admin_id)->first();
                                $currency = DB::table('occ_countries')->where('id', $vendor->country_id)->first();

                                if($product->product_size == 1){
                                    $product_name = $product->small_size_display_name;
                                    $price = $product->small_size_price.''.($currency->currency);
                                } elseif($product->product_size == 2){
                                    $product_name = $product->medium_size_display_name;
                                    $price = ($product->medium_size_price).''.($currency->currency);
                                }else{
                                    if(($product->has_small_size == 0)&&($product->has_medium_size == 0)){
                                        $product_name = $product->name;
                                        $price = ($product->price).''.($currency->currency);
                                    }else{
                                        $product_name = $product->large_size_display_name;
                                        $price = ($product->price).''.($currency->currency);
                                    }
                                }
                                $totalprice = $price*($product->order_amount).''.($currency->currency);
                            //shipper;
                                $vendor = DB::table('users')->where('id', $product->admin_id)->first();
                                $shipper_id = $vendor->shipper_id;

                                if ($shipper_id == 0) {
                                    $shipper_name = 'Self';
                                } else {
                                    $shipper = DB::table('occ_shippers')->where('id', $shipper_id)->first();
                                    $shipper_name = $shipper->name;
                                }
                            //delivery date;
                                if(!empty($product->delivery_date)){
                                    if(date('Y', strtotime($product->delivery_date)) == date('Y', strtotime($product->created_at))){
                                        if(date('M/d', strtotime($product->delivery_date)) == date('M/d', strtotime($product->created_at))){
                                            $delivery_date = date('H:i', strtotime($date->delivery_date));
                                        }else{
                                            $delivery_date = date('H:M/d', strtotime($product->delivery_date));
                                        }
                                    }else{
                                        $delivery_date = date('H:M/d/Y', strtotime($product->delivery_date));
                                    }
                                }
                        ?>
                            <tr>
                                <td>{{ $product_name }}</td>
                                <td>{{ $delivery_date }}
                                <td>{{ $price }}</td>
                                <td>{{ $product->order_amount }}</td>
                                <td>{{ $totalprice }}</td>
                                <td class="cancel-btn" onclick="cancel_Modal('{{$id}}','{{$product->items_id}}')">
                                    <i class="livicon del-color" data-name="trash" data-size="18" data-c="#ff173d" data-hc="#ff173d" data-loop="true"></i>Cancel</td>
                        <?php
                            }
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="modal fade" id="cancelModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content modal-size": >
                        <div class="modal-body modal-header-size">
                            Notice!!!
                        </div>
                        <input type="hidden" id="id" value="">
                        <input type="hidden" id="items_id" value="">
                        <div class="modal-body modal-body-size" id="modalcontent">
                            Do you really want to cancel pay for this product?
                        </div>
                        <div class="modal-footer">
                            <input type="hidden" id="id" value="">
                            <input type="hidden" id="items_id" value="">
                            <button type="button" class="btn btn-default" class="form-control" data-dismiss="modal" onclick="cancel_product()">Ok</button>
                            <button type="button" class="btn btn-default" class="form-control" data-dismiss="modal">No</button>
                        </div>
                    </div>
                </div>
            </div>

            {{--customer information--}}
            <?php if(!empty($products)){?>
            <div>
                <div class="panel-body" style="width:90%;margin-left:5%">
                    <div class="form-group has-success">
                        <label class="control-label customer-title">Customer Information</label>
                    </div>
                </div>
                <div>
                    <div class="form-group has-success customer-style">
                        <?php
                        if(!empty($user)){
                            $customername =  $user->first_name.' '.$user->last_name;
                        }
                        ?>
                        <label class="control-label label-size" for="name">Name</label>
                        <input type="text" class="form-control" id="name" name="name" disabled value="{{ $customername }}" placeholder="product name">
                    </div>
                    <div class="form-group has-success customer-style">
                        <label class="control-label label-size" for="gender">Gender</label>
                        <input type="text" class="form-control" id="gender" name="gender" disabled value="{{ $user->gender }}">
                    </div>
                    <div class="form-group has-success customer-style">
                        <label class="control-label label-size" for="birthday">Birthday</label>
                        <input type="text" class="form-control" id="birthday" name="birthday" disabled value="{{ $user->dob }}">
                    </div>
                    <div class="form-group has-success customer-style">
                        <label class="control-label label-size" for="fb_id">Facebook ID</label>
                        <input type="text" class="form-control" id="fb_id" name="fb_id" disabled value="{{ $user->fb_id }}">
                    </div>
                    <div class="form-group has-success customer-style">
                        <label class="control-label label-size" for="email">User Email</label>
                        <input type="text" class="form-control" id="email" name="email" disabled value="{{ $user->email }}">
                    </div>
                    <div class="form-group has-success customer-style">
                        <label class="control-label label-size" for="country">Country</label>
                        <?php
                        $country = DB::table('occ_countries')->where('id', $user->country_id)->first();
                        $name = $country->name;
                        ?>
                        <input type="text" class="form-control" id="country" name="country" disabled value="{{ $name }}">
                    </div>
                    <div class="form-group has-success customer-style">
                        <label class="control-label label-size" for="city">City</label>
                        <?php
                        $city = DB::table('occ_cities')->where('id', $user->city_id)->first();
                        $city_name = $city->name;
                        ?>
                        <input type="text" class="form-control" id="city" name="city" disabled value="{{ $city_name }}">
                    </div>
                    <div class="form-group has-success customer-style">
                        <label class="control-label label-size" for="address">Address</label>
                        <input type="text" class="form-control" id="address" name="address" disabled value="{{ $user->address }}">
                    </div>
                    <div class="form-group has-success customer-style">
                        <label class="control-label label-size" for="address">Contact email</label>
                        <input type="text" class="form-control" id="address" name="address" disabled value="{{ $user->email2 }}">
                    </div>
                    <div class="form-group has-success customer-style">
                        <label class="control-label label-size" for="address">Postal Code</label>
                        <input type="text" class="form-control" id="address" name="address" disabled value="{{ $user->postal }}">
                    </div>
                    <div class="form-group has-success customer-style">
                        <label class="control-label label-size" for="address">Contact Phone</label>
                        <input type="text" class="form-control" id="address" name="address" disabled value="{{ $user->contactno }}">
                    </div>
                </div>
            </div>
            <div>
                <div class="col-md-12 mar-10"  style="width:50%;margin-left:25%">
                    <input type="reset" value="Close" class="btn btn-success btn-block btn-md btn-responsive btn-font" onclick="onBack({{$user->id}})">
                </div>
            </div>
            <?php }?>
        </form>
    </div>

    <div class="modal fade" id="cancel_Modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content modal-size">
                <div class="modal-body modal-header-size">
                    Notice!!!
                </div>
                <input type="hidden" id="id" value="">
                <input type="hidden" id="items_id" value="">
                <div class="modal-body modal-body-size" id="modalcontent">
                    Do you really want to cancel this ticket?
                </div>
                <div class="modal-footer">
                    <input type="hidden" id="id" value="">
                    <input type="hidden" id="items_id" value="">
                    <button type="button" class="btn btn-default" class="form-control" data-dismiss="modal" onclick="cancel_product()">Ok</button>
                    <button type="button" class="btn btn-default" class="form-control" data-dismiss="modal">No</button>
                </div>
            </div>
        </div>
    </div>

@stop

{{-- page level scripts --}}
@section('footer_scripts')

    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.buttons.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.colReorder.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.responsive.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.rowReorder.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.colVis.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.html5.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.print.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.bootstrap.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.print.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/pdfmake.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/vfs_fonts.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.scroller.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/table-advanced.js') }}" ></script>

    <script>
        function cancel_Modal(id, items_id){
        $('#id').val(id);
        $('#items_id').val(items_id);
        console.log(id);
        console.log(items_id);
        $("#cancel_Modal").modal('show');
        }

        function cancel_product() {
            var id = $("#id").val();
            var items_id = $("#items_id").val();
            console.log(id);
            console.log(items_id);
            var data = {
                id:id,
                items_id: items_id
            };
            $.ajax({
                type: "get",
                url: '/admin/ticket/' + id+'/'+items_id+'/cancel',
                data: data,
                success: function (result) {
                    $('#messageModal').modal('show');
                    console.log(result);
                    location.reload();
                },
                error: function (result) {
                }
            });
        }



    </script>

@stop
