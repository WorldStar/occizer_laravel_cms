@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
    Shipped Tickets
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/colReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/rowReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/scroller.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/pages/tables.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/ticket.css') }}">
@stop

{{-- Page content --}}
@section('content')

    <section class="content-header">
        <h1>Shipped Tickets</h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{ route('dashboard') }}">
                    <i class="livicon" data-name="home" data-size="14" data-loop="true"></i>
                    Dashboard
                </a>
            </li>
            <li class="active" >shipped tickets</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Second Data Table -->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="panel panel-danger table-edit">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                                    <span class="title-font">
                                        <i class="livicon" data-name="book-address" data-c="#00dd00" data-hc="#00dd00" data-size="12" data-loop="true"></i>
                                        Shipped Tickets List
                                    </span>
                        </h3>
                    </div>
                    <div class="panel-body">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                        <div id="sample_editable_1_wrapper" class="">
                            <table class="table table-striped table-bordered table-hover dataTable no-footer sample_editable"
                                   id="shipped_table" role="grid">
                                <thead>
                                <tr role="row">
                                    <th class="sorting_asc th-align" tabindex="0" aria-controls="shipped_table" rowspan="1"
                                        colspan="1"  aria-label="
                                                   Product List
                                            : activate to sort column ascending"> Product List
                                    </th>
                                    <th class="sorting th-align" tabindex="0" aria-controls="shipped_table" rowspan="1"
                                        colspan="1" aria-label="
                                                Description
                                            : activate to sort column ascending">Description
                                    </th>
                                    <th class="sorting_asc th-align" tabindex="0" aria-controls="shipped_table" rowspan="1"
                                        colspan="1"  aria-label="
                                                   Ticket_ID
                                            : activate to sort column ascending">Ticket_ID
                                    </th>
                                    <th class="sorting th-align" tabindex="0" aria-controls="shipped_table" rowspan="1"
                                        colspan="1" aria-label="
                                                Customer
                                            : activate to sort column ascending">Customer
                                    </th>
                                    <?php if($_SESSION['userrole'] == 1){?>
                                    <th class="sorting th-align" tabindex="0" aria-controls="shipped_table" rowspan="1"
                                        colspan="1" aria-label="
                                                Vendor
                                            : activate to sort column ascending">Vendor
                                    </th>
                                    <th class="sorting th-align" tabindex="0" aria-controls="shipped_table" rowspan="1"
                                        colspan="1" aria-label="
                                               Shipper
                                            : activate to sort column ascending">Shipper
                                    </th>
                                    <?php }?>
                                    <th class="sorting_asc th-align" tabindex="0" aria-controls="shipped_table" rowspan="1"
                                        colspan="1"  aria-label="
                                                   Total Price
                                            : activate to sort column ascending" style="width: 70px; text-align: center">Total Price
                                    </th>
                                    <th class="sorting_asc th-align" tabindex="0" aria-controls="shipped_table" rowspan="1"
                                        colspan="1"  aria-label="
                                                   Paid Date
                                            : activate to sort column ascending">Paid Date
                                    </th>
                                    <th class="sorting th-align" tabindex="0" aria-controls="shipped_table" rowspan="1"
                                        colspan="1" aria-label="
                                                Payment Method
                                            : activate to sort column ascending">Payment Method
                                    </th>
                                    <th class="sorting th-align" tabindex="0" aria-controls="shipped_table" rowspan="1"
                                        colspan="1" aria-label="
                                                 Cancel
                                            : activate to sort column ascending">
                                    </th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                        <!-- END EXAMPLE TABLE PORTLET-->
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- content -->

    <!-- cancel modal -->
    <div class="modal fade" id="cancelModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content modal-size">
                <div class="modal-body modal-header-size">
                    Notice!!!
                </div>
                <input type="hidden" id="ticket_id" value="0">
                <div class="modal-body modal-body-size" id="modalcontent">
                    Do you really want to cancel this ticket?
                </div>
                <div class="modal-footer">
                    <input type="hidden" id="modal_ticket_id" value=""/>
                    <button type="button" class="btn btn-default" class="form-control" data-dismiss="modal" onclick="cancel_ticket()">Ok</button>
                    <button type="button" class="btn btn-default" class="form-control" data-dismiss="modal">No</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="messageModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content modal-size">
                <div class="modal-body modal-header-size">
                    Notice!!!
                </div>
                <div class="modal-body modal-body-size" id="messagecontent">
                    Successfully cancelled.
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" class="form-control" data-dismiss="modal">OK</button>
                </div>
            </div>
        </div>
    </div>
@stop

{{-- page level scripts --}}
@section('footer_scripts')

    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.buttons.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.colReorder.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.responsive.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.rowReorder.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.colVis.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.html5.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.print.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.print.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/pdfmake.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/vfs_fonts.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.scroller.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/select2/js/select2.js') }}"></script>
    {{--<script type="text/javascript" src="{{ asset('assets/js/pages/table-editable.js') }}" ></script>--}}

    <script>
        // display table by using Datatable method
        var table = null;
        $(function () {
            var nEditing = null;
            var table = $('#shipped_table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! url("/admin/ticket/getshipped") !!}',
                columns: [
                    {data: 'productlist', name: 'productlist'},
                    {data: 'description', name: 'description'},
                    {data: 'ticket_id', name: 'ticket_id'},
                    {data: 'customer_id', name: 'customer_id'},
                    <?php if($_SESSION['userrole'] == 1){?>
                    {data: 'vendor_id', name: 'vendor_id'},
                    {data: 'shipper_id', name: 'shipper_id'},
                    <?php }?>
                    {data: 'totalprice', name: 'totalprice'},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'payment_method', name: 'payment_method'},
                    {data: 'status', name: 'state', orderable: false, searchable: false}
                ]
            });

            table.on('draw', function () {
                console.log('asaa');
                $('.livicon').each(function () {
                    $(this).updateLivicon();
                });
            });
        })

        //cancel order
        function cancelModal(id){
            $("#modal_ticket_id").val(id);
            $('#cancelModal').modal('show');
            console.log(id);
        }

        function cancel_ticket() {
            var id = $("#modal_ticket_id").val();
            var data = {
                id:id
            };
            $.ajax({
                type: "get",
                url: '/admin/ticket/'+id+'/cancel',
                data: data,
                success: function (result) {
                    $('#messageModal').modal('show');
                    console.log(result);
                    location.reload();
                },
                error: function (result) {
                }
            });
        }
    </script>
@stop
