@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
    Josh Admin Template
    @parent
@stop

{{-- page level styles --}}
@section('header_styles')

    <link href="{{ asset('assets/vendors/fullcalendar/css/fullcalendar.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/css/pages/calendar_custom.css') }}" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" media="all" href="{{ asset('assets/vendors/bower-jvectormap/css/jquery-jvectormap-1.2.2.css') }}"/>
    <link rel="stylesheet" href="{{ asset('assets/vendors/animate/animate.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/pages/only_dashboard.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/frontpage.css') }}" />
    <meta name="_token" content="{{ csrf_token() }}">
    <link rel="stylesheet" type="text/css"
          href="{{ asset('assets/vendors/datetimepicker/css/bootstrap-datetimepicker.min.css') }}">

@stop

{{-- Page content --}}
@section('content')

    <section class="content-header">
        <h1>Welcome to Dashboard</h1>
        <ol class="breadcrumb">
            <li class="active">
                <a href="#">
                    <i class="livicon" data-name="home" data-size="16" data-color="#333" data-hovercolor="#333"></i>
                    Dashboard
                </a>
            </li>
        </ol>
    </section>
    <?php
        if($_SESSION['userrole'] == 2){
            $dashboard_layout = "col-lg-4";
        }else{
            $dashboard_layout = "col-lg-3";
        }
    ?>
    <section class="content">
        <div class="row">
            <div class="{{$dashboard_layout}} col-md-6  col-sm-6 margin_10 animated fadeInLeftBig">
                <!-- Trans label pie charts strats here-->
                <div class="lightbluebg no-radius">
                    <div class="panel-body squarebox square_boxs">
                        <div class="col-xs-12 pull-left nopadmar">
                            <div class="row">
                                <div class="square_box col-xs-7 text-right">
                                    <span>Orders Today</span>
                                    <?php
                                        $today = date("Ymd");
                                        $yesterday = date("Ymd",strtotime("-1 day"));
                                        $month = date('M',strtotime($today));
                                        $today_order_price = 0;
                                        $month_order_price = 0;
                                        $yesterday_order_price = 0;
                                        if(!empty($orders)) {
                                            foreach ($orders as $order) {
                                                $product = DB::table('occ_products')->where('id', ($order->product_id))->first();
                                                if(!empty($product)){
                                                    $date = date("Ymd", strtotime($order->created_at));

                                                    if ($order->product_size == 1) {
                                                        $price = $product->small_size_price;
                                                    } elseif ($order->product_size == 2) {
                                                        $price = $product->medium_size_price;
                                                    } else {
                                                        $price = $product->price;
                                                    }

                                                    if ($date == $today) {
                                                        $today_order_price = $today_order_price + $price*($order->amount);
                                                    }

                                                    if ($date == $yesterday) {
                                                        $yesterday_order_price = $yesterday_order_price + $price*($order->amount);
                                                    }

                                                    if (date("M", strtotime($order->created_at)) == $month) {
                                                        $month_order_price = $month_order_price + $price*($order->amount);
                                                    }
                                                }
                                             }
                                        }
                                        $today_order_price = number_format($today_order_price);
                                        $yesterday_order_price = number_format($yesterday_order_price);
                                        $month_order_price = number_format($month_order_price);


                                    if($_SESSION['userrole'] == 2){
                                        $user = DB::table('users')->where('id', $order->vendor_id)->first();
                                        $country = DB::table('occ_countries')->where('id', $user->country_id)->first();
                                        $currency = $country->currency;
                                    } else{
                                        $currency = '';
                                    }
                                    ?>
                                    <div class="number lab-size">{{ $currency }} {{$today_order_price}}</div>
                                </div>
                                <i class="livicon  pull-right" data-name="shopping-cart-in" data-l="true" data-c="#fff"
                                   data-hc="#fff" data-s="70"></i>
                            </div>
                            <div class="row">
                                <div class="col-xs-6">
                                    <small class="stat-label">Yesterday</small>
                                    <h4>{{ $currency }} {{$yesterday_order_price}}</h4>
                                </div>
                                <div class="col-xs-6 text-right">
                                    <small class="stat-label">This Month</small>
                                    <h4>{{ $currency }} {{$month_order_price}}</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="{{$dashboard_layout}} col-md-6 col-sm-6 margin_10 animated fadeInUpBig">
                <!-- Trans label pie charts strats here-->
                <div class="redbg no-radius">
                    <div class="panel-body squarebox square_boxs">
                        <div class="col-xs-12 pull-left nopadmar">
                            <div class="row">
                                <div class="square_box col-xs-7 pull-left">
                                    <span>Today's Sales</span>
                                    <?php
                                    $today = date("Ymd");
                                    $yesterday = date("Ymd",strtotime("-1 day"));
                                    $month = date('M',strtotime($today));
                                    $today_sale_price = 0;
                                    $yesterday_sale_price = 0;
                                    $month_sale_price = 0;
                                    if(!empty($sales)){
                                        foreach ($sales as $sale) {
                                            if($_SESSION['userrole'] == 1){
                                                $date = date("Ymd", strtotime($sale->created_at));
                                            } else{
                                                $date = date("Ymd", strtotime($sale->paid_date));

                                                if ($sale->product_size == 1) {
                                                    $price = $sale->small_size_price;
                                                } elseif ($sale->product_size == 2) {
                                                    $price = $sale->medium_size_price;
                                                } else {
                                                    $price = $sale->price;
                                                }
                                            }

                                            if ($date == $today) {
                                                if ($_SESSION['userrole'] == 1){
                                                    $today_sale_price = $today_sale_price + $sale->totalprice;
                                                }else{
                                                    $today_sale_price = $today_sale_price + $price*($sale->amount);
                                                }
                                            }

                                            if ($date == $yesterday) {
                                                if ($_SESSION['userrole'] == 1){
                                                    $yesterday_sale_price = $yesterday_sale_price + $sale->totalprice;
                                                }else{
                                                    $yesterday_sale_price = $yesterday_sale_price + $price*($sale->amount);
                                                }
                                            }

                                            if (date("M", strtotime($sale->created_at)) == $month) {
                                                if ($_SESSION['userrole'] == 1){
                                                    $month_sale_price = $month_sale_price + $sale->totalprice;
                                                }else{
                                                    $month_sale_price = $month_sale_price + $price*($sale->amount);
                                                }
                                            }
                                        }
                                    }
                                    $today_sale_price = number_format($today_sale_price);
                                    $yesterday_sale_price = number_format($yesterday_sale_price);
                                    $month_sale_price = number_format($month_sale_price);

                                    if($_SESSION['userrole'] == 2){
                                        $user = DB::table('users')->where('id', $sale->vendor_id)->first();
                                        $country = DB::table('occ_countries')->where('id', $user->country_id)->first();
                                        $currency = $country->currency;
                                    } else{
                                        $currency = '';
                                    }
                                    ?>

                                    <div class="number lab-size">{{ $currency }} {{$today_sale_price}}</div>
                                </div>
                                <i class="livicon pull-right" data-name="piggybank" data-l="true" data-c="#fff"
                                   data-hc="#fff" data-s="70"></i>
                            </div>
                            <div class="row">
                                <div class="col-xs-6">
                                    <small class="stat-label">Yesterday</small>
                                    <h4>{{ $currency }} {{$yesterday_sale_price}}</h4>
                                </div>
                                <div class="col-xs-6 text-right">
                                    <small class="stat-label">This Month</small>
                                    <h4>{{ $currency }} {{$month_sale_price}}</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="{{$dashboard_layout}} col-md-6 col-md-6 margin_10 animated fadeInDownBig">
                <!-- Trans label pie charts strats here-->
                <div class="goldbg no-radius">
                    <div class="panel-body squarebox square_boxs">
                        <div class="col-xs-12 pull-left nopadmar">
                            <div class="row">
                                <div class="square_box col-xs-7 pull-left">
                                    <span>Customers</span>
                                    <?php
                                    $month = date('M',strtotime($today));
                                    $lastmonth = date("M", strtotime("previous month"));
                                    $total_customer_count = count($customers);
                                    $thismonth_customer_count = 0;
                                    $lastmonth_customer_count = 0;
                                    if(!empty($customers)) {
                                        foreach ($customers as $customer) {
                                            $customer_month = date("M", strtotime($customer->created_at));
                                            if ($customer_month == $month) {
                                                $thismonth_customer_count = $thismonth_customer_count +1;
                                            }
                                            if ($customer_month == $lastmonth) {
                                                $lastmonth_customer_count = $lastmonth_customer_count +1;
                                            }
                                        }
                                    }
                                    $total_customer_count = number_format($total_customer_count);
                                    $thismonth_customer_count = number_format($thismonth_customer_count);
                                    $lastmonth_customer_count = number_format($lastmonth_customer_count);
                                    ?>
                                    <div class="number lab-size">{{$total_customer_count}}</div>
                                </div>
                                <i class="livicon pull-right" data-name="user" data-l="true" data-c="#fff"
                                   data-hc="#fff" data-s="70"></i>
                            </div>
                            <div class="row">
                                <div class="col-xs-6">
                                    <small class="stat-label">This Month</small>
                                    <h4>{{$thismonth_customer_count}}</h4>
                                </div>
                                <div class="col-xs-6 text-right">
                                    <small class="stat-label">Last Month</small>
                                    <h4>{{$lastmonth_customer_count}}</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <?php
                if($_SESSION['userrole'] == 1) {
            ?>
            <div class="{{$dashboard_layout}} col-md-6 col-sm-6 margin_10 animated fadeInRightBig">
                <!-- Trans label pie charts strats here-->
                <div class="palebluecolorbg no-radius">
                    <div class="panel-body squarebox square_boxs">
                        <div class="col-xs-12 pull-left nopadmar">
                            <div class="row">
                                <div class="square_box col-xs-7 pull-left">
                                    <span>Vendors</span>
                                    <?php
                                    $month = date('M',strtotime($today));
                                    $lastmonth = date("M", strtotime("previous month"));
                                    $total_vendor_count = count($vendors);
                                    $thismonth_vendor_count = 0;
                                    $lastmonth_vendor_count = 0;
                                    if(!empty($vendors)){
                                        foreach ($vendors as $vendor) {
                                            $vendor_month = date("M", strtotime($vendor->created_at));
                                            if ($vendor_month == $month) {
                                                $thismonth_vendor_count = $thismonth_vendor_count +1;
                                            }
                                            $lastmonth = date("M", strtotime("previous month"));
                                            if ($vendor_month == $lastmonth) {
                                                $lastmonth_vendor_count = $lastmonth_vendor_count +1;
                                            }
                                        }
                                    }
                                    $total_vendor_count = number_format($total_vendor_count);
                                    $thismonth_vendor_count = number_format($thismonth_vendor_count);
                                    $lastmonth_vendor_count = number_format($lastmonth_vendor_count);
                                    ?>

                                    <div class="number lab-size">{{$total_vendor_count}}</div>
                                </div>
                                <i class="livicon pull-right" data-name="users" data-l="true" data-c="#fff"
                                   data-hc="#fff" data-s="70"></i>
                            </div>
                            <div class="row">
                                <div class="col-xs-6">
                                    <small class="stat-label">This Month</small>
                                    <h4>{{$thismonth_vendor_count}}</h4>
                                </div>
                                <div class="col-xs-6 text-right">
                                    <small class="stat-label">Last Month</small>
                                    <h4>{{$lastmonth_vendor_count}}</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php
            }
            ?>
        </div>

        <!--/row-->
        <div class="row ">
            <div class="row recent-custom">
                <!-- BEGIN SAMPLE TABLE PORTLET-->
                <div class="portlet box primary">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="livicon" data-name="address-book" data-c="rgb(255, 255, 255)" data-hc="rgb(255, 255, 255)" data-size="12"
                               data-loop="true"></i>
                            Recently Tickets
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="table-scrollable">

                            <table class="table table-hover tb-recently ">
                                <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Ticket_ID</th>
                                    <th>Customer</th>
                                    <th>Total Price</th>
                                    <th>Created Date</th>
                                    <th>Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $i = 0;
                                foreach ($recentlytickets as $recentlyticket) {
                                    if(!empty($recentlyticket)){
                                        $i++;
                                        $user = DB::table('users')->where('id', $recentlyticket->customer_id)->first();

                                        $customername = '';
                                        if (!empty($user)) {
                                            $customername =  $user->first_name.' '.$user->last_name;
                                        }
                                    $totalprice = $recentlyticket->totalprice;

                                    $user = DB::table('users')->where('id', $recentlyticket->vendor_id)->first();
                                    $country = DB::table('occ_countries')->where('id', $user->country_id)->first();
                                    $currency = $country->currency;

                                    $created_date = date('M d, Y', strtotime($recentlyticket->created_at));
                                        ?>
                                        <tr>
                                            <td>{{$i}}</td>
                                            <td>{{$recentlyticket->ticket_id}}</td>
                                            <td>{{$customername}}</td>
                                            <td>{{$totalprice}} {{ $currency }}</td>
                                            <td>{{$created_date}}</td>

                                            <?php if($recentlyticket->status == 1){?>
                                            <td> <span class="label label-sm label-success paid-icon">paid</span></td>
                                            <?php } elseif($recentlyticket->status == 2) {?>
                                            <td> <span class="label label-sm label-info shipped-icon">shipped</span> </td>
                                            <?php }else{?>
                                            <td> <span class="label label-sm label-info cancelled-icon">cancelled</span> </td>
                                            <?php }?>
                                        </tr>
                                <?php
                                    }
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- END SAMPLE TABLE PORTLET-->
            </div>
        </div>

        <div class="row ">
            <div class="row recent-custom">
                <!-- BEGIN SAMPLE TABLE PORTLET-->
                <div class="portlet box primary">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="livicon" data-name="user" data-c="rgb(255, 255, 255)" data-hc="rgb(255, 255, 255)" data-size="12"
                               data-loop="true"></i>
                            Recently Customers
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="table-scrollable">
                            <table class="table table-hover tb-recently">
                                <thead>
                                <tr>
                                    <th>No</th>
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>Address</th>
                                    <th>Email</th>
                                    <th>Phone Number</th>
                                    <th>Created Date</th>
                                </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $i = 0;
                                    foreach($recentlycustomers as $recentlycustomer){
                                        if(!empty($recentlycustomer)){
                                            $i++;
                                            $created_date = date('M d, Y', strtotime($recentlycustomer->created_at));

                                            $country = DB::table('occ_countries')->where('id', $recentlycustomer->country_id)->first();
                                            $city = DB::table('occ_cities')->where('id', $recentlycustomer->city_id)->first();



                                    $info =$recentlycustomer->address.', '.$city->name.', '.$country->name;
                                    ?>
                                        <tr>
                                            <td>{{$i}}</td>
                                            <td>{{$recentlycustomer->first_name}}</td>
                                            <td>{{$recentlycustomer->last_name}}</td>
                                            <td>{{$info}}</td>
                                            <td>{{$recentlycustomer->email}}</td>
                                            <td>{{$recentlycustomer->postal}}</td>
                                            <td>{{$created_date}}</td>
                                        </tr>
                                    <?php
                                        }
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>

    </section>

@stop

{{-- page level scripts --}}
@section('footer_scripts')
    <script type="text/javascript" src="{{ asset('assets/vendors/moment/js/moment.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datetimepicker/js/bootstrap-datetimepicker.min.js') }}"></script>

    <!-- EASY PIE CHART JS -->
    <script src="{{ asset('assets/vendors/bower-jquery-easyPieChart/js/easypiechart.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/bower-jquery-easyPieChart/js/jquery.easypiechart.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/bower-jquery-easyPieChart/js/jquery.easingpie.js') }}"></script>
    <!--for calendar-->
    <script src="{{ asset('assets/vendors/moment/js/moment.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/fullcalendar/js/fullcalendar.min.js') }}" type="text/javascript"></script>
    <!--   Realtime Server Load  -->
    <script src="{{ asset('assets/vendors/flotchart/js/jquery.flot.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/flotchart/js/jquery.flot.resize.js') }}" type="text/javascript"></script>
    <!--Sparkline Chart-->
    <script src="{{ asset('assets/vendors/sparklinecharts/jquery.sparkline.js') }}"></script>
    <!-- Back to Top-->
    <script type="text/javascript" src="{{ asset('assets/vendors/countUp.js/js/countUp.js') }}"></script>
    <!--   maps -->
    <script src="{{ asset('assets/vendors/bower-jvectormap/js/jquery-jvectormap-1.2.2.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/bower-jvectormap/js/jquery-jvectormap-world-mill-en.js') }}"></script>
    {{--<script src="{{ asset('assets/vendors/Chart.js/js/Chart.js') }}"></script>--}}
    <!--  todolist-->
    <script src="{{ asset('assets/js/pages/todolist.js') }}"></script>
    <script src="{{ asset('assets/js/pages/dashboard.js') }}" type="text/javascript"></script>

@stop
