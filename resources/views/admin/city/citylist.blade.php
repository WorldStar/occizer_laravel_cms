@extends('admin.layouts.default')

{{-- Page title --}}
@section('title')
    Cities
    @parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <meta name="_token" content="{!! csrf_token() !!}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/select2/css/select2.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/select2/css/select2-bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css"
          href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css"
          href="{{ asset('assets/vendors/datatables/css/colReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css"
          href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css"
          href="{{ asset('assets/vendors/datatables/css/rowReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/scroller.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/pages/tables.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/category.css') }}" />
@stop

{{-- Page content --}}
@section('content')

    <section class="content-header">
        <h1>Cities</h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{ route('dashboard') }}">
                    <i class="livicon" data-name="home" data-size="14" data-loop="true"></i>
                    Dashboard
                </a>
            </li>
            <li class="active">cities</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Second Data Table -->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="panel panel-danger table-edit">

                    <div class="panel-heading">
                        <h3 class="panel-title">
                                    <span class="title-font">
                                     <i class="livicon" data-name="map" data-c="#FFFA0F" data-hc="#FFFA0F" data-size="12"
                                        data-loop="true"></i>
                                     Cities</span>
                        </h3>
                    </div>
                    <div class="panel-body">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>

                        <table class="table table-striped table-bordered table-hover dataTable no-footer sample_editable"
                               id="city_table" role="grid">
                            <thead>
                            <tr role="row">
                                <th class="sorting_asc th-align" tabindex="0" aria-controls="city_table" rowspan="1"
                                    colspan="1" aria-label="
                                                ID
                                            : activate to sort column ascending">ID
                                </th>
                                <th class="sorting_asc th-align" tabindex="0" aria-controls="city_table" rowspan="1"
                                    colspan="1" aria-label="
                                                City Name
                                            : activate to sort column ascending">City Name
                                </th>
                                <th class="sorting_asc th-align" tabindex="0" aria-controls="city_table" rowspan="1"
                                    colspan="1" aria-label="
                                                Country Name
                                            : activate to sort column ascending">Country Name
                                </th>
                                <?php if($_SESSION['userrole'] == 1){?>
                                    <th class="sorting_asc th-align" tabindex="0" aria-controls="city_table" rowspan="1"
                                        colspan="1" aria-label="
                                                    Status
                                                : activate to sort column ascending">Status
                                    </th>
                                <?php } ?>
                                <th class="sorting th-align" tabindex="0" aria-controls="city_table" rowspan="1"
                                    colspan="1" aria-label="
                                                Created Date
                                            : activate to sort column ascending">Created Date
                                </th>
                                <?php if($_SESSION['userrole'] == 1){?>
                                    <th class="sorting del-field th-align" tabindex="0" aria-controls="city_table" rowspan="1"
                                        colspan="1" aria-label="
                                                    Edit
                                                : activate to sort column ascending">Edit
                                    </th>
                                    <th class="sorting del-field th-align" tabindex="0" aria-controls="city_table" rowspan="1"
                                        colspan="1" aria-label="
                                                    Delete
                                                : activate to sort column ascending">Delete
                                    </th>
                                <?php } ?>
                            </tr>
                            </thead>
                            <tbody class="th-align">

                            </tbody>
                        </table>
                    </div>
                    <!-- END EXAMPLE TABLE PORTLET-->
                </div>
            </div>
        </div>
        </div>
    </section>
    <!-- content -->

    <!-- add modal -->
    <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content modal-size">
                <div class="modal-header dodal-header-size">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add City </h4>
                </div>
                <div class="modal-body dodal-body-size">
                    <table class="table form-body modal-table-width">
                        <tr class="th-border">
                            <th class="th-border">name:</th>
                            <td class="th-border">
                                <input type="text" name="city_name" class="form-control" id="city_name"  value="" placeholder="city name">
                            </td>
                        </tr>
                        <tr>
                            <th class="th-border">country:</th>
                            <td class="th-border">
                                <select class="form-control" id="country_id" name="country_id" >
                                    <option value="0">select country</option>
                                    <?php
                                    foreach($countries as $country){
                                        echo '<option value="'.$country->id.'">'.$country->name.'</option>';
                                    }
                                    ?>
                                </select>
                            </td>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" class="form-control" data-dismiss="modal" onclick="add_city()">Add</button>
                    <button type="button" class="btn btn-default" class="form-control" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>

    <!-- delete modal -->
    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content modal-size">
                <div class="modal-body modal-body-size">
                    Notice!!!
                </div>
                <input type="hidden" id="delete_id" value="0">
                <div class="modal-body">
                    Do you really want to delete this city?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" class="form-control" data-dismiss="modal" onclick="delete_city()">Delete</button>
                    <button type="button" class="btn btn-default" class="form-control" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Message modal -->
    <div class="modal fade" id="messageModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content modal-size">
                <div class="modal-body modal-body-size">
                    Notice!!!
                </div>
                <div class="modal-body" id="messagecontent">
                    Please input a city name.
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" class="form-control" data-dismiss="modal">Ok</button>
                </div>
            </div>
        </div>
    </div>

    {{-- active product modal--}}
    <div class="modal fade" id="activeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content modal-size">
                <div class="modal-body modal-body-size">
                    Notice!!!
                </div>
                <input type="hidden" id="city_id" value="0">
                <div class="modal-body" id="statecontent">
                    Do you really want to active this city?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" class="form-control" data-dismiss="modal" onclick="active_city()">Ok</button>
                    <button type="button" class="btn btn-default" class="form-control" data-dismiss="modal">No</button>
                </div>
            </div>
        </div>
    </div>
    {{-- inactive product modal--}}
    <div class="modal fade" id="inactiveModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content modal-size">
                <div class="modal-body modal-body-size">
                    Notice!!!
                </div>
                <input type="hidden" id="city_id" value="0">
                <div class="modal-body" id="statecontent">
                    Do you really want to inactive this city?
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" class="form-control" data-dismiss="modal" onclick="inactive_city()">Ok</button>
                    <button type="button" class="btn btn-default" class="form-control" data-dismiss="modal">No</button>
                </div>
            </div>
        </div>
    </div>

@stop

{{-- page level scripts --}}
@section('footer_scripts')

    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.buttons.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.colReorder.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.responsive.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.rowReorder.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.colVis.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.html5.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.print.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.print.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/pdfmake.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/vfs_fonts.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.scroller.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/select2/js/select2.js') }}"></script>

    {{--<script type="text/javascript" src="{{ asset('assets/js/pages/table-editable.js') }}" ></script>--}}

    <script>
        /* display table by using Datatable method  */
        var table = null;
        $(function () {
            var nEditing = null;
            table = $('#city_table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! route("admin.city.data") !!}',
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'name', name: 'name'},
                    {data: 'country_name', name: 'country_name'},
                    <?php if($_SESSION['userrole'] == 1){?>
                        {data: 'status', name: 'status'},
                    <?php }?>
                    {data: 'created_at', name: 'creadted_date'},
                    <?php if($_SESSION['userrole'] == 1){?>
                        {data: 'edit', name: 'edit', orderable: false, searchable: false},
                        {data: 'delete', name: 'delete', orderable: false, searchable: false}
                    <?php }?>
            ]
            });
            table.on('draw', function () {
                $('.livicon').each(function () {
                    $(this).updateLivicon();
                });
            });

            /*  Cancel functionality in row */
            function restoreRow(table, nRow) {
                var aData = table.row(nRow).data();
                var jqTds = $('>td', nRow);

                for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
                    table.cell().data(aData[i], nRow, i, false);
                }
                table.draw(false);
            }
            /*  in Javascript add HTML(button): append('-')  */
            <?php if($_SESSION['userrole'] == 1){?>
                $('#city_table_length').append('<button type="button" class="form-control button-sm add-btn" data-toggle="modal" data-target="#addModal">Add</button >');
            <?php }?>
            /*  Edit functionality  */
            var row_id, name, created_date;

            function editRow(table, nRow) {
                var aData = table.row(nRow).data();
                var jqTds = $('>td', nRow);
                row_id = aData.id ? aData.id : '';
                name = aData.name ? aData.name : '';
                jqTds[0].innerHTML = row_id;
                jqTds[1].innerHTML = '<input type="text" name="name" id="name" class="form-control input-small" value="' + name + '">';
                jqTds[5].innerHTML = '<a class="edit" href="">Save</a>';
                jqTds[6].innerHTML = '<a class="cancel" href="">Cancel</a>';
            }

            /*  Save functionality  */
            function saveRow(table, nRow) {
                var jqInputs = $('input', nRow);
                name = jqInputs[0].value;
                var aData = table.row(nRow).data();
                var jqTds = $('>td', nRow);
                row_id = aData.id ? aData.id : '';
                var data = {
                    id : row_id,
                    name: name,
                    _token:$('meta[name=_token]').attr('content')
                };
                $.ajax({
                    type: "get",
                    url: '/admin/city/' + row_id + '/update',
                    data: data,
                    success: function (result) {
                        console.log('result is' + result);
                        table.draw(false);
                    },
                    error: function (result) {
                        console.log(result)
                    }
                });
            }

            /*  Cancel Edit functionality  */
            function cancelEditRow(table, nRow) {
                var aData = table.row(nRow).data();
                var jqTds = $('>td', nRow);
                for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
                    table.cell().data(aData[i], nRow, i, false);
                }
                table.draw(false);
            }

            /*  When clicked on Delete button   */
            table.on('click', '.delete', function (e) {
                e.preventDefault();
                var nRow = $(this).parents('tr')[0];
                var aData = table.row(nRow).data();
                var jqTds = $('>td', nRow);
                var id = aData.id;
                console.log(id);
                $('#delete_id').val(id);
                $('#deleteModal').modal('show');
            });

            /*  When clicked on cancel button  */
            table.on('click', '.cancel', function (e) {
                e.preventDefault();
                restoreRow(table, nEditing);
                nEditing = null;
            });

            /*  When clicked on edit button  */
            table.on('click', '.edit', function (e) {
                e.preventDefault();
                var nRow = $(this).parents('tr')[0];
                console.log(nRow);
                console.log(nEditing);
                if (nEditing !== null && nEditing != nRow) {
                    alert('You are already editing a row, you must save or cancel that row before editing a new row');
                } else if (nEditing == nRow && this.innerHTML == "Save") {
                    saveRow(table, nEditing);
                    nEditing = null;
                } else {
                    editRow(table, nRow);
                    nEditing = nRow;
                }
            });

            /*
             When clicked on active button
             */
            table.on('click', '.active', function (e) {
                e.preventDefault();
                var nRow = $(this).parents('tr')[0];
                var aData = table.row(nRow).data();
                var jqTds = $('>td', nRow);
                var id = aData.id;
                /*  get row id to delete */
                $('#city_id').val(id);
                /*  display deletemodal */
                $('#inactiveModal').modal('show');
            });
            /*
             When clicked on inactive button
             */
            table.on('click', '.inactive', function (e) {
                e.preventDefault();
                var nRow = $(this).parents('tr')[0];
                var aData = table.row(nRow).data();
                var jqTds = $('>td', nRow);
                var id = aData.id;
                $('#city_id').val(id);
                $('#activeModal').modal('show');
            });
        });
    </script>

    <script>
        /* active and inactive country */
        function active_city(){
            var id = $('#city_id').val();
            console.log(id);
            $.ajax({
                type: "get",
                url: '/admin/city/' + id + '/active_city',
                success: function (result) {
                    if(result == ''){
                        $('#messagecontent').html('Successfully actived.');
                        $('#messageModal').modal('show');
                        console.log('row ' + result + ' actived');
                        location.reload();
                    }else{
                        console.log('row ' + result + ' actived');
                        location.reload();
                    }
                },
                error: function (result) {
                    console.log(result)
                }
            });
        }

        function inactive_city(){
            var id = $('#city_id').val();
            console.log(id);
            // console.log(id);
            $.ajax({
                type: "get",
                url: '/admin/city/' + id + '/inactive_city',
                success: function (result) {
                    console.log('row' + result + ' inactived');
                    location.reload();
                },
                error: function (result) {
                    console.log(result)
                }
            });
        }

        //delete occasion
        function delete_city(){
            var id = $('#delete_id').val();

            $.ajax({
                type: "get",
                url: '/admin/city/' + id + '/delete',
                success: function (result) {
                    console.log(result);
                    if(result == '') {
                        console.log('city' + result + 'deleted');
                        location.reload();
                    } else {
                        $('#messagecontent').html('This city is using in' + result);
                        $('#messageModal').modal('show');
                        console.log('city is using');
                        location.reload();
                    }
                },
                error: function (result) {
                    console.log(result)

                }
            });
        }

        /*   add occasion */
        function add_city() {
            var name = $("#city_name").val();
            var country_id = $("#country_id").val();
            if (name == "") {
                $('#messagecontent').html('Please input city name.');
                $('#messageModal').modal('show');
                return;
            }
            if (country_id == 0) {
                $('#messagecontent').html('Please select country.');
                $('#messageModal').modal('show');
                return;
            }

            var data = {
                name: name,
                country_id: country_id,
                _token:$('meta[name=_token]').attr('content')
            };

            $.ajax({
                type: "get",
                url: '/admin/city/add',
                data: data,
                success: function (result) {
                    console.log(result);
                    if( result == 1) {
                        $('#messagecontent').html('This city aws already registered.');
                        $('#messageModal').modal('show');
                    } else   {
                        table.draw(false);
                    }
                },
                error: function (result) {
                    console.log(result)
                }
            });
        }
    </script>

@stop
