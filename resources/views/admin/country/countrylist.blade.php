@extends('admin.layouts.default')

{{-- Page title --}}
@section('title')
    Cities
    @parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <meta name="_token" content="{!! csrf_token() !!}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/select2/css/select2.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/select2/css/select2-bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css"
          href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css"
          href="{{ asset('assets/vendors/datatables/css/colReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css"
          href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css"
          href="{{ asset('assets/vendors/datatables/css/rowReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/scroller.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/pages/tables.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/category.css') }}">
@stop

{{-- Page content --}}
@section('content')
    <section class="content-header">
        <!--section starts-->
        <h1>Countries</h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{ route('dashboard') }}">
                    <i class="livicon" data-name="home" data-size="14" data-loop="true"></i>
                    Dashboard
                </a>
            </li>
            <li class="active">countries</li>
        </ol>
    </section>
    <!--section ends-->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="portlet box warning">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="livicon" data-name="country" data-c="#EC3F4C" data-hc="#EC3F4C" data-size="12"
                               data-loop="true"></i>
                            Country List
                        </div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-bordered" id="country_table">
                            <thead>
                            <tr role="row">
                                <th class="sorting_asc th-align" tabindex="0" aria-controls="country_table" rowspan="1"
                                    colspan="1" aria-label="
                                                    ID
                                                : activate to sort column ascending">ID
                                </th>
                                <th class="sorting_asc th-align" tabindex="0" aria-controls="country_table" rowspan="1"
                                    colspan="1" aria-label="
                                                    Name
                                                : activate to sort column ascending">Name
                                </th>
                                <th class="sorting_asc th-align" tabindex="0" aria-controls="country_table" rowspan="1"
                                    colspan="1" aria-label="
                                                    Telephone Prefix
                                                : activate to sort column ascending">Telephone Prefix
                                </th>
                                <th class="sorting_asc th-align" tabindex="0" aria-controls="country_table" rowspan="1"
                                    colspan="1" aria-label="
                                                    Currency
                                                : activate to sort column ascending">Currency
                                </th>
                                <th class="sorting th-align" tabindex="0" aria-controls="country_table" rowspan="1"
                                    colspan="1" aria-label="
                                                    Created Date
                                                : activate to sort column ascending">Created Date
                                </th>
                                <?php if($_SESSION['userrole'] == 1) {?>
                                    <th class="sorting th-align" tabindex="0" aria-controls="country_table" rowspan="1"
                                        colspan="1" aria-label="
                                                        Status
                                                    : activate to sort column ascending">Status
                                    </th>
                                    <th class="sorting del-field th-align" tabindex="0" aria-controls="country_table" rowspan="1"
                                        colspan="1" aria-label="
                                                        Edit
                                                    : activate to sort column ascending">Edit
                                    </th>
                                    <th class="sorting del-field th-align" tabindex="0" aria-controls="country_table" rowspan="1"
                                        colspan="1" aria-label="
                                                        Delete
                                                    : activate to sort column ascending">Delete
                                    </th>
                                <?php }?>
                            </tr>
                            </thead>
                            <tbody class="th-align">

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- add modal -->
    <div class="modal fade" id="add_Modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content modal-size">
                <div class="modal-header dodal-header-size">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add Country </h4>
                </div>
                <div class="modal-body modal-body-size">
                    <table class="table form-body modal-table-width">
                        <tr class="th-border">
                            <th class="th-border">Name:</th>
                            <td class="th-border">
                                <select name="countryCode" class="form-control" id="country_name">
                                    <option selected value="">Select currency</option>
                                    <option value="Afghanistan">Afghanistan</option>
                                    <option value="Albania">Albania</option>
                                    <option value="Algeria">Algeria</option>
                                    <option value="Andorra">Andorra</option>
                                    <option value="Angola">Angola</option>
                                    <option value="Anguilla">Anguilla</option>
                                    <option value="Antartica">Antarctica</option>
                                    <option value="Antigua and Barbuda">Antigua and Barbuda</option>
                                    <option value="Argentina">Argentina</option>
                                    <option value="Armenia">Armenia</option>
                                    <option value="Aruba">Aruba</option>
                                    <option value="Australia">Australia</option>
                                    <option value="Austria">Austria</option>
                                    <option value="Azerbaijan">Azerbaijan</option>
                                    <option value="Bahamas">Bahamas</option>
                                    <option value="Bahrain">Bahrain</option>
                                    <option value="Bangladesh">Bangladesh</option>
                                    <option value="Barbados">Barbados</option>
                                    <option value="Belarus">Belarus</option>
                                    <option value="Belgium">Belgium</option>
                                    <option value="Belize">Belize</option>
                                    <option value="Benin">Benin</option>
                                    <option value="Bermuda">Bermuda</option>
                                    <option value="Bhutan">Bhutan</option>
                                    <option value="Bolivia">Bolivia</option>
                                    <option value="Bosnia and Herzegowina">Bosnia Herzegovina</option>
                                    <option value="Botswana">Botswana</option>
                                    <option value="Bouvet Island">Bouvet Island</option>
                                    <option value="Brazil">Brazil</option>
                                    <option value="British Indian Ocean Territory">British Indian Ocean Territory</option>
                                    <option value="Brunei Darussalam">Brunei Darussalam</option>
                                    <option value="Bulgaria">Bulgaria</option>
                                    <option value="Burkina Faso">Burkina Faso</option>
                                    <option value="Burundi">Burundi</option>
                                    <option value="Cambodia">Cambodia</option>
                                    <option value="Cameroon">Cameroon</option>
                                    <option value="Canada">Canada</option>
                                    <option value="Cape Verde Islands">Cape Verde Islands</option>
                                    <option value="Cayman Islands">Cayman Islands</option>
                                    <option value="Central African Republic">Central African Republic</option>
                                    <option value="Chad">Chad</option>
                                    <option value="Chile">Chile</option>
                                    <option value="China">China</option>
                                    <option value="Colombia">Colombia</option>
                                    <option value="Comoros">Comoros</option>
                                    <option value="Congo">Congo</option>
                                    <option value="Congo">Congo, the Democratic Republic of the</option>
                                    <option value="Cook Islands">Cook Islands</option>
                                    <option value="Costa Rica">Costa Rica</option>
                                    <option value="Cota D'Ivoire">Cote d'Ivoire</option>
                                    <option value="Croatia">Croatia (Hrvatska)</option>
                                    <option value="Cuba">Cuba</option>
                                    <option value="Cyprus North">Cyprus North</option>
                                    <option value="Cyprus South">Cyprus South</option>
                                    <option value="Czech Republic">Czech Republic</option>
                                    <option value="Denmark">Denmark</option>
                                    <option value="Djibouti">Djibouti</option>
                                    <option value="Dominica">Dominica</option>
                                    <option value="Dominican Republic">Dominican Republic</option>
                                    <option value="East Timor">East Timor</option>
                                    <option value="Ecuador">Ecuador</option>
                                    <option value="Egypt">Egypt</option>
                                    <option value="El Salvador">El Salvador</option>
                                    <option value="Equatorial Guinea">Equatorial Guinea</option>
                                    <option value="Eritrea">Eritrea</option>
                                    <option value="Estonia">Estonia</option>
                                    <option value="Ethiopia">Ethiopia</option>
                                    <option value="Falkland Islands">Falkland Islands</option>
                                    <option value="Faroe Islands">Faroe Islands</option>
                                    <option value="Fiji">Fiji</option>
                                    <option value="Finland">Finland</option>
                                    <option value="France">France</option>
                                    <option value="France Metropolitan">France</option>
                                    <option value="French Guiana">French Guiana</option>
                                    <option value="French Polynesia">French Polynesia</option>
                                    <option value="Gabon">Gabon</option>
                                    <option value="Gambia">Gambia</option>
                                    <option value="Georgia">Georgia</option>
                                    <option value="Germany">Germany</option>
                                    <option value="Ghana">Ghana</option>
                                    <option value="Gibraltar">Gibraltar</option>
                                    <option value="Greece">Greece</option>
                                    <option value="Greenland">Greenland</option>
                                    <option value="Grenada">Grenada</option>
                                    <option value="Guadeloupe">Guadeloupe</option>
                                    <option value="Guam">Guam</option>
                                    <option value="Guatemala">Guatemala</option>
                                    <option value="Guinea">Guinea</option>
                                    <option value="Guinea-Bissau">Guinea-Bissau</option>
                                    <option value="Guyana">Guyana</option>
                                    <option value="Haiti">Haiti</option>
                                    <option value="Honduras">Honduras</option>
                                    <option value="Hong Kong">Hong Kong</option>
                                    <option value="Hungary">Hungary</option>
                                    <option value="Iceland">Iceland</option>
                                    <option value="India">India</option>
                                    <option value="Indonesia">Indonesia</option>
                                    <option value="Iran">Iran</option>
                                    <option value="Iraq">Iraq</option>
                                    <option value="Ireland">Ireland</option>
                                    <option value="Israel">Israel</option>
                                    <option value="Italy">Italy</option>
                                    <option value="Jamaica">Jamaica</option>
                                    <option value="Japan">Japan</option>
                                    <option value="Jordan">Jordan</option>
                                    <option value="Kazakhstan">Kazakhstan</option>
                                    <option value="Kenya">Kenya</option>
                                    <option value="Kiribati">Kiribati</option>
                                    <option value="Korea North">Korea North</option>
                                    <option value="Korea South">Korea South</option>
                                    <option value="Kuwait">Kuwait</option>
                                    <option value="Kyrgyzstan">Kyrgyzstan</option>
                                    <option value="Lao">Lao People's Democratic Republic</option>
                                    <option value="Latvia">Latvia</option>
                                    <option value="Lebanon">Lebanon</option>
                                    <option value="Lesotho">Lesotho</option>
                                    <option value="Liberia">Liberia</option>
                                    <option value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</option>
                                    <option value="Liechtenstein">Liechtenstein</option>
                                    <option value="Lithuania">Lithuania</option>
                                    <option value="Luxembourg">Luxembourg</option>
                                    <option value="Macau">Macau</option>
                                    <option value="Macedonia">Macedonia</option>
                                    <option value="Madagascar">Madagascar</option>
                                    <option value="Malawi">Malawi</option>
                                    <option value="Malaysia">Malaysia</option>
                                    <option value="Maldives">Maldives</option>
                                    <option value="Mali">Mali</option>
                                    <option value="Malta">Malta</option>
                                    <option value="Marshall Islands">Marshall Islands</option>
                                    <option value="Martinique">Martinique</option>
                                    <option value="Mauritania">Mauritania</option>
                                    <option value="Mauritius">Mauritius</option>
                                    <option value="Mayotte">Mayotte</option>
                                    <option value="Mexico">Mexico</option>
                                    <option value="Micronesia">Micronesia, Federated States of</option>
                                    <option value="Moldova">Moldova, Republic of</option>
                                    <option value="Monaco">Monaco</option>
                                    <option value="Mongolia">Mongolia</option>
                                    <option value="Montserrat">Montserrat</option>
                                    <option value="Morocco">Morocco</option>
                                    <option value="Mozambique">Mozambique</option>
                                    <option value="Myanmar">Myanmar</option>
                                    <option value="Namibia">Namibia</option>
                                    <option value="Nauru">Nauru</option>
                                    <option value="Nepal">Nepal</option>
                                    <option value="Netherlands">Netherlands</option>
                                    <option value="Netherlands Antilles">Netherlands Antilles</option>
                                    <option value="New Caledonia">New Caledonia</option>
                                    <option value="New Zealand">New Zealand</option>
                                    <option value="Nicaragua">Nicaragua</option>
                                    <option value="Niger">Niger</option>
                                    <option value="Nigeria">Nigeria</option>
                                    <option value="Niue">Niue</option>
                                    <option value="Norfolk Island">Norfolk Island</option>
                                    <option value="Northern Mariana Islands">Northern Mariana Islands</option>
                                    <option value="Norway">Norway</option>
                                    <option value="Oman">Oman</option>
                                    <option value="Pakistan">Pakistan</option>
                                    <option value="Palau">Palau</option>
                                    <option value="Panama">Panama</option>
                                    <option value="Papua New Guinea">Papua New Guinea</option>
                                    <option value="Paraguay">Paraguay</option>
                                    <option value="Peru">Peru</option>
                                    <option value="Philippines">Philippines</option>
                                    <option value="Pitcairn">Pitcairn</option>
                                    <option value="Poland">Poland</option>
                                    <option value="Portugal">Portugal</option>
                                    <option value="Puerto Rico">Puerto Rico</option>
                                    <option value="Qatar">Qatar</option>
                                    <option value="Reunion">Reunion</option>
                                    <option value="Romania">Romania</option>
                                    <option value="Russia">Russian Federation</option>
                                    <option value="Rwanda">Rwanda</option>
                                    <option value="San Marino">San Marino</option>
                                    <option value="Sao Tome and Principe">Sao Tome and Principe</option>
                                    <option value="Saudi Arabia">Saudi Arabia</option>
                                    <option value="Senegal">Senegal</option>
                                    <option value="Senegal">Serbia</option>
                                    <option value="Seychelles">Seychelles</option>
                                    <option value="Sierra">Sierra Leone</option>
                                    <option value="Singapore">Singapore</option>
                                    <option value="Slovakia">Slovakia Republic</option>
                                    <option value="Slovenia">Slovenia</option>
                                    <option value="Solomon Islands">Solomon Islands</option>
                                    <option value="Somalia">Somalia</option>
                                    <option value="South Africa">South Africa</option>
                                    <option value="Span">Spain</option>
                                    <option value="SriLanka">Sri Lanka</option>
                                    <option value="St. Helena">St. Helena</option>
                                    <option value="St. Pierre and Miguelon">St. Pierre and Miquelon</option>
                                    <option value="Sudan">Sudan</option>
                                    <option value="Suriname">Suriname</option>
                                    <option value="Swaziland">Swaziland</option>
                                    <option value="Sweden">Sweden</option>
                                    <option value="Switzerland">Switzerland</option>
                                    <option value="Syria">Syrian</option>
                                    <option value="Taiwan">Taiwan</option>
                                    <option value="Tajikistan">Tajikistan</option>
                                    <option value="Tanzania">Tanzania</option>
                                    <option value="Thailand">Thailand</option>
                                    <option value="Togo">Togo</option>
                                    <option value="Tonga">Tonga</option>
                                    <option value="Trinidad and Tobago">Trinidad and Tobago</option>
                                    <option value="Tunisia">Tunisia</option>
                                    <option value="Turkey">Turkey</option>
                                    <option value="Turkmenistan">Turkmenistan</option>
                                    <option value="Turks and Caicos">Turks and Caicos Islands</option>
                                    <option value="Tuvalu">Tuvalu</option>
                                    <option value="Uganda">Uganda</option>
                                    <option value="Ukraine">Ukraine</option>
                                    <option value="United Arab Emirates">United Arab Emirates</option>
                                    <option value="United Kingdom">United Kingdom</option>
                                    <option value="United States">USA</option>
                                    <option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option>
                                    <option value="Uruguay">Uruguay</option>
                                    <option value="Uzbekistan">Uzbekistan</option>
                                    <option value="Vanuatu">Vanuatu</option>
                                    <option value="Venezuela">Venezuela</option>
                                    <option value="Vietnam">Vietnam</option>
                                    <option value="Virgin Islands (British)">Virgin Islands (British)</option>
                                    <option value="Virgin Islands (U.S)">Virgin Islands (U.S.)</option>
                                    <option value="Yemen (Nouth)">Yemen (Nouth)</option>
                                    <option value="Yemen (South)">Yemen (South)</option>
                                    <option value="Zambia">Zambia</option>
                                    <option value="Zimbabwe">Zimbabwe</option>
                                </select>
                            </td>
                        </tr>
                        <tr class="th-border">
                            <th class="th-border">Telephone prefix:</th>
                            <td class="th-border">
                                <select name="countryCode" class="form-control" id="phone_prefix">
                                    <option selected value="">Select currency</option>
                                    <option data-countryCode="" value="+93">Afghanistan  (+93)</option>
                                    <option data-countryCode="" value="+355">Albania  (+355)</option>
                                    <option data-countryCode="DZ" value="+213">Algeria (+213)</option>
                                    <option data-countryCode="AD" value="+376">Andorra (+376)</option>
                                    <option data-countryCode="AO" value="+244">Angola (+244)</option>
                                    <option data-countryCode="AI" value="+1264">Anguilla (+1264)</option>
                                    <option data-countryCode="" value="+672">Antarctica (+672)</option>
                                    <option data-countryCode="AG" value="+1268">Antigua and Barbuda (+1268)</option>
                                    <option data-countryCode="AR" value="+54">Argentina (+54)</option>
                                    <option data-countryCode="AM" value="+374">Armenia (+374)</option>
                                    <option data-countryCode="AW" value="+297">Aruba (+297)</option>
                                    <option data-countryCode="AU" value="+61">Australia (+61)</option>
                                    <option data-countryCode="AT" value="+43">Austria (+43)</option>
                                    <option data-countryCode="AZ" value="+994">Azerbaijan (+994)</option>
                                    <option data-countryCode="BS" value="+1242">Bahamas (+1242)</option>
                                    <option data-countryCode="BH" value="+973">Bahrain (+973)</option>
                                    <option data-countryCode="BD" value="+880">Bangladesh (+880)</option>
                                    <option data-countryCode="BB" value="+1246">Barbados (+1246)</option>
                                    <option data-countryCode="BY" value="+375">Belarus (+375)</option>
                                    <option data-countryCode="BE" value="+32">Belgium (+32)</option>
                                    <option data-countryCode="BZ" value="+501">Belize (+501)</option>
                                    <option data-countryCode="BJ" value="+229">Benin (+229)</option>
                                    <option data-countryCode="BM" value="+1441">Bermuda (+1441)</option>
                                    <option data-countryCode="BT" value="+975">Bhutan (+975)</option>
                                    <option data-countryCode="BO" value="+591">Bolivia (+591)</option>
                                    <option data-countryCode="BA" value="+387">Bosnia Herzegovina (+387)</option>
                                    <option data-countryCode="BW" value="+267">Botswana (+267)</option>
                                    <option data-countryCode="BR" value="+55">Brazil (+55)</option>
                                    <option data-countryCode="BN" value="+673">Brunei (+673)</option>
                                    <option data-countryCode="BG" value="+359">Bulgaria (+359)</option>
                                    <option data-countryCode="BF" value="+226">Burkina Faso (+226)</option>
                                    <option data-countryCode="BI" value="+257">Burundi (+257)</option>
                                    <option data-countryCode="KH" value="+855">Cambodia (+855)</option>
                                    <option data-countryCode="CM" value="+237">Cameroon (+237)</option>
                                    <option data-countryCode="CA" value="+1">Canada (+1)</option>
                                    <option data-countryCode="CV" value="+238">Cape Verde Islands (+238)</option>
                                    <option data-countryCode="KY" value="+1345">Cayman Islands (+1345)</option>
                                    <option data-countryCode="CF" value="+236">Central African Republic (+236)</option>
                                    <option data-countryCode="CL" value="+56">Chile (+56)</option>
                                    <option data-countryCode="CN" value="+86">China (+86)</option>
                                    <option data-countryCode="CO" value="+57">Colombia (+57)</option>
                                    <option data-countryCode="KM" value="+269">Comoros (+269)</option>
                                    <option data-countryCode="CG" value="+242">Congo (+242)</option>
                                    <option data-countryCode="CK" value="+682">Cook Islands (+682)</option>
                                    <option data-countryCode="CR" value="+506">Costa Rica (+506)</option>
                                    <option data-countryCode="HR" value="+385">Croatia (+385)</option>
                                    <option data-countryCode="CU" value="+53">Cuba (+53)</option>
                                    <option data-countryCode="CY" value="+90392">Cyprus North (+90392)</option>
                                    <option data-countryCode="CY" value="+357">Cyprus South (+357)</option>
                                    <option data-countryCode="CZ" value="+42">Czech Republic (+42)</option>
                                    <option data-countryCode="DK" value="+45">Denmark (+45)</option>
                                    <option data-countryCode="DJ" value="+253">Djibouti (+253)</option>
                                    <option data-countryCode="DM" value="+1809">Dominica (+1809)</option>
                                    <option data-countryCode="DO" value="+1809">Dominican Republic (+1809)</option>
                                    <option data-countryCode="EC" value="+593">Ecuador (+593)</option>
                                    <option data-countryCode="EG" value="+20">Egypt (+20)</option>
                                    <option data-countryCode="SV" value="+503">El Salvador (+503)</option>
                                    <option data-countryCode="GQ" value="+240">Equatorial Guinea (+240)</option>
                                    <option data-countryCode="ER" value="+291">Eritrea (+291)</option>
                                    <option data-countryCode="EE" value="+372">Estonia (+372)</option>
                                    <option data-countryCode="ET" value="+251">Ethiopia (+251)</option>
                                    <option data-countryCode="FK" value="+500">Falkland Islands (+500)</option>
                                    <option data-countryCode="FO" value="+298">Faroe Islands (+298)</option>
                                    <option data-countryCode="FJ" value="+679">Fiji (+679)</option>
                                    <option data-countryCode="FI" value="+358">Finland (+358)</option>
                                    <option data-countryCode="FR" value="+33">France (+33)</option>
                                    <option data-countryCode="GF" value="+594">French Guiana (+594)</option>
                                    <option data-countryCode="PF" value="+689">French Polynesia (+689)</option>
                                    <option data-countryCode="GA" value="+241">Gabon (+241)</option>
                                    <option data-countryCode="GM" value="+220">Gambia (+220)</option>
                                    <option data-countryCode="GE" value="+7880">Georgia (+7880)</option>
                                    <option data-countryCode="DE" value="+49">Germany (+49)</option>
                                    <option data-countryCode="GH" value="+233">Ghana (+233)</option>
                                    <option data-countryCode="GI" value="+350">Gibraltar (+350)</option>
                                    <option data-countryCode="GR" value="+30">Greece (+30)</option>
                                    <option data-countryCode="GL" value="+299">Greenland (+299)</option>
                                    <option data-countryCode="GD" value="+1473">Grenada (+1473)</option>
                                    <option data-countryCode="GP" value="+590">Guadeloupe (+590)</option>
                                    <option data-countryCode="GU" value="+671">Guam (+671)</option>
                                    <option data-countryCode="GT" value="+502">Guatemala (+502)</option>
                                    <option data-countryCode="GN" value="+224">Guinea (+224)</option>
                                    <option data-countryCode="GW" value="+245">Guinea-Bissau (+245)</option>
                                    <option data-countryCode="GY" value="+592">Guyana (+592)</option>
                                    <option data-countryCode="HT" value="+509">Haiti (+509)</option>
                                    <option data-countryCode="HN" value="+504">Honduras (+504)</option>
                                    <option data-countryCode="HK" value="+852">Hong Kong (+852)</option>
                                    <option data-countryCode="HU" value="+36">Hungary (+36)</option>
                                    <option data-countryCode="IS" value="+354">Iceland (+354)</option>
                                    <option data-countryCode="IN" value="+91">India (+91)</option>
                                    <option data-countryCode="ID" value="+62">Indonesia (+62)</option>
                                    <option data-countryCode="IR" value="+98">Iran (+98)</option>
                                    <option data-countryCode="IQ" value="+964">Iraq (+964)</option>
                                    <option data-countryCode="IE" value="+353">Ireland (+353)</option>
                                    <option data-countryCode="IL" value="+972">Israel (+972)</option>
                                    <option data-countryCode="IT" value="+39">Italy (+39)</option>
                                    <option data-countryCode="JM" value="+1876">Jamaica (+1876)</option>
                                    <option data-countryCode="JP" value="+81">Japan (+81)</option>
                                    <option data-countryCode="JO" value="+962">Jordan (+962)</option>
                                    <option data-countryCode="KZ" value="+7">Kazakhstan (+7)</option>
                                    <option data-countryCode="KE" value="+254">Kenya (+254)</option>
                                    <option data-countryCode="KI" value="+686">Kiribati (+686)</option>
                                    <option data-countryCode="KP" value="+850">Korea North (+850)</option>
                                    <option data-countryCode="KR" value="+82">Korea South (+82)</option>
                                    <option data-countryCode="KW" value="+965">Kuwait (+965)</option>
                                    <option data-countryCode="KG" value="+996">Kyrgyzstan (+996)</option>
                                    <option data-countryCode="LA" value="+856">Laos (+856)</option>
                                    <option data-countryCode="LV" value="+371">Latvia (+371)</option>
                                    <option data-countryCode="LB" value="+961">Lebanon (+961)</option>
                                    <option data-countryCode="LS" value="+266">Lesotho (+266)</option>
                                    <option data-countryCode="LR" value="+231">Liberia (+231)</option>
                                    <option data-countryCode="LY" value="+218">Libya (+218)</option>
                                    <option data-countryCode="LI" value="+417">Liechtenstein (+417)</option>
                                    <option data-countryCode="LT" value="+370">Lithuania (+370)</option>
                                    <option data-countryCode="LU" value="+352">Luxembourg (+352)</option>
                                    <option data-countryCode="MO" value="+853">Macao (+853)</option>
                                    <option data-countryCode="MK" value="+389">Macedonia (+389)</option>
                                    <option data-countryCode="MG" value="+261">Madagascar (+261)</option>
                                    <option data-countryCode="MW" value="+265">Malawi (+265)</option>
                                    <option data-countryCode="MY" value="+60">Malaysia (+60)</option>
                                    <option data-countryCode="MV" value="+960">Maldives (+960)</option>
                                    <option data-countryCode="ML" value="+223">Mali (+223)</option>
                                    <option data-countryCode="MT" value="+356">Malta (+356)</option>
                                    <option data-countryCode="MH" value="+692">Marshall Islands (+692)</option>
                                    <option data-countryCode="MQ" value="+596">Martinique (+596)</option>
                                    <option data-countryCode="MR" value="+222">Mauritania (+222)</option>
                                    <option data-countryCode="YT" value="+269">Mayotte (+269)</option>
                                    <option data-countryCode="MX" value="+52">Mexico (+52)</option>
                                    <option data-countryCode="FM" value="+691">Micronesia (+691)</option>
                                    <option data-countryCode="MD" value="+373">Moldova (+373)</option>
                                    <option data-countryCode="MC" value="+377">Monaco (+377)</option>
                                    <option data-countryCode="MN" value="+976">Mongolia (+976)</option>
                                    <option data-countryCode="MS" value="+1664">Montserrat (+1664)</option>
                                    <option data-countryCode="MA" value="+212">Morocco (+212)</option>
                                    <option data-countryCode="MZ" value="+258">Mozambique (+258)</option>
                                    <option data-countryCode="MN" value="+95">Myanmar (+95)</option>
                                    <option data-countryCode="NA" value="+264">Namibia (+264)</option>
                                    <option data-countryCode="NR" value="+674">Nauru (+674)</option>
                                    <option data-countryCode="NP" value="+977">Nepal (+977)</option>
                                    <option data-countryCode="NL" value="+31">Netherlands (+31)</option>
                                    <option data-countryCode="NC" value="+687">New Caledonia (+687)</option>
                                    <option data-countryCode="NZ" value="+64">New Zealand (+64)</option>
                                    <option data-countryCode="NI" value="+505">Nicaragua (+505)</option>
                                    <option data-countryCode="NE" value="+227">Niger (+227)</option>
                                    <option data-countryCode="NG" value="+234">Nigeria (+234)</option>
                                    <option data-countryCode="NU" value="+683">Niue (+683)</option>
                                    <option data-countryCode="NF" value="+672">Norfolk Islands (+672)</option>
                                    <option data-countryCode="NP" value="+670">Northern Marianas (+670)</option>
                                    <option data-countryCode="NO" value="+47">Norway (+47)</option>
                                    <option data-countryCode="OM" value="+968">Oman (+968)</option>
                                    <option data-countryCode="PW" value="+680">Palau (+680)</option>
                                    <option data-countryCode="PA" value="+507">Panama (+507)</option>
                                    <option data-countryCode="PG" value="+675">Papua New Guinea (+675)</option>
                                    <option data-countryCode="PY" value="+595">Paraguay (+595)</option>
                                    <option data-countryCode="PE" value="+51">Peru (+51)</option>
                                    <option data-countryCode="PH" value="+63">Philippines (+63)</option>
                                    <option data-countryCode="PL" value="+48">Poland (+48)</option>
                                    <option data-countryCode="PT" value="+351">Portugal (+351)</option>
                                    <option data-countryCode="PR" value="+1787">Puerto Rico (+1787)</option>
                                    <option data-countryCode="QA" value="+974">Qatar (+974)</option>
                                    <option data-countryCode="RE" value="+262">Reunion (+262)</option>
                                    <option data-countryCode="RO" value="+40">Romania (+40)</option>
                                    <option data-countryCode="RU" value="+7">Russia (+7)</option>
                                    <option data-countryCode="RW" value="+250">Rwanda (+250)</option>
                                    <option data-countryCode="SM" value="+378">San Marino (+378)</option>
                                    <option data-countryCode="ST" value="+239">Sao Tome and Principe (+239)</option>
                                    <option data-countryCode="SA" value="+966">Saudi Arabia (+966)</option>
                                    <option data-countryCode="SN" value="+221">Senegal (+221)</option>
                                    <option data-countryCode="CS" value="+381">Serbia (+381)</option>
                                    <option data-countryCode="SC" value="+248">Seychelles (+248)</option>
                                    <option data-countryCode="SL" value="+232">Sierra Leone (+232)</option>
                                    <option data-countryCode="SG" value="+65">Singapore (+65)</option>
                                    <option data-countryCode="SK" value="+421">Slovak Republic (+421)</option>
                                    <option data-countryCode="SI" value="+386">Slovenia (+386)</option>
                                    <option data-countryCode="SB" value="+677">Solomon Islands (+677)</option>
                                    <option data-countryCode="SO" value="+252">Somalia (+252)</option>
                                    <option data-countryCode="ZA" value="+27">South Africa (+27)</option>
                                    <option data-countryCode="ES" value="+34">Spain (+34)</option>
                                    <option data-countryCode="LK" value="+94">Sri Lanka (+94)</option>
                                    <option data-countryCode="SH" value="+290">St. Helena (+290)</option>
                                    {{--<option data-countryCode="KN" value="1869">St. Kitts (+1869)</option>--}}
                                    {{--<option data-countryCode="SC" value="1758">St. Lucia (+1758)</option>--}}
                                    <option data-countryCode="SD" value="+249">Sudan (+249)</option>
                                    <option data-countryCode="SR" value="+597">Suriname (+597)</option>
                                    <option data-countryCode="SZ" value="+268">Swaziland (+268)</option>
                                    <option data-countryCode="SE" value="+46">Sweden (+46)</option>
                                    <option data-countryCode="CH" value="+41">Switzerland (+41)</option>
                                    <option data-countryCode="SI" value="+963">Syria (+963)</option>
                                    <option data-countryCode="TW" value="+886">Taiwan (+886)</option>
                                    <option data-countryCode="TJ" value="+7">Tajikistan (+7)</option>
                                    <option data-countryCode="TJ" value="+255">Tanzania (+255)</option>
                                    <option data-countryCode="TH" value="+66">Thailand (+66)</option>
                                    <option data-countryCode="TG" value="+228">Togo (+228)</option>
                                    <option data-countryCode="TO" value="+676">Tonga (+676)</option>
                                    <option data-countryCode="TT" value="+1868">Trinidad and Tobago (+1868)</option>
                                    <option data-countryCode="TN" value="+216">Tunisia (+216)</option>
                                    <option data-countryCode="TR" value="+90">Turkey (+90)</option>
                                    <option data-countryCode="TM" value="+993">Turkmenistan (+993)</option>
                                    <option data-countryCode="TC" value="+1649">Turks and Caicos Islands (+1649)</option>
                                    <option data-countryCode="TV" value="+688">Tuvalu (+688)</option>
                                    <option data-countryCode="UG" value="+256">Uganda (+256)</option>
                                    <option data-countryCode="UA" value="+380">Ukraine (+380)</option>
                                    <option data-countryCode="AE" value="+971">United Arab Emirates (+971)</option>
                                    <option data-countryCode="UE" value="+44">United Kingdom (+44)</option>
                                    <option data-countryCode="UY" value="+598">Uruguay (+598)</option>
                                    <option data-countryCode="US" value="+1">USA (+1)</option>
                                    <option data-countryCode="UZ" value="+998">Uzbekistan (+998)</option>
                                    <option data-countryCode="VU" value="+678">Vanuatu (+678)</option>
                                    <option data-countryCode="VE" value="+58">Venezuela (+58)</option>
                                    <option data-countryCode="VN" value="+84">Vietnam (+84)</option>
                                    <option data-countryCode="VG" value="+1">Virgin Islands (British) (+1)</option>
                                    <option data-countryCode="VI" value="+1">Virgin Islands (U.S.) (+1)</option>
                                    <option data-countryCode="YE" value="+969">Yemen (North)(+969)</option>
                                    <option data-countryCode="YE" value="+967">Yemen (South)(+967)</option>
                                    <option data-countryCode="ZM" value="+260">Zambia (+260)</option>
                                    <option data-countryCode="ZW" value="+263">Zimbabwe (+263)</option>
                                </select>
                            </td>
                        </tr>
                        <tr class="th-border">
                            <th class="th-border">Currency:</th>
                            <td class="th-border">
                                <select name="currency" class="form-control" id="currency">
                                    <option selected value="">Select currency</option>
                                    <option value="USD">America (United States) Dollars – USD</option>
                                    <option value="AFN">Afghanistan Afghanis – AFN</option>
                                    <option value="ALL">Albania Leke – ALL</option>
                                    <option value="DZD">Algeria Dinars – DZD</option>
                                    <option value="ARS">Argentina Pesos – ARS</option>
                                    <option value="AUD">Australia Dollars – AUD</option>
                                    <option value="ATS">Austria Schillings – ATS</OPTION>

                                    <option value="BSD">Bahamas Dollars – BSD</option>
                                    <option value="BHD">Bahrain Dinars – BHD</option>
                                    <option value="BDT">Bangladesh Taka – BDT</option>
                                    <option value="BBD">Barbados Dollars – BBD</option>
                                    <option value="BEF">Belgium Francs – BEF</OPTION>
                                    <option value="BMD">Bermuda Dollars – BMD</option>

                                    <option value="BRL">Brazil Reais – BRL</option>
                                    <option value="BGN">Bulgaria Leva – BGN</option>
                                    <option value="CAD">Canada Dollars – CAD</option>
                                    <option value="XOF">CFA BCEAO Francs – XOF</option>
                                    <option value="XAF">CFA BEAC Francs – XAF</option>
                                    <option value="CLP">Chile Pesos – CLP</option>

                                    <option value="CNY">China Yuan Renminbi – CNY</option>
                                    <option value="CNY">RMB (China Yuan Renminbi) – CNY</option>
                                    <option value="COP">Colombia Pesos – COP</option>
                                    <option value="XPF">CFP Francs – XPF</option>
                                    <option value="CRC">Costa Rica Colones – CRC</option>
                                    <option value="HRK">Croatia Kuna – HRK</option>

                                    <option value="CYP">Cyprus Pounds – CYP</option>
                                    <option value="CZK">Czech Republic Koruny – CZK</option>
                                    <option value="DKK">Denmark Kroner – DKK</option>
                                    <option value="DEM">Deutsche (Germany) Marks – DEM</OPTION>
                                    <option value="DOP">Dominican Republic Pesos – DOP</option>
                                    <option value="NLG">Dutch (Netherlands) Guilders – NLG</OPTION>

                                    <option value="XCD">Eastern Caribbean Dollars – XCD</option>
                                    <option value="EGP">Egypt Pounds – EGP</option>
                                    <option value="EEK">Estonia Krooni – EEK</option>
                                    <option value="EUR">Euro – EUR</option>
                                    <option value="FJD">Fiji Dollars – FJD</option>
                                    <option value="FIM">Finland Markkaa – FIM</OPTION>

                                    <option value="FRF*">France Francs – FRF*</OPTION>
                                    <option value=DEM">Germany Deutsche Marks – DEM</OPTION>
                                    <option value="XAU">Gold Ounces – XAU</option>
                                    <option value="GRD">Greece Drachmae – GRD</OPTION>
                                    <option value="GTQ">Guatemalan Quetzal – GTQ</OPTION>
                                    <option value="NLG">Holland (Netherlands) Guilders – NLG</OPTION>
                                    <option value="HKD">Hong Kong Dollars – HKD</option>

                                    <option value="HUF">Hungary Forint – HUF</option>
                                    <option value="ISK">Iceland Kronur – ISK</option>
                                    <option value="XDR">IMF Special Drawing Right – XDR</option>
                                    <option value="INR">India Rupees – INR</option>
                                    <option value="IDR">Indonesia Rupiahs – IDR</option>
                                    <option value="IRR">Iran Rials – IRR</option>

                                    <option value="IQD">Iraq Dinars – IQD</option>
                                    <option value="IEP*">Ireland Pounds – IEP*</OPTION>
                                    <option value="ILS">Israel New Shekels – ILS</option>
                                    <option value="ITL*">Italy Lire – ITL*</OPTION>
                                    <option value="JMD">Jamaica Dollars – JMD</option>
                                    <option value="JPY">Japan Yen – JPY</option>

                                    <option value="JOD">Jordan Dinars – JOD</option>
                                    <option value="KES">Kenya Shillings – KES</option>
                                    <option value="KRW">Korea (North) Won – KRW</option>
                                    <option value="KRW">Korea (South) Won – KRW</option>
                                    <option value="KWD">Kuwait Dinars – KWD</option>
                                    <option value="LBP">Lebanon Pounds – LBP</option>
                                    <option value="LUF">Luxembourg Francs – LUF</OPTION>

                                    <option value="MYR">Malaysia Ringgits – MYR</option>
                                    <option value="MTL">Malta Liri – MTL</option>
                                    <option value="MUR">Mauritius Rupees – MUR</option>
                                    <option value="MXN">Mexico Pesos – MXN</option>
                                    <option value="MAD">Morocco Dirhams – MAD</option>
                                    <option value="NLG">Netherlands Guilders – NLG</OPTION>

                                    <option value="NZD">New Zealand Dollars – NZD</option>
                                    <option value="NOK">Norway Kroner – NOK</option>
                                    <option value="OMR">Oman Rials – OMR</option>
                                    <option value="PKR">Pakistan Rupees – PKR</option>
                                    <option value="XPD">Palladium Ounces – XPD</option>
                                    <option value="PEN">Peru Nuevos Soles – PEN</option>

                                    <option value="PHP">Philippines Pesos – PHP</option>
                                    <option value="XPT">Platinum Ounces – XPT</option>
                                    <option value="PLN">Poland Zlotych – PLN</option>
                                    <option value="PTE">Portugal Escudos – PTE</OPTION>
                                    <option value="QAR">Qatar Riyals – QAR</option>
                                    <option value="RON">Romania New Lei – RON</option>

                                    <option value="ROL">Romania Lei – ROL</option>
                                    <option value="RUB">Russia Rubles – RUB</option>
                                    <option value="SAR">Saudi Arabia Riyals – SAR</option>
                                    <option value="XAG">Silver Ounces – XAG</option>
                                    <option value="SGD">Singapore Dollars – SGD</option>
                                    <option value="SKK">Slovakia Koruny – SKK</option>

                                    <option value="SIT">Slovenia Tolars – SIT</option>
                                    <option value="ZAR">South Africa Rand – ZAR</option>
                                    <option value="KRW">South Korea Won – KRW</option>
                                    <option value="ESP">Spain Pesetas – ESP</OPTION>
                                    <option value="XDR">Special Drawing Rights (IMF) – XDR</option>
                                    <option value="LKR">Sri Lanka Rupees – LKR</option>

                                    <option value="SDD">Sudan Dinars – SDD</option>
                                    <option value="SEK">Sweden Kronor – SEK</option>
                                    <option value="CHF">Switzerland Francs – CHF</option>
                                    <option value="TWD">Taiwan New Dollars – TWD</option>
                                    <option value="THB">Thailand Baht – THB</option>
                                    <option value="TTD">Trinidad and Tobago Dollars – TTD</option>

                                    <option value="TND">Tunisia Dinars – TND</option>
                                    <option value="TRY">Turkey New Lira – TRY</option>
                                    <option value="AED">United Arab Emirates Dirhams – AED</option>
                                    <option value="GBP">United Kingdom Pounds – GBP</option>
                                    <option value="USD">United States Dollars – USD</option>
                                    <option value="VEB">Venezuela Bolivares – VEB</option>

                                    <option value="VND">Vietnam Dong – VND</option>
                                    <option value="ZMK">Zambia Kwacha – ZMK</option>
                                </select>

                            </td>
                        </tr>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" class="form-control" data-dismiss="modal" onclick="add_country()">Add</button>
                    <button type="button" class="btn btn-default" class="form-control" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>

    <!-- delete modal -->
    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content modal-size">
                <div class="modal-body modal-body-size">
                    Notice!!!
                </div>
                <input type="hidden" id="delete_id" value="0">
                <div class="modal-body">
                    Do you really want to delete this country?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" class="form-control" data-dismiss="modal" onclick="delete_country()">Delete</button>
                    <button type="button" class="btn btn-default" class="form-control" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Message modal -->
    <div class="modal fade" id="messageModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content modal-size">
                <div class="modal-body modal-body-size">
                    Notice!!!
                </div>
                <div class="modal-body" id="messagecontent">
                    Please input a country name.
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" class="form-control" data-dismiss="modal">Ok</button>
                </div>
            </div>
        </div>
    </div>

    {{-- active product modal--}}
    <div class="modal fade" id="activeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content modal-size">
                <div class="modal-body modal-body-size">
                    Notice!!!
                </div>
                <input type="hidden" id="country_id" value="0">
                <input type="hidden" id="active_id" value="0">
                <div class="modal-body" id="statecontent">
                    Do you really want to active this country?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" class="form-control" data-dismiss="modal" onclick="active_country()">Ok</button>
                    <button type="button" class="btn btn-default" class="form-control" data-dismiss="modal">No</button>
                </div>
            </div>
        </div>
    </div>
    {{-- inactive product modal--}}
    <div class="modal fade" id="inactiveModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content modal-size">
                <div class="modal-body modal-body-size">
                    Notice!!!
                </div>
                <input type="hidden" id="country_id" value="0">
                <input type="hidden" id="inactive_id" value="0">
                <div class="modal-body" id="statecontent">
                    Do you really want to inactive this country?
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" class="form-control" data-dismiss="modal" onclick="inactive_country()">Ok</button>
                    <button type="button" class="btn btn-default" class="form-control" data-dismiss="modal">No</button>
                </div>
            </div>
        </div>
    </div>
@stop

{{-- page level scripts --}}
@section('footer_scripts')

    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.buttons.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.colReorder.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.responsive.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.rowReorder.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.colVis.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.html5.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.print.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.print.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/pdfmake.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/vfs_fonts.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.scroller.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/select2/js/select2.js') }}"></script>
    {{--<script type="text/javascript" src="{{ asset('assets/js/pages/table-editable.js') }}" ></script>--}}
    <script>
        /* display table by using Datatable method  */
        var table = null;
        $(function () {
            var nEditing = null;
            table = $('#country_table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! route("admin.country.data") !!}',
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'name', name: 'name'},
                    {data: 'phone_prefix', name: 'phone_prefix'},
                    {data: 'currency', name: 'currency'},
                    {data: 'created_at', name: 'creadted_date'},
                    <?php if($_SESSION['userrole'] == 1){?>
                        {data: 'status', name: 'status'},
                        {data: 'edit', name: 'edit', orderable: false, searchable: false},
                        {data: 'delete', name: 'delete', orderable: false, searchable: false}
                    <?php }?>
                ]
            });
            table.on('draw', function () {
                $('.livicon').each(function () {
                    $(this).updateLivicon();
                });
            });

            /*  Cancel functionality in row */
            function restoreRow(table, nRow) {
                var aData = table.row(nRow).data();
                var jqTds = $('>td', nRow);

                for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
                    table.cell().data(aData[i], nRow, i, false);
                }
                table.draw(false);
            }
            /*  in Javascript add HTML(button): append('-')  */
            <?php if($_SESSION['userrole'] == 1) { ?>
                $('#country_table_length').append('<button type="button" class="form-control button-sm add-btn" data-toggle="modal" data-target="#add_Modal">Add</button >');
            <?php } ?>
            /*  Edit functionality  */
            var row_id, name, phone_prefix, currency,  created_date;

            function editRow(table, nRow) {
                var aData = table.row(nRow).data();
                var jqTds = $('>td', nRow);
                row_id = aData.id ? aData.id : '';
                name = aData.name ? aData.name : '';
                phone_prefix = aData.phone_prefix ? aData.phone_prefix : '';
                currency = aData.currency ? aData.currency : '';

                jqTds[0].innerHTML = row_id;
                jqTds[1].innerHTML = '<input type="text" name="name" id="name" class="form-control input-small" value="' + name + '">';
                jqTds[2].innerHTML = '<input type="text" name="phone_prefix" id="phone_prefix" class="form-control input-small" value="' +phone_prefix + '">';
                jqTds[3].innerHTML = '<input type="text" name="currency" id="currency" class="form-control input-small" value="' + currency + '">';
                jqTds[6].innerHTML = '<a class="edit" href="">Save</a>';
                jqTds[7].innerHTML = '<a class="cancel" href="">Cancel</a>';
            }

            /*  Save functionality  */
            function saveRow(table, nRow) {
                var jqInputs = $('input', nRow);
                name = jqInputs[0].value;
                phone_prefix = jqInputs[1].value;
                currency = jqInputs[2].value;
                var aData = table.row(nRow).data();
                var jqTds = $('>td', nRow);
                row_id = aData.id ? aData.id : '';
                //var tableData = 'name=' + name  + '&phone_prefix=' + phone_prefix + '&currency=' + currency + '&_token=' + $('meta[name=_token]').attr('content');
                var data = {
                    id: row_id,
                    name: name,
                    phone_prefix: phone_prefix,
                    currency: currency,
                    _token:$('meta[name=_token]').attr('content')
                };
                $.ajax({
                    type: "get",
                    url: '/admin/country/' + row_id + '/update',
                    data: data,
                    success: function (result) {
                        console.log('result is' + result);
                        table.draw(false);
                    },
                    error: function (result) {
                        console.log(result)
                    }
                });
            }

            /*  Cancel Edit functionality  */
            function cancelEditRow(table, nRow) {
                var aData = table.row(nRow).data();
                var jqTds = $('>td', nRow);
                for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
                    table.cell().data(aData[i], nRow, i, false);
                }
                table.draw(false);
            }

            /*  When clicked on Delete button   */
            table.on('click', '.delete', function (e) {
                e.preventDefault();
                var nRow = $(this).parents('tr')[0];
                var aData = table.row(nRow).data();
                var jqTds = $('>td', nRow);
                var id = aData.id;
                console.log(id);
                $('#delete_id').val(id);
                $('#deleteModal').modal('show');
            });

            /*  When clicked on cancel button  */
            table.on('click', '.cancel', function (e) {
                e.preventDefault();
                restoreRow(table, nEditing);
                nEditing = null;
            });

            /*  When clicked on edit button  */
            table.on('click', '.edit', function (e) {
                e.preventDefault();
                var nRow = $(this).parents('tr')[0];
                console.log(nRow);
                console.log(nEditing);
                if (nEditing !== null && nEditing != nRow) {
                    alert('You are already editing a row, you must save or cancel that row before editing a new row');
                } else if (nEditing == nRow && this.innerHTML == "Save") {
                    saveRow(table, nEditing);
                    nEditing = null;
                } else {
                    editRow(table, nRow);
                    nEditing = nRow;
                }
            });

            /*
             When clicked on active button
             */
            table.on('click', '.active', function (e) {
                e.preventDefault();
                var nRow = $(this).parents('tr')[0];
                var aData = table.row(nRow).data();
                var jqTds = $('>td', nRow);
                var id = aData.id;
                /*  get row id to delete */
                $('#country_id').val(id);
                /*  display deletemodal */
                $('#inactiveModal').modal('show');
            });
            /*
             When clicked on inactive button
             */
            table.on('click', '.inactive', function (e) {
                e.preventDefault();
                var nRow = $(this).parents('tr')[0];
                var aData = table.row(nRow).data();
                var jqTds = $('>td', nRow);
                var id = aData.id;
                $('#country_id').val(id);
                $('#activeModal').modal('show');
            });
        });
    </script>

    /*  add and delete country */
    <script>
        function delete_country(){
            var id = $('#delete_id').val();
            $.ajax({
                type: "get",
                url: '/admin/country/' + id + '/delete',
                success: function (result) {
                    if(result == ''){
                        console.log(result);
                        console.log('row ' + result + ' deleted');
                        table.draw(false);
                        $('#messagecontent').html('Successfully deleted.');
                    }else{
                        console.log(result);
                        $('#messagecontent').html('This is using in '+result);
                    }
                    $('#messageModal').modal('show');
                    location.reload();
                },
                error: function (result) {
                    console.log(result)
                }
            });
        }

        /*   add country  */
        function add_country () {
            var name = $("#country_name").val();
            var phone_prefix = $("#phone_prefix").val();
            var currency = $("#currency").val();
            if (name == "") {
                $('#messagecontent').html('Please input country name.');
                $('#messageModal').modal('show');
                return;
            }
            if (phone_prefix == "") {
                $('#messagecontent').html('Please input phone_prefix.');
                $('#messageModal').modal('show');
                return;
            }
            if (currency == "") {
                $('#messagecontent').html('Please input currency.');
                $('#messageModal').modal('show');
                return;
            }

            var data = {
                name: name,
                phone_prefix: phone_prefix,
                currency: currency,
                _token:$('meta[name=_token]').attr('content')
            };
            $.ajax({
                type: "get",
                url: '/admin/country/add_country',
                data: data,
                success: function (result) {
                    if(result == 1){
                        $('#messagecontent').html('The country name was already registered')
                        $('#messageModal').modal('show');
                        return;
                    } else {
                        table.draw(false);
                    }
                },
                error: function (result) {
                    console.log(result)
                }
            });
        }

        /* active and inactive country */
        function active_country(){
            var id = $('#country_id').val();
            console.log(id);
            $.ajax({
                type: "get",
                url: '/admin/country/' + id + '/active_country',
                success: function (result) {
                    console.log('row ' + result + ' actived');
                    location.reload();
                },
                error: function (result) {
                    console.log(result)
                }
            });
        }

        function inactive_country(){
            var id = $('#country_id').val();
            console.log(id);
            // console.log(id);
            $.ajax({
                type: "get",
                url: '/admin/country/' + id + '/inactive_country',
                success: function (result) {
                    console.log('row ' + result + ' inactived');
                    location.reload();
                },
                error: function (result) {
                    console.log(result)
                }
            });
        }
    </script>

@stop
