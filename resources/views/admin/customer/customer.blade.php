@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
Customers
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/colReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/rowReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/scroller.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/pages/tables.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/customer.css') }}">
@stop

{{-- Page content --}}
@section('content')

    <section class="content-header">
    <h1>Customers</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('dashboard') }}">
                <i class="livicon" data-name="home" data-size="14" data-loop="true"></i>
                Dashboard
            </a>
        </li>
        <li>
            <a href="#">customers</a>
        </li>
        <li class="active">list</li>
    </ol>
</section>
            <!--section ends-->
    <section class="content">
        <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary filterable">
                <div class="panel-heading clearfix  ">
                    <div class="panel-title pull-left">
                        <div class="caption title-font">
                               <i class="livicon" data-name="user" data-c="#FF5722" data-hc="#FF5722" data-size="12"
                                  data-loop="true"></i>
                                 Customer List
                        </div>
                    </div>
                </div>
                <div class="panel-body table-responsive">
                    <table class="table table-striped table-bordered" id="table1">
                        <thead>
                            <tr>
                                <th class="td-align">No</th>
                                <th class="td-align">Photo</th>
                                <th class="td-align">First Name</th>
                                <th class="td-align">Last Name</th>
                                <th class="td-align">Email </th>
                                <th class="td-align">Country</th>
                                <th class="td-align">City</th>
                                <th class="td-align">Address</th>
                                <th class="td-align">Company</th>
                                <th class="td-align">Facebook ID </th>
                                <th class="td-align">Contact Number</th>
                                <?php if($_SESSION['userrole'] == 1) { ?>
                                <th class="td-align">&nbsp;</th>
                                <?php }?>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                        $i = 0;
                        foreach($customers as $customer){
                        $i++;
                        $country = DB::table('occ_countries')->where('id', $customer->country_id)->first();
                        $country_name = $country->name;

                        $city = DB::table('occ_cities')->where('id', $customer->city_id)->first();
                        $city_name = $city->name;

                        $address = $customer->address;

                        $status = 'Active';
                        if($customer->status == 1){
                            $status = 'Inactive';
                        }

                        $photo = $customer->pic;
                        if($customer->pic == ''){
                            $photo = 'default.png';
                        }
                        ?>

                        <tr>
                            <td class="td-align">{{ $i }}</td>
                            <td class="td-cursor th-align" data-toggle="modal" data-target="#{{ $customer->id }}">
                                <img  src="/uploads/users/{{ $photo }}" class="photo-size">
                            </td>

                            <div class="modal fade" id="{{ $customer->id }}" role="dialog">
                                <div class="modal-dialog wide-modal">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;  </button>
                                            <h4 class="modal-title">{{ $customer->first_name }} {{ $customer->last_name }}</h4>
                                        </div>
                                        <div class="modal-body">
                                            <img src="/uploads/users/{{ $photo }}" class="modal-photo">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <td class="td-cursor" onclick="customerProfile('{{ $customer->id }}')">{{ $customer->first_name }}</td>
                            <td class="td-cursor" onclick="customerProfile('{{ $customer->id }}')">{{ $customer->last_name }}</td>
                            <td>{{ $customer->email }}</td>
                            <td>{{ $country_name }}</td>
                            <td>{{ $city_name }}</td>
                            <td>{{ $address }}</td>
                            <td>{{ $customer->company }}</td>
                            <td>{{ $customer->fb_id }}</td>
                            <td>{{ $customer->contactno }}</td>
                            <?php if($_SESSION['userrole'] == 1) { ?>
                            <td class="ianctive_button th-align" onclick="customerInactive('{{ $customer->id }}', '{{ $customer->status }}')">{{ $status }}</td>
                            <?php } ?>
                        </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    </section>

<!-- Message modal -->
    <div class="modal fade" id="customerProfileModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content modal-size">
                <div class="modal-header modal-header-size">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">User Profile </h4>
                </div>
                <div class="modal-body">
                    <div class="form-group has-success modal-items">
                        <label class="control-label" for="first_name">First Name</label>
                        <span type="text-align" class="form-control" id="first_name"></span>
                    </div>
                    <div class="form-group has-success modal-items">
                        <label class="control-label" for="last_name">Last Name</label>
                        <span type="text-align" class="form-control" id="last_name"></span>
                    </div>
                    <div class="form-group has-success modal-items">
                        <label class="control-label" for="email">Email</label>
                        <span type="text-align" class="form-control" id="email"></span>
                    </div>
                    <div class="form-group has-success modal-items">
                        <label class="control-label" for="gender">Gender</label>
                        <span type="text-align" class="form-control" id="gender"></span>
                    </div>
                    <div class="form-group has-success modal-items">
                        <label class="control-label" for="birth">Birthday</label>
                        <span type="text-align" class="form-control" id="birth"></span>
                    </div>
                    <div class="form-group has-success modal-items">
                        <label class="control-label" for="address">Address</label>
                        <span type="text-align" class="form-control" id="address"></span>
                    </div>
                    <div class="form-group has-success modal-items">
                        <label class="control-label" for="email2">Contact Email</label>
                        <span type="text-align" class="form-control" id="email2"></span>
                    </div>
                    <div class="form-group has-success modal-items">
                        <label class="control-label" for="postal"> Postal Code</label>
                        <span type="text-align" class="form-control" id="postal"></span>
                    </div>
                    <div class="form-group has-success modal-items">
                        <label class="control-label" for="phone">Phone Number</label>
                        <span type="text-align" class="form-control" id="phone"></span>
                    </div>
                </div>
            </div>
        </div>
    </div>

<!-- statechange  modal -->
    <div class="modal fade" id="inactiveModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content modal-size">
                <div class="modal-body modal-font">
                    Notice!!!
                </div>
                <input type="hidden" id="customerid" value="0">
                <input type="hidden" id="status" value="0">
                <div class="modal-body" id="inactivecontent">
                    Do you really want to in-active this user?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" class="form-control" data-dismiss="modal" onclick="inactiveProc()">Yes</button>
                    <button type="button" class="btn btn-default" class="form-control" data-dismiss="modal">No</button>
                </div>
            </div>
        </div>
    </div>

<!-- Message modal -->
    <div class="modal fade" id="messageModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content modal-size">
                <div class="modal-body modal-font">
                    Notice!!!
                </div>
                <div class="modal-body" id="messagecontent">
                    Successfully in-actived the user.
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" class="form-control" data-dismiss="modal">Ok</button>
                </div>
            </div>
        </div>
    </div>

<!-- active/inactive sfow-->
    <style>
        .ianctive_button:hover {
            color:#0618d8;
        }
        .ianctive_button {
            color:  #d80b06
        }
        .wide-modal{
            width: 200px;
            height: auto;
        }
    </style>
@stop

{{-- page level scripts --}}
@section('footer_scripts')

    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.buttons.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.colReorder.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.responsive.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.rowReorder.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.colVis.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.html5.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.print.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.bootstrap.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.print.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/pdfmake.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/vfs_fonts.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.scroller.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/table-advanced.js') }}" ></script>
    <script>
        function customerProfile(id){
            $.ajax({
                type: "get",
                url: '/admin/customer/get/' + id,
                success: function (result) {
                    var data = JSON.parse(result);
                    $('#first_name').html(data.first_name);
                    $('#last_name').html(data.last_name);
                    $('#email').html(data.email);
                    $('#gender').html(data.gender);
                    $('#birth').html(data.dob);
                    $('#address').html(data.address);
                    $('#email2').html(data.email2);
                    $('#postal').html(data.postal);
                    $('#phone').html(data.contactno);

                    $('#customerProfileModal').modal('show');
                },
                error: function (result) {
                    console.log(result)
                }
            });
        }
        function customerInactive(id, status){
            $('#customerid').val(id);
            $('#status').val(status);
            console.log(status);
            if(status == 1){
                $('#inactivecontent').html("Do you really want to in-active this customer?");
            }else{
                $('#inactivecontent').html("Do you really want to active this customer?");
            }
            $("#inactiveModal").modal('show');
        }
        function inactiveProc(){
            var customerid = $('#customerid').val();
            var status = $('#status').val();
            $.ajax({
                type: "get",
                url: '/admin/customer/inactive/' + customerid+'/'+status,
                success: function (result) {
                    console.log(result);
                    console.log(result.status);
                    if(result.status == 0){
                        $('#messagecontent').html('Successfully in-actived the customer.');
                    }else{
                        $('#messagecontent').html('Successfully actived the customer.');
                    }
                    $('#messageModal').modal('show');
                    location.reload();
                },
                error: function (result) {
                    console.log(result);
                }
            });
        }
    </script>
@stop
