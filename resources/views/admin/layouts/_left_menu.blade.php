<ul id="menu" class="page-sidebar-menu" style="margin-top:10px;">
        <li {!! (Request::is('admin') ? 'class="active"' : '') !!}>
            <a href="{{ route('dashboard') }}">
                <i class="livicon" data-name="home" data-size="18" data-c="#418BCA" data-hc="#418BCA"
                   data-loop="true"></i>
                <span class="title">Dashboard</span>
            </a>
         </li>
        <?php if($_SESSION['userrole'] == 1){?>
        <li {!! (Request::is('admin/category') ? 'class="active"' : '') !!}>
            <a href="{{ URL::to('admin/category') }}">
                <i class="livicon" data-c="#6CC66C" data-hc="#6CC66C" data-name="list-ul" data-size="18" data-loop="true"></i>
                <span class="title">Category</span>
            </a>
        </li>
        <?php }?>
        <li {!! (Request::is('admin/products') || Request::is('admin/flowers') || Request::is('admin/dropzone') || Request::is('admin/multiple_upload')? 'class="active"' : '') !!}>
            <a href="#">
                <i class="livicon" data-name="lab" data-c="#EF6F6C" data-hc="#EF6F6C" data-size="18" data-loop="true"></i>
                <span class="title">Products</span>
                <span class="fa arrow"></span>
            </a>
            <ul class="sub-menu">
                <?php
                    $categories = DB::table('occ_categories')->orderby('id', 'asc')->get();
                    foreach($categories as $category){
                ?>
                <li {!! (Request::is('admin/products/'.$category->id) ? 'class="active"' : '') !!}>
                    <a href="{{ URL::to('admin/products/'.$category->id) }}">
                        <i class="fa fa-angle-double-right"></i>
                        {{ $category->name }}
                    </a>
                </li>
                <?php } ?>
            </ul>
        </li>
    <?php if($_SESSION['userrole'] == 1){?>
        <li {!! (Request::is('admin/occasions') ? 'class="active"' : '') !!}>
            <a href="{{ URL::to('admin/occasions') }}">
                <i class="livicon" data-name="tags" data-size="18" data-c="#EF6F6C" data-hc="#EF6F6C"
                   data-loop="true"></i>
                <span class="title">Occasions</span>
            </a>
        </li>
    <?php }?>
    <?php if($_SESSION['userrole'] == 1) {?>
        <li {!! (Request::is('admin/vendor') || Request::is('admin/vendor') ? 'class="active"' : '') !!}>
                <a href="{{ URL::to('admin/vendor') }}">
                    <i class="livicon" data-name="user-flag" data-c="#418BCA" data-hc="#418BCA" data-size="18"
                       data-loop="true"></i>
                    <span class="title">Vendors</span>
                </a>
        </li>
    <?php }?>
        <li {!! (Request::is('admin/customer') || Request::is('admin/customer') ? 'class="active"' : '') !!}>
            <a href="{{ URL::to('admin/customer') }}">
                <i class="livicon" data-name="user" data-c="#418BCA" data-hc="#418BCA" data-size="18"
                   data-loop="true"></i>
                <span class="title">Customers</span>
            </a>
        </li>
        <li {!! (Request::is('admin/shippers') || Request::is('admin/shippers') ? 'class="active"' : '') !!}>
            <a href="{{ URL::to('admin/shippers') }}">
                <i class="livicon" data-name="car" data-size="18" data-c="#6CC66C" data-hc="#6CC66C"
                   data-loop="true"></i>
                <span class="title">Shippers</span>
            </a>
        </li>
        <li {!! (Request::is('admin/paymethod') ? 'class="active"' : '') !!}>
            <a href="{{ URL::to('admin/paymethod') }}">
                <i class="livicon" data-name="currency" data-size="15" data-c="#EF6F6C" data-hc="#EF6F6C"
                   data-loop="true"></i>
                <span class="title">Payment Method</span>
            </a>
        </li>
        <li {!! (Request::is('admin/orders') || Request::is('admin/flowers') || Request::is('admin/dropzone') || Request::is('admin/multiple_upload')? 'class="active"' : '') !!}>
            <a href="{{ URL::to('admin/orders') }}">
                <i class="livicon" data-name="lab" data-c="#EF6F6C" data-hc="#EF6F6C" data-size="18" data-loop="true"></i>
                <span class="title">Orders</span>
            </a>
        </li>
        <li {!! (Request::is('admin/ticket/paid') || Request::is('admin/ticket/cancelled') || Request::is('admin/ticket/ship') || Request::is('admin/advanced_tables2') ? 'class="active"' : '') !!}>
            <a href="#">
                <i class="livicon" data-name="address-book" data-c="#EF6F6C" data-hc="#EF6F6C" data-size="18"
                   data-loop="true"></i>
                <span class="title">Tickets</span>
                <span class="fa arrow"></span>
            </a>
            <ul class="sub-menu">
                <li {!! (Request::is('admin/ticket/paid') ? 'class="active"' : '') !!}>
                    <a href="{{ URL::to('admin/ticket/paid') }}">
                        <i class="fa fa-angle-double-right"></i>
                        Paid Tickets
                    </a>
                </li>
                <li {!! (Request::is('admin/ticket/ship') ? 'class="active"' : '') !!}>
                    <a href="{{ URL::to('admin/ticket/ship') }}">
                        <i class="fa fa-angle-double-right"></i>
                        Ship Tickets
                    </a>
                </li>
                <li {!! (Request::is('admin/ticket/cancelled') ? 'class="active"' : '') !!}>
                    <a href="{{ URL::to('admin/ticket/cancelled') }}">
                        <i class="fa fa-angle-double-right"></i>
                        Cancelled Tickets
                    </a>
                </li>
            </ul>
        </li>
        <li {!! (Request::is('admin/charts') || Request::is('admin/piecharts') || Request::is('admin/charts_animation') || Request::is('admin/jscharts') || Request::is('admin/sparklinecharts') ? 'class="active"' : '') !!}>
            <a href="#">
                <i class="livicon" data-name="barchart" data-size="18" data-c="#6CC66C" data-hc="#6CC66C"
                   data-loop="true"></i>
                <span class="title">Reports</span>
                <span class="fa arrow"></span>
            </a>
            <ul class="sub-menu">
                <li {!! (Request::is('admin/charts') ? 'class="active"' : '') !!}>
                    <a href="{{ URL::to('admin/charts') }}">
                        <i class="fa fa-angle-double-right"></i>
                        Flot Charts
                    </a>
                </li>
                <li {!! (Request::is('admin/piecharts') ? 'class="active"' : '') !!}>
                    <a href="{{ URL::to('admin/piecharts') }}">
                        <i class="fa fa-angle-double-right"></i>
                        Pie Charts
                    </a>
                </li>
                <li {!! (Request::is('admin/charts_animation') ? 'class="active"' : '') !!}>
                    <a href="{{ URL::to('admin/charts_animation') }}">
                        <i class="fa fa-angle-double-right"></i>
                        Animated Charts
                    </a>
                </li>
                <li {!! (Request::is('admin/jscharts') ? 'class="active"' : '') !!}>
                    <a href="{{ URL::to('admin/jscharts') }}">
                        <i class="fa fa-angle-double-right"></i>
                        JS Charts
                    </a>
                </li>
                <li {!! (Request::is('admin/sparklinecharts') ? 'class="active"' : '') !!}>
                    <a href="{{ URL::to('admin/sparklinecharts') }}">
                        <i class="fa fa-angle-double-right"></i>
                        Sparkline Charts
                    </a>
                </li>
            </ul>
        </li>
        <li {!! (Request::is('admin/country') || Request::is('admin/city')  ? 'class="active"' : '') !!}>
            <a href="#">
                <i class="livicon" data-name="map" data-c="#EF6F6C" data-hc="#EF6F6C" data-size="18"
                   data-loop="true"></i>
                <span class="title">Country</span>
                <span class="fa arrow"></span>
            </a>
            <ul class="sub-menu">
                <li {!! (Request::is('admin/country') ? 'class="active"' : '') !!}>
                    <a href="{{ URL::to('admin/country') }}">
                        <i class="fa fa-angle-double-right"></i>
                        Countries
                    </a>
                </li>
                <li {!! (Request::is('admin/country/city') ? 'class="active"' : '') !!}>
                    <a href="{{ URL::to('admin/city') }}">
                        <i class="fa fa-angle-double-right"></i>
                        Cities
                    </a>
                </li>
            </ul>
        </li>
        <li {!! (Request::is('admin/login') ? 'class="active"' : '') !!}>
            <a href="{{ URL::to('admin/login') }}">
                <i class="livicon" data-name="sign-out" data-size="18" data-c="#EF6F6C" data-hc="#EF6F6C"
                   data-loop="true"></i>
                <span class="title">Log out</span>
            </a>
        </li>
</ul>