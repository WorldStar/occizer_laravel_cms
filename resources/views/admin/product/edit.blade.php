@extends('admin/layouts/default')
{{-- Page title --}}
@section('title')
    Product Details
    @parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/colReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/rowReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/scroller.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/pages/tables.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/product.css') }}">
@stop

{{-- Page content --}}
@section('content')

    <section class="content-header">
        <!--section starts-->
        <h1><?php echo ucfirst($category->name);?></h1>
        <ol class="breadcrumb">
            <li>
                <a href="">
                    <i class="livicon" data-name="home" data-size="14" data-loop="true"></i>
                    Dashboard
                </a>
            </li>
            <li>
                <a href="#">products</a>
            </li>
            <li class="active"><?php echo ($category->name);?></li>
        </ol>
    </section>

    <div class="panel-body page-format">
        <div class="form-group has-success">
            <label class="control-label edit-title">Edit <?php echo ucfirst($category->name);?></label>
        </div>
        <div class="col-sm-12">
            @if(!empty($error))
                {!! $error !!}
            @endif
        </div>
        @if(!empty($success))
            <div class="alert alert-success alert-dismissable">
                {!! $success !!}
            </div>
        @endif
        <form role="form" action="{{ url('/admin/product/update') }}" method="POST" enctype="multipart/form-data">
            <input type="hidden" name="cat_id" value="{{ $category->id }}"/>
            <input type="hidden" name="product_id" value="{{ $product->id }}"/>
            <input type="hidden" name="_token" value="{{ csrf_token() }}"/>

            <div class="form-group has-success">
                <label class="control-label add-product" for="name">Product Name ;</label>
                <input type="text" class="form-control" id="name" name="name" value="{{ $product->name }}" placeholder="product name">
            </div>

            <div class="form-group has-success">
                <label class="control-label add-product" for="photo">Product Photos ;</label>
                <input type="file" class="form-control"  multiple="multiple" id="photo" name="photo[]" placeholder="product photos">
                <?php
                foreach($photos as $photo){
                    echo '<img id="photo'.$photo->id.'" src="/uploads/files/'.$photo->photo.'"
                    class="photo" onclick="deletePhoto('.$photo->id.')">';
                }
                ?>
            </div>

            <div class="form-group has-success" >
                <label class="control-label add-product" for="description">Product Description ;</label>
                <textarea class="form-control" id="description" name="description"
                          placeholder="Please enter product description here..." rows="5">{{ $product->description }}</textarea>
            </div>

            <div class="form-group has-success" >
                <label class="control-label add-product" for="description2">Product Description(Arabic) ;</label>
                <textarea class="form-control" id="description" name="description2"
                          placeholder="Please enter product description here..." rows="5">{{ $product->description2 }}</textarea>
            </div>

            <div class="form-group has-success">
                <label class="control-label add-product" for="amount">Product Amount ;</label>
                <input type="number"  min="0" class="form-control" id="amount" name="amount"
                       value="{{ $product->amount }}">
            </div>
            <div class="form-group has-success">
                <label class="control-label add-product" for="person_number"> Number of Persons ;</label>
                <input type="number" min="0" class="form-control" id="person_number" name="person_number"
                       disabled value="{{ $product->number_of_persons }}">
            </div>

            <?php if($product->has_large_size == 1) {?>
            <div class="form-group has-success">
                <label class="control-label add-product" for="large_size_display_name">Large Size Product ;</label>
                <div class="product-size-select-format">
                    <label class="control-label input-field" for="large_size_display_name">Name ;</label>
                    <?php
                    $vendor = DB::table('users')->where('id', $product->admin_id)->first();
                    $country = DB::table('occ_countries')->where('id', $vendor->country_id)->first();
                    $currency = $country->currency;
                    $large_size_price = $product->price;
                    ?>
                    <input type="text" class="form-control" id="large_size_display_name" name="large_size_display_name"
                           value="{{ $product->large_size_display_name }}" placeholder="name">
                    <label class="control-label input-field" for="large_size_price">Price({{$currency}}) ;</label>
                    <input type="number" min="0" step="0.000" class="form-control" id="large_size_price" name="large_size_price"
                           value="{{ $large_size_price }}" placeholder="{{ $large_size_price }}">
                </div>
            </div>
            <?php }?>

            <?php if($product->has_medium_size == 1) {?>
            <div class="form-group has-success">
                <label class="control-label add-product" for="has_medium_size">Medium Size Product ;</label>
                <div class="product-size-select-format">
                    <label class="control-label input-field" for="has_medium_size">Name ;</label>
                    <?php
                    $vendor = DB::table('users')->where('id', $product->admin_id)->first();
                    $country = DB::table('occ_countries')->where('id', $vendor->country_id)->first();
                    $currency = $country->currency;
                    $medium_size_price = $product->medium_size_price;
                    ?>
                    <input type="text" class="form-control" id="medium_size_display_name" name="medium_size_display_name"
                           value="{{ $product->medium_size_display_name }}" placeholder="name">
                    <label class="control-label input-field" for="medium_size_price">Price({{$currency}}) ;</label>
                    <input type="number" min="0" step="0.000" class="form-control" id="medium_size_price" name="medium_size_price"
                           value="{{ $medium_size_price }}" placeholder="{{ $medium_size_price }}">
                </div>
            </div>
            <?php }?>

            <?php if($product->has_small_size == 1) {?>
            <div class="form-group has-success">
                <label class="control-label add-product" for="has_small_size">Small Size Product ;</label>
                <div class="product-size-select-format">
                    <label class="control-label input-field" for="has_small_size">Name ;</label>
                    <?php
                    $vendor = DB::table('users')->where('id', $product->admin_id)->first();
                    $country = DB::table('occ_countries')->where('id', $vendor->country_id)->first();
                    $currency = $country->currency;
                    $small_size_price = $product->small_size_price;
                    ?>
                    <input type="text" class="form-control" id="small_size_display_name" name="small_size_display_name"
                           value="{{ $product->small_size_display_name }}" placeholder="name">
                    <label class="control-label input-field" for="small_size_price">Price({{$currency}}) ;</label>
                    <input type="number" min="0" step="0.000" class="form-control" id="small_size_price" name="small_size_price"
                           value="{{ $small_size_price }}" placeholder="{{ $small_size_price }}">
                </div>
            </div>
            <?php }?>

            <?php if($_SESSION['userrole'] == 1) {?>
            <div class="form-group has-warning">
                <label class="control-label add-product" for="vendor">Vendor ;</label>
                <select class="form-control" id="vendor" name="vendor">
                    <?php
                    foreach($vendors as $vendor){
                        if($vendor->id == $product->admin_id){
                            echo '<option value="'.$vendor->id.'" selected>'.$vendor->first_name.' '.$vendor->last_name.'</option>';
                        }else{
                            echo '<option value="'.$vendor->id.'">'.$vendor->first_name.' '.$vendor->last_name.'</option>';
                        }
                    }
                    ?>
                </select>
            </div>
            <?php } ?>
            <div class="form-group has-error">
                <label class="control-label add-product" for="occasion">Occasion ;</label>
                <select class="form-control" id="occasion" name="occasion[]" multiple="multiple">
                    <?php
                    foreach($occasions as $occasion){
                        $occ = explode(',', $product->occ_id);
                        $flg = 0;
                        for($i = 0; $i < count($occ); $i++){
                            if($occasion->id == $occ[$i]){
                                $flg = 1;break;
                            }
                        }
                        if($flg == 1){
                            echo '<option value="'.$occasion->id.'" selected>'.$occasion->name.'</option>';
                        }else{
                            echo '<option value="'.$occasion->id.'">'.$occasion->name.'</option>';
                        }
                    }
                    ?>
                </select>
            </div>

            <div class="col-md-12 mar-10">
                <div class="col-xs-6 col-md-6">
                    <input type="submit" name="btnSubmit" id="btnSubmit" value="Update Product"
                           class="btn btn-primary btn-block btn-md btn-responsive btn-add">
                </div>
                <div class="col-xs-6 col-md-6">
                    <input type="reset" value="Exit" class="btn btn-success btn-block btn-md btn-responsive btn-add"
                           onclick="onBack({{ $category->id }})">
                </div>
            </div>
        </form>
    </div>

@stop

{{-- page level scripts --}}
@section('footer_scripts')

    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.buttons.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.colReorder.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.responsive.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.rowReorder.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.colVis.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.html5.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.print.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.bootstrap.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.print.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/pdfmake.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/vfs_fonts.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.scroller.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/table-advanced.js') }}" ></script>
    <script>
        function onBack(cat_id){
            window.location = "/admin/products/"+cat_id;
        }

        function deletePhoto(photo_id){
            $.ajax({
                type: "get",
                url: '/admin/product/photo/delete/' + photo_id,
                success: function (result) {
                    console.log('row ' + result + ' deleted');
                    $('#photo'+photo_id).remove();
                },
                error: function (result) {
                    console.log(result)
                }
            });
        }
    </script>

@stop
