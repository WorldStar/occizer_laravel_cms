@extends('admin/layouts/default')
{{-- Page title --}}
@section('title')
    Product Details
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/colReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/rowReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/scroller.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/pages/tables.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/product.css') }}" />
@stop

{{-- Page content --}}
@section('content')

<section class="content-header">
                <!--section starts-->
    <h1><?php echo ucfirst($category->name);?></h1>
    <ol class="breadcrumb">
        <li>
            <a href="">
                <i class="livicon" data-name="home" data-size="14" data-loop="true"></i>
                         Dashboard
            </a>
        </li>
        <li>
            <a href="#">products</a>
        </li>
        <li class="active"><?php echo ($category->name);?></li>
    </ol>
</section>

<div class="panel-body page-format">
    <div class="form-group has-success">
        <label class="control-label title-format">Add <?php echo ucfirst($category->name);?></label>
    </div>
    <div class="col-sm-12">
        @if(!empty($error))
        {!! $error !!}
        @endif
    </div>
    @if(!empty($success))
        <div class="alert alert-success alert-dismissable">
                {!! $success !!}
        </div>
    @endif
    <form role="form" action="{{ url('/admin/product/store') }}" method="POST" enctype="multipart/form-data">
        <input type="hidden" name="cat_id" value="{{ $category->id }}"/>
        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>

        <div class="form-group has-success">
            <label class="control-label add-product" for="name">Product Name</label>
            <input type="text" class="form-control" id="name" name="name" placeholder="product name" required>
        </div>

        <div class="form-group has-success ">
            <label class="control-label add-product" for="photo" >Product Photos</label>
            <input type="file" class="form-control"  multiple="multiple" id="photo" name="photo[]" placeholder="product photos">
        </div>

        <div class="form-group has-success">
            <label class="control-label add-product" for="description" >Product Description</label>
            <textarea class="form-control" id="description" name="description" placeholder="Please enter product description here..." rows="5"></textarea>
        </div>

        <div class="form-group has-success">
            <label class="control-label add-product" for="description2" >Product Description(Arabic)</label>
            <textarea class="form-control" id="description2" name="description2" placeholder="Please enter product description here in Arabic..." rows="5"></textarea>
        </div>

        <div class="form-group has-success">
            <label class="control-label add-product" for="amount">Product Amount</label>
            <input type="number" min="0" class="form-control"  id="amount" name="amount" placeholder="0" required onkeypress="return isNumberKey(event)">
        </div>

        <?php
        $user = Sentinel::getUser();
            $country = DB::table('occ_countries')->where('id',$user->country_id)->first();
            $currency = $country->currency;
        ?>
        <div class="form-group has-success">
            <label class="control-label add-product" for="amount">Product Size</label>

            <div class="checkbox">
                <label><input type="checkbox" class="control-label"  onclick="check_large(this)" checked>Large Size</label>
                <input type="hidden" id="large-check-state" name="largeCheckState" value="1"/>
            </div>
            <div class="form-group has-success " id="large_product" style="display:block !important;">
                <div class="product-size-select-format">
                    <label class="control-label input-field">Name </label>
                    <input type="text" class="form-control" id="large_size_display_name" name="large_size_display_name"
                           value="" placeholder="name">
                    <label class="control-label input-field">Price({{ $currency }})</label>
                    <input  type="text"  class="form-control" id="large_size_price" name="large_size_price" value=""
                           placeholder="0" onkeypress="return isNumberKey(event)">
                </div>
            </div>

            <div class="checkbox">
                <label><input type="checkbox" class="control-label" onclick="check_medium(this)">Medium Size</label>
            </div>
            <div class="form-group has-success" id="medium_product" style="display:none !important;">
                <div class="product-size-select-format">
                    <label class="control-label input-field">Name </label>
                    <input type="text" class="form-control" id="medium_size_display_name" name="medium_size_display_name"
                           placeholder="name">
                    <label class="control-label input-field">Price({{ $currency }})</label>
                    <input type="text" class="form-control" id="medium_size_price" name="medium_size_price"
                           value="" placeholder="0" onkeypress="return isNumberKey(event)">
                </div>
            </div>

            <div class="checkbox">
                <label><input type="checkbox" class="control-label" onclick="check_small(this)">Small Size</label>
            </div>
            <div class="form-group has-success" id="small_product" style="display:none !important;">
                <div class="product-size-select-format">
                    <label class="control-label">Name </label>
                    <input type="text" class="form-control" id="small_size_display_name" name="small_size_display_name"
                           placeholder="name">
                    <label class="control-label">Price({{ $currency }})</label>
                    <input type="text" class="form-control" id="small_size_price" name="small_size_price"
                           value="" placeholder="0" onkeypress="return isNumberKey(event)">
                </div>
            </div>
        </div>

        <?php if($_SESSION['userrole'] == 1) {?>
        <div class="form-group has-warning">
            <label class="control-label add-product" for="vendor">Vendor</label>
            <select class="form-control" id="vendor" name="vendor" required>
                <?php
                    foreach($vendors as $vendor){
                        echo '<option value="'.$vendor->id.'">'.$vendor->first_name.' '.$vendor->last_name.'</option>';
                    }
                ?>
            </select>
        </div>
        <?php } ?>

        <div class="form-group has-error">
            <label class="control-label add-product" for="occasion">Occasion</label>
            <select class="form-control" id="occasion" name="occasion[]" multiple="multiple" required>
                <?php
                foreach($occasions as $occasion){
                    echo '<option value="'.$occasion->id.'">'.$occasion->name.'</option>';
                }
                ?>
            </select>
        </div>

        <div class="col-md-12 mar-10">
            <div class="col-xs-6 col-md-6">
                <input type="submit" name="btnSubmit" id="btnSubmit"  value="Add Product" class="btn btn-primary btn-block btn-md btn-responsive btn-add">
            </div>
            <div class="col-xs-6 col-md-6">
                <input type="reset" value="Cancel" class="btn btn-success btn-block btn-md btn-responsive btn-add" onclick="onBack({{ $category->id }})">
            </div>
        </div>
    </form>
</div>

@stop

{{-- page level scripts --}}
@section('footer_scripts')

    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.buttons.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.colReorder.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.responsive.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.rowReorder.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.colVis.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.html5.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.print.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.bootstrap.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.print.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/pdfmake.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/vfs_fonts.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.scroller.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/table-advanced.js') }}" ></script>
    <script src="https://code.jquery.com/jquery.min.js"></script>
    <script>
        $(function() {
            console.log("jquery start");
            $("#large_size_display_name").prop("required",true);
            $("#large_size_price").prop("required",true);
        });
        // input only number;
        function isNumberKey(evt){
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode != 46 &&(charCode < 48 || charCode > 57)))
                return false;
            return true;
        }

        function check_large(checkbox) {
            if (checkbox.checked) {
                $("#large_product").css("display","block");
                $("#large_size_display_name").prop("required",true);
                $("#large_size_price").prop("required",true);
            } else {
                $("#large_product").css("display","none");
                document.getElementById("large_size_display_name").value="";
                document.getElementById("large_size_price").value="";
                $("#large_size_display_name").prop("required",false);
                $("#large_size_price").prop("required",false);
            }
            console.log(has_large_size);
        }

        function check_medium(checkbox) {
            if (checkbox.checked) {
                console.log("1","checked");
                $("#medium_product").css("display","block");
                $("#medium_size_display_name").prop("required",true);
                $("#medium_size_price").prop("required",true);
            } else {
                $("#medium_product").css("display","none");
                document.getElementById("medium_size_display_name").value = "";
                document.getElementById("medium_size_price").value = "";
                $("#medium_size_display_name").prop("required",false);
                $("#medium_size_price").prop("required",false);
            }
            console.log(has_medium_size);
        }

        function check_small(checkbox) {
            if (checkbox.checked) {
                $("#small_product").css("display","block");
                $("#small_size_display_name").prop("required",true);
                $("#small_size_price").prop("required",true);
            } else {
                $("#small_product").css("display","none");
                document.getElementById("small_size_display_name").value = "";
                document.getElementById("small_size_price").value = "";
                $("#small_size_display_name").prop("required",false);
                $("#small_size_price").prop("required",false);
            }
            console.log(has_small_size);
        }

        function onBack(cat_id){
            window.location = "/admin/products/"+cat_id;
        }

    </script>

@stop
