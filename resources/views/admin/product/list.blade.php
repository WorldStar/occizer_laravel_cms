@extends('admin.layouts.default')

{{-- Page title --}}
@section('title')
    Products
    @parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/colReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/rowReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/scroller.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/pages/tables.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/product.css') }}">
@stop
{{-- Page content --}}
@section('content')

    <section class="content-header add-product">
        <h1>Products</h1>
       	<ol class="breadcrumb">
            <li>
                <a href="{{ route('dashboard') }}">
                    <i class="livicon" data-name="home" data-size="14" data-loop="true"></i>
                    Dashboard
                </a>
            </li>
            <li class="active "><?php echo ucfirst($category->name);?></li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Second Data Table -->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="panel panel-danger table-edit">
                    <div class="panel-heading clearfix">
                        <h4 class="panel-title pull-left">
                                    <span class="title-icon add-product">
                                        <i class="livicon" data-name="lab" data-c="#71ef6c" data-hc="#71ef6c" data-size="18" data-loop="true"></i>
                                        <?php echo ucfirst($category->name);?>
                                    </span>
                        </h4>
                        <div class="pull-right">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                            <button type="button" class="btn btn-responsive button-alignment btn-success" onclick="addProduct()">Add</button >
                        </div>
                    </div>
					
                    <div class="panel-body"> 
                        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                        <input type="hidden" id="cat_id" value="{{ $category->id }}">
                        <div id="sample_editable_1_wrapper" class="">
                            <table class="table table-striped table-bordered table-hover dataTable no-footer sample_editable"
                                   id="prodyct_table" role="grid">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Name</th>
                                        <th>Descriptin</th>
                                        <th>Descriptin(Arabic)</th>
                                        <th>Size</th>
                                        <th>Price</th>
                                        <th>Number of Persons</th>
                                        <?php if($_SESSION['userrole'] == 1) { ?>
                                        <th>Vendor</th>
                                        <?php }?>
                                        <th>State</th>
                                        <th>Edit</th>
                                        <th>Delete</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    foreach($products as $product){
                                        $product = DB::table('occ_products')->where('id', $product->id)->first();
                                        // product state
                                        if($product->state == 0){
                                            $state = 'incative';
                                        }else{
                                            $state = 'active';
                                        }
                                        //vendor name and currecy;
                                        $vendor = DB::table('users')->where('id', $product->admin_id)->first();
                                        $vendor_name = ($vendor->first_name).' '.($vendor->last_name);
                                        $vendor_country = DB::table('occ_countries')->where('id', $vendor->country_id)->first();
                                        $currency = $vendor_country->currency;

                                    ?>

                                    <tr>
                                        <td>{{ $product->id }}</td>
                                        <td class="td-cursor" onclick="product_detail('{{ $product->id }}')">{{ $product->name }}</td>
                                        <td class="td-cursor" onclick="product_detail('{{ $product->id }}')">{{ $product->description }}</td>
                                        <td class="td-cursor" onclick="product_detail('{{ $product->id }}')">{{ $product->description2 }}</td>

                                        <td>
                                            <?php
                                            if(($product->has_medium_size == 0)&&($product->has_small_size == 0)){?>
                                                {{ $product->name }}
                                            <?php } else{?>

                                            <select id="size_select_{{$product->id}}" onchange="selectFunction({{$product->id}})">
                                                <?php  if($product->has_large_size == 1){?>
                                                    <option  value="{{$product->price}}">{{$product->large_size_display_name}}</option>;
                                                <?php }?>
                                                <?php if($product->has_medium_size == 1){?>
                                                    <option value="{{$product->medium_size_price}}">{{$product->medium_size_display_name}}</option>;
                                                <?php }?>
                                                <?php if($product->has_small_size == 1){?>
                                                    <option value="{{$product->small_size_price}}">{{$product->small_size_display_name}}</option>;
                                                <?php }?>
                                            </select>
                                            <?php }?>
                                        </td>

                                        <td><span id="select_price_{{$product->id}}">{{ $product->price }}</span><span>&nbsp;{{$currency}}</span></td>
                                        <td>{{ $product->number_of_persons }}</td>

                                        <?php if($_SESSION['userrole'] == 1) { ?>
                                            <td>{{ $vendor_name }}</td>
                                        <?php } ?>

                                        <?php if($product->state == 0){?>
                                            <td class="active_button" onclick="active_Modal('{{ $product->id }}')"> {{ $state }}</td>
                                        <?php }else{?>
                                            <td  class="inactive_button" onclick="inactive_Modal('{{ $product->id }}')"> {{ $state }}</td>
                                        <?php }?>

                                        <td class="edit td-cursor" onclick="edit_product('{{ $product->id }}')">
                                            <i class="livicon edit-color" data-name="edit" data-size="18"
                                               data-c="#2e16ff" data-hc="#2e16ff" data-loop="true"></i>Edit</td>
                                        <td class="delete td-cursor" onclick="delete_modal('{{ $product->id }}')">
                                            <i class="livicon del-color" data-name="trash" data-size="18"
                                               data-c="#ff173d" data-hc="#ff173d" data-loop="true"></i>Delete</td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- content -->

    <!-- delete modal -->
    <div class="modal fade" id="delete_Modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content modal-size">
                <div class="modal-body add-product">
                    Notice!!!
                </div>
                <input type="hidden" id="delete_id" value="0">
                <div class="modal-body">
                    Do you really want to delete this product?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" class="form-control" data-dismiss="modal" onclick="delete_product()">Delete</button>
                    <button type="button" class="btn btn-default" class="form-control" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Message modal -->
    <div class="modal fade" id="message_Modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content modal-size">
                <div class="modal-body add-product">
                    Notice!!!
                </div>
                <div class="modal-body" id="message_content">
                    Please input a product name.
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" class="form-control" data-dismiss="modal">OK</button>
                </div>
            </div>
        </div>
    </div>

    {{-- active product modal--}}
    <div class="modal fade" id="active_Modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content modal-size">
                <div class="modal-body add-product">
                    Notice!!!
                </div>
                <input type="hidden" id="product_id" value="0">
                <input type="hidden" id="activeid" value="0">
                <div class="modal-body" id="statecontent">
                    Do you really want to active this product?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" class="form-control" data-dismiss="modal" onclick="active_product()">Ok</button>
                    <button type="button" class="btn btn-default" class="form-control" data-dismiss="modal">No</button>
                </div>
            </div>
        </div>
    </div>
    {{-- inactive product modal--}}
    <div class="modal fade" id="inactive_Modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content modal-size">
                <div class="modal-body add-product">
                    Notice!!!
                </div>
                <input type="hidden" id="product_id" value="0">
                <input type="hidden" id="inactiveid" value="0">
                <div class="modal-body" id="statecontent">
                    Do you really want to inactive this product?
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" class="form-control" data-dismiss="modal" onclick="inactive_product()">OK</button>
                    <button type="button" class="btn btn-default" class="form-control" data-dismiss="modal">No</button>
                </div>
            </div>
        </div>
    </div>


@stop

{{-- page level scripts --}}
@section('footer_scripts')

    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.buttons.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.colReorder.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.responsive.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.rowReorder.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.colVis.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.html5.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.print.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.print.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/pdfmake.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/vfs_fonts.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.scroller.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/select2/js/select2.js') }}"></script>

   <script>

       function selectFunction(id) {
           var sel = document.getElementById("size_select_"+id);
           var price = sel.options[sel.selectedIndex].value;
           console.log(id, ";", price);
           document.getElementById("select_price_"+id).innerHTML =  price;
       }

       function product_detail(id){
            window.location = "/admin/product/view/" + id;
        }

       // delete product
       function delete_modal(id){
           $('#delete_id').val(id);
           $('#delete_Modal').modal('show');
       }
       function delete_Modal(id){
           $('#delete_id').val(id);
           $('#delete_Modal').modal('show');
       }
       function delete_product(id){
           var id = $('#delete_id').val();
           /* delete row in database and datatable  */
           $.ajax({
               type: "get",
               url: '/admin/product/' + id + '/delete',
               success: function (result) {
                   console.log('row ' + result + ' deleted');
                   location.reload();
               },
               error: function (result) {
                   console.log(result)
               }
           });
       }

       /*   add product */
       function addProduct () {
           var cat_id = $('#cat_id').val();
           window.location = "/admin/product/"+cat_id+"/add";
       }

       /*   edit product */
       function edit_product(id) {
           window.location = "/admin/product/edit/" + id;
       }

        /* active product */
       function active_Modal(id){
           $('#product_id').val(id);
           $('#active_Modal').modal('show');
       }
       function active_product(){
            var id = $('#product_id').val();
             console.log(id);
            $.ajax({
                type: "get",
                url: '/admin/product/' + id + '/active_product',
                success: function (result) {
                    console.log('row ' + result + ' actived');
                    location.reload();
                },
                error: function (result) {
                    console.log(result)
                }
            });
        }

       /* inactive product */
       function inactive_Modal(id){
           $('#product_id').val(id);
           $('#inactive_Modal').modal('show');
       }

        function inactive_product(){
            var id = $('#product_id').val();
            console.log(id);
            // console.log(id);
            $.ajax({
                type: "get",
                url: '/admin/product/' + id + '/inactive_product',
                success: function (result) {
                    console.log('row ' + result + ' inactived');
                    location.reload();
                },
                error: function (result) {
                    console.log(result)
                }
            });
        }
    </script>
@stop
