@extends('admin/layouts/default')
{{-- Page title --}}
@section('title')
    Product Details
    @parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/colReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/rowReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/scroller.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/pages/tables.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/product.css') }}">
@stop

{{-- Page content --}}
@section('content')

    <section class="content-header">
        <!--section starts-->
        <h1><?php echo ucfirst($category->name);?></h1>
        <ol class="breadcrumb">
            <li>
                <a href="">
                    <i class="livicon" data-name="home" data-size="14" data-loop="true"></i>
                    Dashboard
                </a>
            </li>
            <li>
                <a href="#">products</a>
            </li>
            <li class="active title-font"><?php echo ucfirst($category->name);?></li>
        </ol>
    </section>

    <div class="panel-body page-format">
        <div class="form-group has-success">
            <label class="control-label title-format">Details</label>
        </div>
        <div class="col-sm-12">
            @if(!empty($error))
                {!! $error !!}
            @endif
        </div>
            @if(!empty($success))
                <div class="alert alert-success alert-dismissable">
                    {!! $success !!}
                </div>
            @endif
        <form role="form" action="{{ url('/admin/product/edit/'.$product->id) }}" method="get" enctype="multipart/form-data">
            <input type="hidden" name="cat_id" value="{{ $category->id }}"/>
            <input type="hidden" name="product_id" value="{{ $product->id }}"/>
            <input type="hidden" name="_token" value="{{ csrf_token() }}"/>

            <div class="form-group has-success">
                <label class="control-label view-product" for="name">Product Name</label>
                <input type="text" class="form-control" id="name" name="name" disabled value="{{ $product->name }}">
            </div>

            <div class="form-group has-success">
                <label class="control-label view-product" for="photo">Product Photos</label><br>
                <?php
                foreach($photos as $photo){
                    echo '<img id="photo'.$photo->id.'" src="/uploads/files/'.$photo->photo.'" class="photo">';
                }
                ?>
            </div>

            <div class="form-group has-success">
                <label class="control-label view-product" for="description">Product Description</label>
                <textarea class="form-control" id="description" name="description" disabled placeholder="Please enter product description here..." rows="5">{{ $product->description }}</textarea>
            </div>

            <div class="form-group has-success">
                <label class="control-label view-product" for="description2">Product Description(Arabic)</label>
                <textarea class="form-control" id="description2" name="description" disabled placeholder="Please enter product description here in Arabic..." rows="5">{{ $product->description2 }}</textarea>
            </div>

            <div class="form-group has-success">
                <label class="control-label view-product" for="amount">Product Amount</label>
                <input type="number" min="0" class="form-control" id="amount" name="amount"disabled value="{{ $product->amount }}" placeholder="product amount">
            </div>

            <div class="form-group has-success">
                <label class="control-label view-product" for="person_number">Number of Persons</label>
                <input type="number" class="form-control" id="person_number" name="number_of_persons" disabled value="{{ $product->number_of_persons }}">
            </div>

            <?php if($product->has_large_size == 1) {?>
            <div class="form-group has-success">
                <label class="control-label view-product" for="large_size_display_name">Large Size Product </label>
                <div class="page-format">
                    <label class="control-label lab-font" for="large_size_display_name">Name </label>
                    <?php
                        $vendor = DB::table('users')->where('id', $product->admin_id)->first();
                        $country = DB::table('occ_countries')->where('id', $vendor->country_id)->first();
                        $currency = $country->currency;
                        $large_size_price = ($product->price).' '.$currency;
                    ?>
                    <input class="form-control" id="large_size_display_name" name="large_size_display_name" disabled value="{{ $product->large_size_display_name }}">
                    <label class="control-label lab-font" for="large_size_price">Price</label>
                    <input class="form-control" id="large_size_price" name="large_size_price" disabled value="{{ $large_size_price }}">
                </div>
            </div>
            <?php }?>

            <?php if($product->has_medium_size == 1) {?>
            <div class="form-group has-success">
                <label class="control-label view-product" for="has_medium_size">Medium Size Product </label>
                <div class="page-format">
                    <label class="control-label add-product" for="has_medium_size">Name </label>
                    <?php
                        $vendor = DB::table('users')->where('id', $product->admin_id)->first();
                        $country = DB::table('occ_countries')->where('id', $vendor->country_id)->first();
                        $currency = $country->currency;
                        $medium_size_price = ($product->medium_size_price).' '.$currency;
                    ?>
                    <input class="form-control" id="medium_size_display_name" name="medium_size_display_name" disabled value="{{ $product->medium_size_display_name }}">
                    <label class="control-label lab-font" for="medium_size_price">Price</label>
                    <input class="form-control" id="medium_size_price" name="medium_size_price" disabled value="{{ $medium_size_price }}">
                </div>
            </div>
            <?php }?>

            <?php if($product->has_small_size == 1) {?>
            <div class="form-group has-success">
                <label class="control-label view-product" for="has_small_size">Small Size Product </label>
                <div class="page-format">
                    <label class="control-label lab-font" for="has_small_size">Name </label>
                    <?php
                        $vendor = DB::table('users')->where('id', $product->admin_id)->first();
                        $country = DB::table('occ_countries')->where('id', $vendor->country_id)->first();
                        $currency = $country->currency;
                        $small_size_price = ($product->small_size_price).' '.$currency;
                    ?>
                    <input class="form-control" id="small_size_display_name" name="small_size_display_name" disabled value="{{ $product->small_size_display_name }}">
                    <label class="control-label lab-font" for="small_size_price">Price</label>
                    <input class="form-control" id="small_size_price" name="small_size_price" disabled value="{{ $small_size_price }}">
                </div>
            </div>
            <?php }?>

            <div class="form-group has-error">
                <label class="control-label add-product" for="occasion">Occasion</label>
                    <?php
                        $occasion1 = explode(',', $product->occ_id);
                        $occas = '';
                        for($i = 0; $i < count($occasion1); $i++){
                            $occasion = DB::table('occ_occasions')->where('id', $occasion1[$i])->first();
                            if(!empty($occasion)){
                                if($i == 0)
                                    $occas = $occasion->name;
                                else
                                    $occas .= ', '.$occasion->name;
                            }
                        }
                    ?>
                    <input type="text" class="form-control" id="amount" name="amount" disabled value="{{ $occas }}">

            </div>

            <?php if($_SESSION['userrole'] == 1) {?>
            <div class="form-group has-warning">
                <label class="control-label add-product" for="vendor">Vendor</label>
                <select class="form-control" id="vendor" disabled name="vendor">
                    <?php
                    foreach($vendors as $vendor){
                        if($vendor->id == $product->admin_id){
                            echo '<option value="'.$vendor->id.'" selected>'.$vendor->first_name.' '.$vendor->last_name.'</option>';
                        }else{
                            echo '<option value="'.$vendor->id.'">'.$vendor->first_name.' '.$vendor->last_name.'</option>';
                        }
                    }
                    ?>
                </select>
            </div>

            <div class="form-group has-success">
                <label class="control-label view-product" for="description2">Shipper</label>
                <?php
                $shipper = DB::table('occ_shippers')->where('id', $product->s_contact_id)->first();
                $shipper_name = $shipper->name;
                ?>
                <input class="form-control" id="shipper" name="shipper" disabled value="{{ $shipper_name }}"/>
            </div>
            <?php } ?>

            <div class="col-md-12 mar-10">
                <div class="col-xs-6 col-md-6">
                    <input type="submit" name="btnSubmit" id="btnSubmit" value="Edit Product" class="btn btn-primary btn-block btn-md btn-responsive btn-add">
                </div>
                <div class="col-xs-6 col-md-6">
                    <input type="reset" value="Cancel" class="btn btn-primary btn-block btn-md btn-responsive btn-add" onclick="onBack({{ $category->id }})">
                </div>
            </div>

        </form>
    </div>

@stop

{{-- page level scripts --}}
@section('footer_scripts')

    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.buttons.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.colReorder.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.responsive.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.rowReorder.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.colVis.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.html5.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.print.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.bootstrap.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.print.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/pdfmake.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/vfs_fonts.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.scroller.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/table-advanced.js') }}" ></script>

    <script>
        function onBack(cat_id){
            window.location = "/admin/products/"+cat_id;
        }
    </script>

@stop
