@extends('admin.layouts.default')

{{-- Page title --}}
@section('title')
    Occasions
    @parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <meta name="_token" content="{!! csrf_token() !!}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/select2/css/select2.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/select2/css/select2-bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css"
          href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css"
          href="{{ asset('assets/vendors/datatables/css/colReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css"
          href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css"
          href="{{ asset('assets/vendors/datatables/css/rowReorder.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/scroller.bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/pages/tables.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/occasion.css') }}">
@stop

{{-- Page content --}}
@section('content')

    <section class="content-header">
        <h1>Occasions</h1>
       	<ol class="breadcrumb">
            <li>
                <a href="{{ route('dashboard') }}">
                    <i class="livicon" data-name="home" data-size="14" data-loop="true"></i>
                    Dashboard
                </a>
            </li>
            <li class="active">occasions</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Second Data Table -->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="panel panel-danger table-edit">
                    
					<div class="panel-heading">
                        <h3 class="panel-title">
                                    <span class="title-font">
                                     <i class="livicon" data-name="tags" data-size="12" data-c="#FFFA1F" data-hc="#FFFA1F"
                                        data-loop="true"></i>
                                    Occasion List</span>
                        </h3>
                    </div>
					
                    <div class="panel-body"> 
                        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>

                            <table class="table table-striped table-bordered table-hover dataTable no-footer sample_editable"
                                   id="occasion_table" role="grid">
                                <thead>
                                    <tr role="row">
                                        <th class="sorting_asc th-align" tabindex="0" aria-controls="occasion_table" rowspan="1"
                                            colspan="1" aria-label="
                                                ID
                                            : activate to sort column ascending">ID
                                        </th>
                                        <th class="sorting_asc th-align" tabindex="0" aria-controls="occasion_table" rowspan="1"
                                            colspan="1" aria-label="
                                                Name
                                            : activate to sort column ascending">Name
                                        </th>
                                        <th class="sorting th-align" tabindex="0" aria-controls="occasion_table" rowspan="1"
                                            colspan="1" aria-label="
                                                Description
                                            : activate to sort column ascending">Description
                                        </th>
                                        <th class="sorting th-align" tabindex="0" aria-controls="occasion_table" rowspan="1"
                                            colspan="1" aria-label="
                                                Created Date
                                            : activate to sort column ascending">Created Date
                                        </th>
                                        <th class="sorting th-align" tabindex="0" aria-controls="occasion_table" rowspan="1"
                                            colspan="1" aria-label="
                                                status
                                            : activate to sort column ascending">Status
                                        </th>
                                        <th class="sorting del-field th-align" tabindex="0" aria-controls="occasion_table" rowspan="1"
                                            colspan="1" aria-label="
                                                Edit
                                            : activate to sort column ascending">Edit
                                        </th>
                                        <th class="sorting del-field th-align" tabindex="0" aria-controls="occasion_table" rowspan="1"
                                            colspan="1" aria-label="
                                                Delete
                                            : activate to sort column ascending">Delete
                                        </th>
                                </tr>
                                </thead>
                                <tbody class="th-align">

                                </tbody>
                            </table>
                        </div>
                        <!-- END EXAMPLE TABLE PORTLET-->
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- content -->

    <!-- add modal -->
	<div class="modal fade" id="add_Modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content modal-size">
				<div class="modal-header modal-header-size">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel">Add Occasion </h4>
				</div>
				<div class="modal-body modal-body-size">
					<table  class="table form-body th-border">
						<tr class="th-border">
							<th class="th-border">name:</th>
							<td class="th-border">
                                <input type="text" name="occasion_name" class="form-control" id="occasion_name"  value="" placeholder="occasion name">
							</td>
						</tr>
						<tr>
							<th class="th-border">description:</th>
							<td class="th-border">
								<input type="text" name="occasion_description" class="form-control" id="occasion_description"  value="" placeholder="occasion description">
							</td>
						</tr>			  
					</table>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" class="form-control" data-dismiss="modal" onclick="add_occasion()">Add</button>
                    <button type="button" class="btn btn-default" class="form-control" data-dismiss="modal">Cancel</button>
				</div>
			</div>
		</div>
	</div>

    <!-- delete modal -->
    <div class="modal fade" id="delete_Modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content modal-size">
                    <div class="modal-body modal-header-size">
                        Notice!!!
                    </div>
                    <input type="hidden" id="delete_id" value="0">
                    <div class="modal-body">
                        Do you really want to delete this occasion?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" class="form-control" data-dismiss="modal" onclick="delete_occasion()">Delete</button>
                        <button type="button" class="btn btn-default" class="form-control" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
    </div>

    <!-- Message modal -->
    <div class="modal fade" id="message_Modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content modal-size">
                <div class="modal-body modal-header-size">
                    Notice!!!
                </div>
                <div class="modal-body" id="message_content">
                    Please input a occasion name.
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" class="form-control" data-dismiss="modal">OK</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="active_Modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content modal-size">
                <div class="modal-body modal-header-size">
                    Notice!!!
                </div>
                <input type="hidden" id="occ_id" value="0">
                <div class="modal-body" id="statecontent">
                    Do you really want to active this occasion?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" class="form-control" data-dismiss="modal" onclick="inactive_occasion()">OK</button>
                    <button type="button" class="btn btn-default" class="form-control" data-dismiss="modal">No</button>
                </div>
            </div>
        </div>
    </div>
    {{-- inactive product modal--}}
    <div class="modal fade" id="inactive_Modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content moda-size">
                <div class="modal-body modal-header-size">
                    Notice!!!
                </div>
                <input type="hidden" id="occ_id" value="0">
                <div class="modal-body" id="statecontent">
                    Do you really want to inactive this occasion?
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" class="form-control" data-dismiss="modal" onclick="active_occasion()">OK</button>
                    <button type="button" class="btn btn-default" class="form-control" data-dismiss="modal">No</button>
                </div>
            </div>
        </div>
    </div>


@stop

{{-- page level scripts --}}
@section('footer_scripts')

    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.buttons.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.colReorder.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.responsive.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.rowReorder.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.colVis.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.html5.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.print.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/buttons.print.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/pdfmake.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/vfs_fonts.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.scroller.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/select2/js/select2.js') }}"></script>

    <script>
        /* display table by using Datatable method  */
        var table = null;
		$(function () {
			var nEditing = null;
            table = $('#occasion_table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! route("admin.occasions.data") !!}',
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'name', name: 'name'},
                    {data: 'description', name: 'description'},
                    {data: 'created_at', name: 'creadted_date'},
                    {data: 'status', name: 'status'},
					{data: 'edit', name: 'edit', orderable: false, searchable: false},
                    {data: 'delete', name: 'delete', orderable: false, searchable: false}
                ]
            });
            table.on('draw', function () {
                $('.livicon').each(function () {
                    $(this).updateLivicon();
                });
            });

            /*  Cancel functionality in row */
            function restoreRow(table, nRow) {
                var aData = table.row(nRow).data();
                var jqTds = $('>td', nRow);
                for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
                    table.cell().data(aData[i], nRow, i, false);
                }
                table.draw(false);
            }
			/*  in Javascript add HTML(button): append('-')  */
            $('#occasion_table_length').append('<button type="button" class="form-control button-sm add-btn" data-toggle="modal" data-target="#add_Modal">Add</button >');
            						
			/*  Edit functionality  */
            var row_id, name, description, created_date;

            function editRow(table, nRow) {
                var aData = table.row(nRow).data();
                var jqTds = $('>td', nRow);
                row_id = aData.id ? aData.id : '';
                name = aData.name ? aData.name : '';
                description = aData.description ? aData.description : '';        

                jqTds[0].innerHTML = row_id;
                jqTds[1].innerHTML = '<input type="text" name="name" id="name" class="form-control input-small" value="' + name + '">';
                jqTds[2].innerHTML = '<input type="text" name="description" id="description" class="form-control input-small" value="' + description + '">';
                jqTds[5].innerHTML = '<a class="edit" href="">Save</a>';
                jqTds[6].innerHTML = '<a class="cancel" href="">Cancel</a>';
            }

            /*  Save functionality  */
            function saveRow(table, nRow) {
                var jqInputs = $('input', nRow);
                name = jqInputs[0].value;
                description = jqInputs[1].value;

				var aData = table.row(nRow).data();
                var jqTds = $('>td', nRow);
                row_id = aData.id ? aData.id : '';
				
                var tableData = 'name=' + name + '&description=' + description +
                        '&_token=' + $('meta[name=_token]').attr('content');

				$.ajax({
                    type: "post",
                    url: '/admin/occasions/' + row_id + '/update',
                    data: tableData,
                    success: function (result) {
                        console.log('result is' + result);
                        table.draw(false);
                    },
                    error: function (result) {
                        console.log(result)
                    }
                });
            }

            /*  Cancel Edit functionality  */
            function cancelEditRow(table, nRow) {
                var aData = table.row(nRow).data();
                var jqTds = $('>td', nRow);

                for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
                    table.cell().data(aData[i], nRow, i, false);
                }
                table.draw(false);
            }

            /*  When clicked on Delete button   */
            table.on('click', '.delete', function (e) {
                e.preventDefault();
                var nRow = $(this).parents('tr')[0];
                var aData = table.row(nRow).data();
                var jqTds = $('>td', nRow);
                var id = aData.id;
                console.log(id);
                $('#delete_id').val(id);
                $('#delete_Modal').modal('show');
            });

            /*  When clicked on cancel button  */
            table.on('click', '.cancel', function (e) {
                e.preventDefault();

                restoreRow(table, nEditing);
                nEditing = null;

            });

            /*  When clicked on edit button  */
            table.on('click', '.edit', function (e) {
                e.preventDefault();

                var nRow = $(this).parents('tr')[0];
                console.log(nRow);
                console.log(nEditing);
                if (nEditing !== null && nEditing != nRow) {
                    alert('You are already editing a row, you must save or cancel that row before editing a new row');
                } else if (nEditing == nRow && this.innerHTML == "Save") {
                    saveRow(table, nEditing);
                    nEditing = null;

                } else {
                    editRow(table, nRow);
                    nEditing = nRow;
                }
            });

            /*
             When clicked on active button
             */
            table.on('click', '.active', function (e) {
                e.preventDefault();
                var nRow = $(this).parents('tr')[0];
                var aData = table.row(nRow).data();
                var jqTds = $('>td', nRow);
                var id = aData.id;
                console.log(id);
                $('#occ_id').val(id);
                $('#inactive_Modal').modal('show');
            })

            /*
             When clicked on inactive button
             */
            table.on('click', '.inactive', function (e) {
                e.preventDefault();
                var nRow = $(this).parents('tr')[0];
                var aData = table.row(nRow).data();
                var jqTds = $('>td', nRow);
                var id = aData.id;
                console.log(id);
                $('#occ_id').val(id);
                $('#active_Modal').modal('show');
            })
        });
    </script>

    /*  add and delete occasion */
    <script>
        //delete occasion
        function delete_occasion(){
            var id = $('#delete_id').val();
            $.ajax({
                type: "get",
                url: '/admin/occasions/' + id + '/delete',
                success: function (result) {
                    if(result == ''){
                        console.log('row ' + result + ' deleted');
                        table.draw(false);
                        $('#message_content').html('Successfully deleted.');
                    }else{
                        $('#message_content').html('This is using in '+result);
                    }
                    $('#message_Modal').modal('show');
                    location.reload();
                },
                error: function (result) {
                    console.log(result)
                }
            });
        }

  /*   add occasion */
        function add_occasion () {
            var name = $("#occasion_name").val();
            var description = $("#occasion_description").val();
            if (name == "") {
                $('#message_content').html('Please input occasion name.');
                $('#message_Modal').modal('show');
                return;
            }

            var data = {
              name: name,
              description: description,
              _token:$('meta[name=_token]').attr('content')
            };

            $.ajax({
                type: "get",
                url: '/admin/occasions/add_occasion',
                data: data,
                success: function (result) {
                    if(result == 1){
                        $('#message_content').html('This occasion was already registered.');
                        $('#message_Modal').modal('show');
                    } else {
                        table.draw(false);
                    }
                },
                error: function (result) {
                    console.log(result)
                }
            });
        }


        /* active and inactive product */
        function active_occasion(){
            var id = $('#occ_id').val();
            $.ajax({
                type: "get",
                url: '/admin/occasions/' + id + '/active_occasion',
                success: function (result) {
                    console.log('row ' + result + ' actived');
                    location.reload();
                },
                error: function (result) {
                    console.log(result)
                }
            });
        }

        function inactive_occasion(){
            var id = $('#occ_id').val();
            console.log(id);
            $.ajax({
                type: "get",
                url: '/admin/occasions/' + id + '/inactive_occasion',
                success: function (result) {
                    console.log('row ' + result + ' inactived');
                    location.reload();
                },
                error: function (result) {
                    console.log(result)
                }
            });
        }
    </script>

@stop
