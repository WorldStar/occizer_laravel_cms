<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'occ_categories';

    protected $guarded  = ['id'];

}
