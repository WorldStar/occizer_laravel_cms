<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    protected $table = 'occ_tickets';

    protected $guarded  = ['id'];

}
