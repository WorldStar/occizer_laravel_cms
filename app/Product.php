<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'occ_products';

    protected $guarded  = ['id'];

}
