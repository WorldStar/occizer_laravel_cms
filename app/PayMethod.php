<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Method extends Model
{
    protected $table = 'occ_cities';

    protected $guarded  = ['id'];

}
