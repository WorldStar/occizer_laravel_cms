<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $table = 'occ_countries';

    protected $guarded  = ['id'];

}
