<?php

namespace App\Http\Controllers;

use App\Occasion;
use App\Http\Requests;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Http\Request;
use Datatables;
use App\User;
use DB;
class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */

    public function getlist()
    {
        if ($_SESSION['userrole'] == 1) {
            $customers = DB::table('role_users as ru')
                ->join('users as u', 'u.id', '=', 'ru.user_id')
                ->select('u.*')->where('ru.role_id', 3)->orderby('u.created_at', 'desc')->get();
            return View('admin/customer/customer', compact('customers'));
        } else {
            $user_id = Sentinel::getUser()->id;
            $customers1 = DB::table('occ_products as p')
                ->join('occ_gifts_items as i', 'i.product_id', '=', 'p.id')
                ->join('occ_gifts as g', 'g.order_id', '=', 'i.order_id')
                ->join('users as u', 'u.id', '=', 'g.customer_id')
                ->select('u.*')->where('p.admin_id', $user_id)->orderby('i.id', 'desc')->get();
            $customers2 = array_map('json_encode', $customers1);
            $customers3 = array_unique($customers2);
            $customers = array_map('json_decode', $customers3);
            return View('admin/customer/customer', compact('customers'));
        }
    }

    public function getCustomer($id = 0)
    {
        $customer = User::find($id);
        return json_encode($customer);
    }

    public function inactiveCustomer($id = 0, $status)
    {
        if($status == 1)
            $status = 0;
        else
            $status = 1;
        DB::table('users')->where('id', $id)->update(['status'=>$status]);
        $data = array("status"=>$status);
        return $data;
    }
}

