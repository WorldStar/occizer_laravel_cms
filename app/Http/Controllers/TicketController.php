<?php

namespace App\Http\Controllers;

use  App\Http\Requests;
use Illuminate\Http\Request;
use DB;
use App\Ticket;
use Datatables;
use Validator;
use URL;
use Redirect;
use Sentinel;
use collection;
use Illuminate\Database\MySqlConnection;
use Illuminate\Database\Query\Builder;

class TicketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */

    public function getlist()
    {
        $tickets = Ticket::orderby('id', 'asc')->where('status', 1)->get();
        return View('admin.ticket.paidlist', compact('tickets', 'orderitems'));
    }

    public function getpaidTicket()
    {
        if ($_SESSION['userrole'] == 1) {
            $tickets = Ticket::orderby('id', 'asc')->where('status', 1)->orderby('ticket_id', 'asc')->get();
            $tickets = collect($tickets);
        } else {
            $login_id = Sentinel::getUser()->id;
            $tickets = DB::table('occ_tickets')->where('vendor_id', $login_id)->where('status', 1)->orderby('ticket_id', 'asc')->get();
            $tickets = collect($tickets);
        }
        return Datatables::of($tickets)
            ->edit_column('productlist', function ($data) {
                $login_id = Sentinel::getUser()->id;
                if ($_SESSION['userrole'] == 1) {
                    $products = DB::table('occ_tickets as t')
                        ->join('occ_gifts_items as i', 'i.order_id', '=', 't.order_id')
                        ->join('occ_products as p', 'p.id', '=', 'i.product_id')
                        ->select('p.id', 'p.name', 'i.product_size', 'i.amount as order_amount')->where('i.order_id', $data->order_id)
                        ->where('t.status', 1)->where('i.status', 1)->orderby('p.id', 'asc')->get();
                } else {
                    $products = DB::table('occ_tickets as t')
                        ->join('occ_gifts_items as i', 'i.order_id', '=', 't.order_id')
                        ->join('occ_products as p', 'p.id', '=', 'i.product_id')
                        ->select('p.id', 'p.name', 'i.product_size', 'i.amount as order_amount')->where('p.admin_id', $login_id)
                        ->where('i.order_id', $data->order_id)->where('t.status', 1)->where('i.status', 1)->orderby('p.id', 'asc')->get();
                }

                $productlist = array();
                for ($i = 0; $i < count($products); $i++) {
                    if (!empty($products[$i])) {
                        if ($i == 0) {
                            $productlist = array($products[$i]->name . '*' . $products[$i]->order_amount);
                        } else {
                            if (($products[$i]->name) == ($products[$i - 1]->name)) {
                                $key1 = array_pop($productlist);
                                $key = explode('*', $key1);
                                $amount = ($products[$i]->order_amount) + $key[1];
                                $list = $key[0] . '*' . $amount;
                                array_push($productlist, $list);
                            } else {
                                array_push($productlist, ($products[$i]->name) . '*' . ($products[$i]->order_amount));
                            }
                        }
                    }
                }
                $productlist = implode('<br>', $productlist);
                return '<a href="/admin/ticket/detail/' . $data->ticket_id . '">' . $productlist . '</a>';
            })
            ->editColumn('description', function ($data) {
                $description = $data->description;
                return '<a href="/admin/ticket/detail/' . $data->description . '">' . $description . '</a>';
            })
            ->editColumn('ticket_id', function ($data) {
                $ticket_id = $data->ticket_id;
                return '<a href="/admin/ticket/detail/' . $data->ticket_id . '">' . $ticket_id . '</a>';
            })
            ->edit_column('customer_id', function ($data) {
                $customer_id = $data->customer_id;
                $user = DB::table('users')->where('id', $customer_id)->first();
                $customername = $user->first_name . ' ' . $user->last_name;
                return '<a href="/admin/ticket/detail/' . $data->ticket_id . '">' . $customername . '</a>';
            })
            ->edit_column('totalprice', function ($data) {
                $vendor = DB::table('users')->where('id', $data->vendor_id)->first();
                $vendor_country = DB::table('occ_countries')->where('id', $vendor->country_id)->first();
                $currency = $vendor_country->currency;
                $totalprice = ($data->totalprice).''.$currency;
                return $totalprice;
            })
            ->edit_column('vendor_id', function ($data) {
                $vendor = DB::table('users')->where('id', $data->vendor_id)->first();
                $vendor_name = $vendor->first_name.' '.$vendor->last_name;
                return  $vendor_name;
            })
            ->editColumn('created_at', function ($data) {
                return date('H:M/d/Y', strtotime($data->created_at));
            })
            ->edit_column('shipper_id', function ($data) {
                $vendor = DB::table('users')->where('id', $data->vendor_id)->first();
                $shipper_id = $vendor->shipper_id;
                if ($shipper_id == 0) {
                    $shipper_name = 'Vendor Delivers';
                } else {
                    $shipper = DB::table('occ_shippers')->where('id', $shipper_id)->first();
                    $shipper_name = $shipper->name;
                }
                return $shipper_name ;
            })
            ->add_column('payment_method', function ($data) {
                if (($data->payment_method) == 1) {
                    $platform = 'Paypal';
                    return $platform;
                } elseif (($data->payment_method) == 2) {
                    $platform = 'Credit Card';
                    return $platform;
                } else {
                    $platform = 'Cash on Delivery';
                    return $platform;
                }
            })
            ->add_column('platform', function ($data) {
                $order = DB::table('occ_gifts')->where('order_id', $data->order_id)->first();
                if (($order->platform_number) == 1) {
                    $platform = 'Website';
                    return $platform;
                } else {
                    $platform = 'Mobile';
                    return $platform;
                }
            })
            ->add_column('status', function ($data) {
                return '<a href="javascript:cancelModal(' . $data->ticket_id . ')"><i class="livicon del-color" data-name="trash" data-size="18"
                      data-c="#ff173d" data-hc="#ff173d" data-loop="true"></i>Cancel</a>';
            })
            ->make(true);
    }

    public function cancel_ticket($id = 0)
    {
        $ticket = DB::table('occ_tickets')->where('ticket_id', $id)->first();
        $order_id = $ticket->order_id;
        if ($_SESSION['userrole'] == 1) {
            $products = DB::table('occ_gifts_items')->where('order_id', $order_id)->where('status', 1)->get();
            foreach ($products as $product) {
                DB::table('occ_products')->where('id', $product->product_id)->increment('amount', $product->amount);
                DB::table('occ_gifts_items')->where('order_id', $order_id)->where('product_id', $product->product_id)->update(array('status'=> -1));
            }
        } else {
            $login_id = Sentinel::getUser()->id;
            $products = DB::table('occ_tickets as t')
                ->join('occ_gifts_items as i', 'i.order_id', '=', 't.order_id')
                ->join('occ_products as p', 'p.id', '=', 'i.product_id')
                ->select('i.amount as order_amount', 'i.product_size', 'i.order_id as order_id', 'p.*')->where('t.ticket_id', $id)
                ->where('p.admin_id', $login_id)->orderby('i.id', 'asc')->get();

            foreach ($products as $product) {
                DB::table('occ_products')->where('id', $product->id)->where('admin_id', $login_id)->increment('amount', $product->order_amount);
                DB::table('occ_gifts_items')->where('order_id', $product->order_id)->where('product_id', $product->id)
                    ->where('status', 1)->update(array('status'=> -1));
            }
            DB::table('occ_gifts')->where('order_id', $order_id)->update(array('status'=> -1));
            DB::table('occ_tickets')->where('ticket_id', $id)->update(array('status'=> 0));
        }
        // in the case that there are some cancelled products, get origin ticket with status=0;
        $already_cancelled = DB::table('occ_gifts_items')->where('order_id', $order_id)->where('status', -1)->get();
        if(empty($already_cancelled)){
            DB::table('occ_gifts')->where('order_id', $order_id)->update(array('status'=> -1));
            DB::table('occ_tickets')->where('ticket_id', $id)->update(array('status'=> 0));
        }else{
            DB::table('occ_gifts')->where('order_id', $order_id)->update(array('status'=> -1));
            $order = DB::table('occ_gifts')->where('order_id', $order_id)->first();
            DB::table('occ_tickets')->where('ticket_id', $id)->update(array('totalprice'=> $order->totalprice));
            DB::table('occ_tickets')->where('ticket_id', $id)->update(array('status'=> 0));
        }
        return $id;
    }

    public function cancel_product($id = 0, $items_id = 0)
    {
        $product1 = DB::table('occ_gifts_items')->where('id', $items_id)->first();
        $product2 = DB::table('occ_products')->where('id', $product1->product_id)->first();
        if ($product1->product_size == 1) {
            $price = $product2->small_size_price;
        } elseif ($product1->product_size == 2) {
            $price = $product2->medium_size_price;
        } else {
            $price = $product2->price;
        }
        $decrease_totalprice = $price * ($product1->amount);
        DB::table('occ_products')->where('id', $product1->product_id)->increment('amount', $product1->amount);
        DB::table('occ_gifts_items')->where('id', $items_id)->where('status', 1)->update(array('status'=> -1));
        DB::table('occ_tickets')->where('ticket_id', $id)->decrement('totalprice', $decrease_totalprice);

        $ticket = DB::table('occ_tickets')->where('ticket_id', $id)->first();

        if ($ticket->totalprice == 0) {
            DB::table('occ_tickets')->where('ticket_id', $id)->update(array('status'=> 0));
            $order = DB::table('occ_gifts')->where('order_id', $ticket->order_id)->first();
            DB::table('occ_tickets')->where('ticket_id', $id)->update(array('totalprice'=> $order->totalprice));
            DB::table('occ_gifts')->where('order_id', $ticket->order_id)->update(array('status'=> -1));
        }
        return $id;
    }

    public function detail($id = 0)
    {
        if ($_SESSION['userrole'] == 1) {
            $products = DB::table('occ_tickets as t')
                ->join('occ_gifts_items as i', 'i.order_id', '=', 't.order_id')
                ->join('occ_products as p', 'i.product_id', '=', 'p.id')
                ->select('p.*', 'i.id as items_id', 'i.amount as order_amount', 'i.product_size', 'i.photo as customize_photo',
                    'i.description as customize_text', 'i.delivery_date')
                ->where('t.ticket_id', $id)->where('i.status', 1)->orderby('p.id', 'asc')->get();
        } else {
            $login_id = Sentinel::getUser()->id;
            $products = DB::table('occ_tickets as t')
                ->join('occ_gifts_items as i', 'i.order_id', '=', 't.order_id')
                ->join('occ_products as p', 'i.product_id', '=', 'p.id')
                ->select('p.*', 'i.id as items_id', 'i.amount as order_amount', 'i.product_size', 'i.photo as customize_photo',
                    'i.description as customize_text', 'i.delivery_date')
                ->where('t.ticket_id', $id)->where('p.admin_id', $login_id)->where('i.status', 1)
                ->orderby('p.id', 'asc')->get();
        }
        $user = DB::table('users as u')
            ->join('occ_tickets as t', 't.customer_id', '=', 'u.id')
            ->select('u.*')->where('t.ticket_id', $id)->first();
        return View(('admin.ticket.detail'), compact('id', 'user', 'products'));
    }



    public function get_shippedlist()
    {
        $tickets = Ticket::orderby('id', 'asc')->where('status', 2)->get();
        return View('admin.ticket.shippedlist', compact('tickets', 'orderitems'));
    }

    public function get_shippedTicket()
    {
        if ($_SESSION['userrole'] == 1) {
            $tickets = Ticket::orderby('id', 'asc')->where('status', 2)->orderby('ticket_id', 'asc')->get();
            $tickets = collect($tickets);
        } else {
            $login_id = Sentinel::getUser()->id;
            $tickets = DB::table('occ_tickets')->where('vendor_id', $login_id)->where('status', 2)->orderby('ticket_id', 'asc')->get();
            $tickets = collect($tickets);
        }
        return Datatables::of($tickets)
            ->edit_column('productlist', function ($data) {
                $login_id = Sentinel::getUser()->id;
                if ($_SESSION['userrole'] == 1) {
                    $products = DB::table('occ_tickets as t')
                        ->join('occ_gifts_items as i', 'i.order_id', '=', 't.order_id')
                        ->join('occ_products as p', 'p.id', '=', 'i.product_id')
                        ->select('p.id', 'p.name', 'i.product_size', 'i.amount as order_amount')->where('i.order_id', $data->order_id)
                        ->where('t.status', 2)->where('i.status', 1)->orderby('p.id', 'asc')->get();
                } else {
                    $products = DB::table('occ_tickets as t')
                        ->join('occ_gifts_items as i', 'i.order_id', '=', 't.order_id')
                        ->join('occ_products as p', 'p.id', '=', 'i.product_id')
                        ->select('p.id', 'p.name', 'i.product_size', 'i.amount as order_amount')->where('p.admin_id', $login_id)
                        ->where('i.order_id', $data->order_id)->where('t.status', 2)->where('i.status', 1)->orderby('p.id', 'asc')->get();
                }

                $productlist = array();
                for ($i = 0; $i < count($products); $i++) {
                    if (!empty($products[$i])) {
                        if ($i == 0) {
                            $productlist = array($products[$i]->name . '*' . $products[$i]->order_amount);
                        } else {
                            if (($products[$i]->name) == ($products[$i - 1]->name)) {
                                $key1 = array_pop($productlist);
                                $key = explode('*', $key1);
                                $amount = ($products[$i]->order_amount) + $key[1];
                                $list = $key[0] . '*' . $amount;
                                array_push($productlist, $list);
                            } else {
                                array_push($productlist, ($products[$i]->name) . '*' . ($products[$i]->order_amount));
                            }
                        }
                    }
                }
                $productlist = implode('<br>', $productlist);
                return '<a href="/admin/ticket/detail/' . $data->ticket_id . '">' . $productlist . '</a>';
            })
            ->editColumn('description', function ($data) {
                $description = $data->description;
                return '<a href="/admin/ticket/detail/' . $data->description . '">' . $description . '</a>';
            })
            ->editColumn('ticket_id', function ($data) {
                $ticket_id = $data->ticket_id;
                return '<a href="/admin/ticket/detail/' . $data->ticket_id . '">' . $ticket_id . '</a>';
            })
            ->edit_column('customer_id', function ($data) {
                $customer_id = $data->customer_id;
                $user = DB::table('users')->where('id', $customer_id)->first();
                $customername = $user->first_name . ' ' . $user->last_name;
                return '<a href="/admin/ticket/detail/' . $data->ticket_id . '">' . $customername . '</a>';
            })
            ->edit_column('totalprice', function ($data) {
                $vendor = DB::table('users')->where('id', $data->vendor_id)->first();
                $vendor_country = DB::table('occ_countries')->where('id', $vendor->country_id)->first();
                $currency = $vendor_country->currency;
                $totalprice = ($data->totalprice).''.$currency;
                return $totalprice;
            })
            ->edit_column('vendor_id', function ($data) {
                $vendor = DB::table('users')->where('id', $data->vendor_id)->first();
                $vendor_name = $vendor->first_name.' '.$vendor->last_name;
                return  $vendor_name;
            })
            ->editColumn('created_at', function ($data) {
                return date('H:M/d/Y', strtotime($data->created_at));
            })
            ->edit_column('shipper_id', function ($data) {
                $vendor = DB::table('users')->where('id', $data->vendor_id)->first();
                $shipper_id = $vendor->shipper_id;
                if ($shipper_id == 0) {
                    $shipper_name = 'Vendor Delivers';
                } else {
                    $shipper = DB::table('occ_shippers')->where('id', $shipper_id)->first();
                    $shipper_name = $shipper->name;
                }
                return $shipper_name ;
            })
            ->add_column('payment_method', function ($data) {
                if (($data->payment_method) == 1) {
                    $platform = 'Paypal';
                    return $platform;
                } elseif (($data->payment_method) == 2) {
                    $platform = 'Credit Card';
                    return $platform;
                } else {
                    $platform = 'Cash on Delivery';
                    return $platform;
                }
            })
            ->add_column('platform', function ($data) {
                $order = DB::table('occ_gifts')->where('order_id', $data->order_id)->first();
                if (($order->platform_number) == 1) {
                    $platform = 'Website';
                    return $platform;
                } else {
                    $platform = 'Mobile';
                    return $platform;
                }
            })
            ->add_column('status', function ($data) {
                return '<a href="javascript:cancelModal(' . $data->ticket_id . ')"><i class="livicon del-color" data-name="trash" data-size="18"
                      data-c="#ff173d" data-hc="#ff173d" data-loop="true"></i>Cancel</a>';
            })
            ->make(true);
    }


    public function get_cancelledlist()
    {
        $tickets = DB::table('occ_gifts_items')->where('status', -1)->orderby('id', 'asc')->get();
        return View('admin.ticket.cancelledlist', compact('tickets', 'orderitems'));
    }

    public function get_cancelledTicket()
    {
        if ($_SESSION['userrole'] == 1) {
            $cancelled_tickets = DB::table('occ_gifts_items')->where('status', -1)->orderby('id','asc')->get();
            $cancelled_tickets = collect($cancelled_tickets);
        } else {
            $login_id = Sentinel::getUser()->id;
            $cancelled_tickets = DB::table('occ_gifts_items as i')
                ->join('occ_products as p', 'p.id','=','i.product_id')
                ->select('i.*')->where('p.admin_id', $login_id)->where('i.status', -1)->orderby('i.id', 'asc')->get();
            $cancelled_tickets = collect($cancelled_tickets);
        }
        return Datatables::of($cancelled_tickets)
            ->edit_column('productlist', function ($data) {
                $product = DB::table('occ_products')->where('id', $data->product_id)->first();
                if($data->product_size == 1) {
                    $name = $product->small_size_display_name;
                }
                if($data->product_size == 2){
                    $name = $product->medium_size_display_name;
                }
                if($data->product_size == 3){
                    if($product->has_large_size == 0){
                        $name = $product->name;
                    }else{
                        $name = $product->large_size_display_name;
                    }
                }
                return $name;
            })
            ->editColumn('description', function ($data) {
                $description = $data->description;
                return $description;
            })
            ->editColumn('ticket_id', function ($data) {
                $ticket = DB::table('occ_tickets')->where('order_id',$data->order_id)->first();
                $ticket_id = $ticket->ticket_id;
                return $ticket_id;
            })
            ->edit_column('customer_id', function ($data) {
                $ticket = DB::table('occ_tickets')->where('order_id', $data->order_id)->first();
                $customer_id = $ticket->customer_id;
                $user = DB::table('users')->where('id', $customer_id)->first();
                $customername = $user->first_name . ' ' . $user->last_name;
                return $customername;
            })
            ->edit_column('totalprice', function ($data) {
                $ticket = DB::table('occ_tickets')->where('order_id', $data->order_id)->first();
                $vendor = DB::table('users')->where('id', $ticket->vendor_id)->first();
                $vendor_country = DB::table('occ_countries')->where('id', $vendor->country_id)->first();
                $currency = $vendor_country->currency;

                $product = DB::table('occ_products')->where('id', $data->product_id)->first();
                if($data->product_size == 1) {
                    $price = $product->small_size_price;
                }elseif($data->product_size == 2){
                    $price = $product->medium_size_price;
                }else {
                    $price = $product->price;
                }
                $totalprice = (($data->amount)*$price).''.$currency;
                return $totalprice;
            })
            ->edit_column('vendor_id', function ($data) {
                $ticket = DB::table('occ_tickets')->where('order_id', $data->order_id)->first();
                $vendor = DB::table('users')->where('id', $ticket->vendor_id)->first();
                $vendor_name = $vendor->first_name.' '.$vendor->last_name;
                return  $vendor_name;
            })
            ->editColumn('created_at', function ($data) {
                return date('H:M/d/Y', strtotime($data->created_at));
            })
            ->edit_column('shipper_id', function ($data) {
                $ticket = DB::table('occ_tickets')->where('order_id', $data->order_id)->first();
                $vendor = DB::table('users')->where('id', $ticket->vendor_id)->first();
                $shipper_id = $vendor->shipper_id;
                if ($shipper_id == 0) {
                    $shipper_name = 'Vendor Delivers';
                } else {
                    $shipper = DB::table('occ_shippers')->where('id', $shipper_id)->first();
                    $shipper_name = $shipper->name;
                }
                return $shipper_name ;
            })
            ->add_column('payment_method', function ($data) {
                $ticket = DB::table('occ_tickets')->where('order_id', $data->order_id)->first();
                if (($ticket->payment_method) == 1) {
                    $platform = 'Paypal';
                    return $platform;
                } elseif (($ticket->payment_method) == 2) {
                    $platform = 'Credit Card';
                    return $platform;
                } else {
                    $platform = 'Cash on Delivery';
                    return $platform;
                }
            })
            ->add_column('platform', function ($data) {
                $order = DB::table('occ_gifts')->where('order_id', $data->order_id)->first();
                if (($order->platform_number) == 1) {
                    $platform = 'Website';
                    return $platform;
                } else {
                    $platform = 'Mobile';
                    return $platform;
                }
            })
            ->add_column('status', function ($data) {
                return '<a href="javascript:delete_Modal(' . $data->id . ')"><i class="livicon del-color" data-name="trash" data-size="18"
                      data-c="#ff173d" data-hc="#ff173d" data-loop="true"></i>Delete</a>';
            })
            ->make(true);
    }

    public function cancelled_detail($id = 0)
    {
        if ($_SESSION['userrole'] == 1) {
            $products = DB::table('occ_tickets as t')
                ->join('occ_gifts_items as i', 'i.order_id', '=', 't.order_id')
                ->join('occ_products as p', 'i.product_id', '=', 'p.id')
                ->select('p.*', 'i.id as items_id', 'i.amount as order_amount', 'i.product_size', 'i.photo as customize_photo',
                    'i.description as customize_text', 'i.delivery_date')
                ->where('t.ticket_id', $id)->where('t.status', 0)->where('i.status', -1)->orderby('p.id', 'asc')->get();
        } else {
            $login_id = Sentinel::getUser()->id;
            $products = DB::table('occ_tickets as t')
                ->join('occ_gifts_items as i', 'i.order_id', '=', 't.order_id')
                ->join('occ_products as p', 'i.product_id', '=', 'p.id')
                ->select('p.*', 'i.id as items_id', 'i.amount as order_amount', 'i.product_size', 'i.photo as customize_photo',
                    'i.description as customize_text', 'i.delivery_date')
                ->where('t.ticket_id', $id)->where('p.admin_id', $login_id)
                ->where('t.status', 0)->where('i.status', -1)->orderby('p.id', 'asc')->get();
        }
        $user = DB::table('users as u')
            ->join('occ_tickets as t', 't.customer_id', '=', 'u.id')
            ->select('u.*')->where('t.ticket_id', $id)->first();
        return View(('admin.ticket.canceldetail'), compact('id', 'user', 'products'));
    }

    public function delete_cancelled($id = 0)
    {
        $item = DB::table('occ_gifts_items')->where('id', $id)->first();
        DB::table('occ_gifts_items')->where('id', $id)->delete();
        $items = DB::table('occ_gifts_items')->where('order_id', $item->order_id)->get();
        if(empty($items)){
            DB::table('occ_tickets')->where('order_id', $item->order_id)->delete();
            DB::table('occ_gifts')->where('order_id', $item->order_id)->delete();
        }
        return $id;
    }
}
