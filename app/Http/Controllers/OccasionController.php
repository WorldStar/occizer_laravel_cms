<?php

namespace App\Http\Controllers;

use App\Occasion;
use App\Http\Requests;
use Illuminate\Http\Request;
use Datatables;
use DB;
use App\Http\Controllers\sweetAlert;

class OccasionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */

    public function getlist()
    {
        $tables = Occasion::orderBy('id', 'asc')->get();
        return View('admin/occasion/occasion', compact('tables'));
    }

    public function occasion_data()
    {
        $tables = Occasion::get(['id', 'name', 'description',  'status',  'created_at']);

        return Datatables::of($tables)
            ->edit_column('status', function ($data) {
                if ($data->status == 1) {
                    $status = 'active';
                    return '<a style="color: #ca0002" class="active" href="javascript:;">' . $status . '</a>';
                    //return '<a onmouseover="this.style.color=\'#0618d8\" onMouseOut="this.style.color=\'#d80b06\'"> '. $status .'</span>';
                } else {
                    $status = 'inactive';
                    return '<a class="inactive" href="javascript:;">' . $status . '</a>';
                    //return '<a onmouseover="this.style.color=\'#0618d8\'" onMouseOut="this.style.color=\'#d80b06\'" class="active" href="javascript:;">' . $status . '</a>';
                }
            })
            ->editColumn('created_at', function ($data) {
                return date('M d, Y', strtotime($data->created_at));
            })
            ->add_column('edit', '<a class="edit" href="javascript:;">
                <i class="livicon edit-color" data-name="edit" data-size="18" data-c="#2e16ff" data-hc="#2e16ff" data-loop="true"></i>Edit</a>')
            ->add_column('delete', '<a class="delete" href="javascript:;">
                <i class="livicon del-color" data-name="trash" data-size="18"
                            data-c="#ff173d" data-hc="#ff173d" data-loop="true"></i>Delete</a>')
            ->make(true);
    }

    public function occasion_update(Request $request, $id)
    {
        $table = Occasion::find($id);

        $table->update($request->except('_token'));
        return $table->id;
    }

    public function occasion_delete($id)
    {
        $products = DB::table('occ_products')->select('name', 'occ_id')->get();;
        //print_r($products);return;
        $product_name ='';
        foreach ($products as $product) {
            $occasion1 = $product->occ_id;
            $occasion = explode(',', $occasion1);
            if(in_array($id, $occasion)) {
                if(empty($product_name)){
                    $product_name = ($product->name);
                } else {
                    $product_name .= ', '.($product->name);
                }
            }
        }
        if (empty($product_name)){
            DB::table('occ_occasions')->where('id', $id)->delete();
        } else {
            return $product_name;
        }
    }

    public function add_occasion(Request $request)
    {
        $name = $_GET['name'];
        $occasion = DB::table('occ_occasions')->where('name',$name)->first();
        if(empty($occasion)) {
            $tables = array();
            if ($request->ajax()) {

                $tables = Occasion::create($request->except('_token'));
            }
            return $tables;
        } else {
            $duplicate = 1;
            return $duplicate;
        }
    }

    public function active_occasion($id = 0)
    {
        DB::table('occ_occasions')->where('id', $id)->update(['status'=>0]);
        $data = array("status"=>0);
        return $data;
    }

    public function inactive_occasion($id = 0)
    {
        DB::table('occ_occasions')->where('id', $id)->update(['status'=>1]);
        $data = array("status"=>1);
        return $data;
    }
}

