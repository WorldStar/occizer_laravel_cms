<?php

namespace App\Http\Controllers;

use App\Country;
use App\Http\Requests;
use Illuminate\Http\Request;
use Datatables;
use DB;
use App\Http\Controllers\sweetAlert;

class CountryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */

    public function getlist()
    {
        $tables = Country::orderBy('id', 'asc')->get();
        return View('admin/country/countrylist', compact('tables'));
    }

    public function get_data()
    {
        if($_SESSION['userrole'] == 1) {
            $tables = Country::get(['id', 'name', 'phone_prefix', 'currency', 'status', 'created_at']);
        } else {
            $tables = Country::where('status', 1)->get(['id', 'name', 'phone_prefix', 'currency', 'status', 'created_at']);
        }
        return Datatables::of($tables)
            ->editColumn('created_at', function ($data) {
                return date('M d, Y', strtotime($data->created_at));
            })
            ->edit_column('status', function ($data) {
                if($data->status == 1) {
                    $status = 'active';
                    return '<a style="color: #ca0002" class="active" href="javascript:;">' . $status . '</a>';
                }else {
                    $status = 'inactive';
                    return '<a class="inactive" href="javascript:;">' . $status . '</a>';
                }
            })
            ->add_column('edit', '<a class="edit" href="javascript:;">
                <i class="livicon edit-color" data-name="edit" data-size="18"
                data-c="#2e16ff" data-hc="#2e16ff" data-loop="true"></i>Edit</a>')
            ->add_column('delete', '<a class="delete" href="javascript:;">
                <i class="livicon del-color" data-name="trash" data-size="18"
                data-c="#ff173d" data-hc="#ff173d" data-loop="true"></i>Delete</a>')
            ->make(true);
    }

    public function country_update(Request $request)
    {
        $id= $_GET['id'];
        $name= $_GET['name'];
        $name = ucfirst($name);
        $phone_prefix = $_GET['phone_prefix'];
        $currency = $_GET['currency'];
        $currency = strtoupper($currency);
        DB::table('occ_countries')->where('id', $id)->update(array('name'=>$name, 'phone_prefix'=>$phone_prefix, 'currency'=>$currency));
        return $id;
    }

    public function country_delete($id)
    {
        $users = DB::table('users as u')
            ->join('role_users as ru', 'u.id','=','ru.user_id')
            ->select('first_name', 'last_name','country_id')->orderby('id', 'asc')->get();

        $user_name = '';
        foreach ($users as $user) {
            $country_id = $user->country_id;
            if($country_id == $id) {
                if(empty($vendor_name)){
                    $user_name = $user->first_name.' '.$user->last_name;
                } else {
                    $user_name  .= ', '.$user->first_name.' '.$user->last_name;
                }
            }
        }
        if (empty($user_name)){
            DB::table('occ_countries')->where('id', $id)->delete();
            DB::table('occ_cities')->where('country_id', $id)->delete();
        } else {
            return $user_name;
        }
    }

    public function add_country(Request $request)
    {
        $name = $_GET['name'];
        $name = ucfirst($name);
        $phone_prefix = $_GET['phone_prefix'];
        $currency = $_GET['currency'];
        $currency = strtoupper($currency);
        $country = DB::table('occ_countries')->where('name', $name)->first();
        if(is_null($country)) {
            DB::table('occ_countries')->insert(array('name'=>$name, 'phone_prefix'=>$phone_prefix, 'currency'=>$currency));
        } else {
            $duplicate = 1;
            return $duplicate;
        }
    }

    public function active_country($id = 0)
    {
        DB::table('occ_countries')->where('id', $id)->update(['status'=>1]);
        return $id;
    }

    public function inactive_country($id = 0)
    {
        DB::table('occ_countries')->where('id', $id)->update(array('status'=>0));

        // inactive all cities of inactived country ;
        $cities = DB::table('occ_cities')->where('country_id', $id)->get();
        if(!empty($cities)){
            foreach($cities as $city) {
                DB::table('occ_cities')->where('id', $city->id)->update(array('status'=>0));
            }
        }
        return $id;
    }
}

