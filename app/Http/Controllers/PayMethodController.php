<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Datatables;
use DB;
use App\Http\Controllers\sweetAlert;

class PayMethodController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */

    public function getlist()
    {
        $methods = DB::table('occ_payment_methods')->orderBy('id', 'asc')->get();
        return View('admin/paymethod/method', compact('methods'));
    }

    public function get_data()
    {
        if ($_SESSION['userrole'] == 1) {
            $methods = DB::table('occ_payment_methods')->orderBy('id', 'asc')->get();
        } else {
            $methods = DB::table('occ_payment_methods')->where('status', 1)->orderBy('id', 'asc')->get();
        }
        $methods = collect($methods);
        return Datatables::of($methods)
            ->edit_column('status', function ($data) {
                if($data->status == 1) {
                    $status = 'active';
                    return '<a style="color: #ca0002" class="active" href="javascript:;">' . $status . '</a>';
                }else {
                    $status = 'inactive';
                    return '<a class="inactive" href="javascript:;">' . $status . '</a>';
                }
            })
            ->editColumn('created_at', function ($data) {
                return date('M d, Y', strtotime($data->created_at));
            })
            ->make(true);
    }

    public function active_method($id = 0)
    {
        DB::table('occ_payment_methods')->where('id', $id)->update(array('status'=>1));
        return $id;
    }

    public function inactive_method($id = 0)
    {
        DB::table('occ_payment_methods')->where('id', $id)->update(array('status'=>0));
        return $id;
    }
}

