<?php

namespace App\Http\Controllers;

use  App\Http\Requests;
use Illuminate\Http\Request;
use DB;
use App\Order;
use Datatables;
use Validator;
use URL;
use Redirect;
use Sentinel;
use collection;
use Illuminate\Database\MySqlConnection;
use Illuminate\Database\Query\Builder;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */

    public function getlist()
    {
        $orders = Order::orderby('id', 'asc')->get();
        $orderitems = DB::table('occ_gifts_items')->where('status', 0)->orderby('id', 'asc')->get();
        return View('admin.order.list', compact('orders', 'orderitems'));
    }

    public function getOrders()
    {
        if ($_SESSION['userrole'] == 1) {
            $orders = Order::orderby('id', 'asc')->where('status', 0)->get();
        } else {
            $login_id = Sentinel::getUser()->id;
            $orders = Order::where('vendor_id', $login_id)->orderby('id', 'asc')->where('status', 0)->get();
        }
        return Datatables::of($orders)
            ->edit_column('productlist', function ($data) {
                $login_id = Sentinel::getUser()->id;
                if ($_SESSION['userrole'] == 1) {
                    $products = DB::table('occ_gifts as g')
                        ->join('occ_gifts_items as i', 'i.order_id', '=', 'g.order_id')
                        ->join('occ_products as p', 'p.id', '=', 'i.product_id')
                        ->select('p.id', 'p.name', 'i.product_size', 'i.amount as order_amount')->where('i.order_id', $data->order_id)
                        ->where('i.status', 0)->orderby('p.id', 'asc')->get();
                } else {
                    $products = DB::table('occ_gifts as g')
                        ->join('occ_gifts_items as i', 'i.order_id', '=', 'g.order_id')
                        ->join('occ_products as p', 'p.id', '=', 'i.product_id')
                        ->select('p.id', 'p.name', 'i.product_size', 'i.amount as order_amount')->where('p.admin_id', $login_id)
                        ->where('i.order_id', $data->order_id)->where('i.status', 0)->orderby('p.id', 'asc')->get();
                }
                //print_r($products);return;
                $productlist = array();
                for ($i = 0; $i < count($products); $i++) {
                    if (!empty($products[$i])) {
                        if ($i == 0) {
                            $productlist = array($products[$i]->name . '*' . $products[$i]->order_amount);
                        } else {
                            if (($products[$i]->name) == ($products[$i - 1]->name)) {
                                $key1 = array_pop($productlist);
                                $key = explode('*', $key1);
                                $amount = ($products[$i]->order_amount) + $key[1];
                                $list = $key[0] . '*' . $amount;
                                array_push($productlist, $list);
                            } else {
                                array_push($productlist, ($products[$i]->name) . '*' . ($products[$i]->order_amount));
                            }
                        }
                    }
                }
                $productlist = implode('<br>', $productlist);
                return '<a href="/admin/order/detail/' . $data->order_id . '">' . $productlist . '</a>';
            })
            ->editColumn('order_id', function ($data) {
                $order_id = $data->order_id;
                return '<a href="/admin/order/detail/' . $data->order_id . '">' . $order_id . '</a>';
            })
            ->edit_column('customer_id', function ($data) {
                $id = $data->customer_id;
                $user = DB::table('users')->where('id', $id)->first();
                $customername = $user->first_name . ' ' . $user->last_name;
                return '<a href="/admin/order/detail/' . $data->order_id . '">' . $customername . '</a>';
            })
            ->edit_column('totalprice', function ($data) {
                $vendor = DB::table('users')->where('id', $data->vendor_id)->first();
                $vendor_country = DB::table('occ_countries')->where('id', $vendor->country_id)->first();
                $currency = $vendor_country->currency;
                $totalprice = ($data->totalprice).''.$currency;
                return $totalprice;
            })
            ->edit_column('vendor_id', function ($data) {
                $vendor = DB::table('users')->where('id', $data->vendor_id)->first();
                $vendor_name = ($vendor->first_name).' '.($vendor->last_name);
                return  $vendor_name;
            })
            ->editColumn('created_at', function ($data) {
                return date('H:M/d/Y', strtotime($data->created_at));
            })
            ->edit_column('shipper_id', function ($data) {
                $vendor = DB::table('users')->where('id', $data->vendor_id)->first();
                $shipper_id = $vendor->shipper_id;
                if ($shipper_id == 0) {
                        $shipper_name = 'Vendor Delivers';
                } else {
                    $shipper = DB::table('occ_shippers')->where('id', $shipper_id)->first();
                    $shipper_name = $shipper->name;
                }
                return $shipper_name ;
            })
            ->add_column('payment_method', function ($data) {
                if (($data->payment_method) == 1) {
                    $platform = 'Paypal';
                    return $platform;
                } elseif (($data->payment_method) == 2) {
                    $platform = 'Credit Card';
                    return $platform;
                } else {
                    $platform = 'Cash on Delivery';
                    return $platform;
                }
            })
            ->edit_column('delivery_location', function ($data) {
                return '<a href="https://www.google.co.kr/maps/place/' . $data->delivery_location . '" target="_blank">' . $data->delivery_location . '</a>';
            })
            ->add_column('status', function ($data) {
                return '<a href="javascript:cancelModal(' . $data->order_id . ')"><span id="order_status_' . $data->id . '" >
                        <i class="livicon del-color" data-name="trash" data-size="18"
                        data-c="#ff173d" data-hc="#ff173d" data-loop="true"></i>Cancel</span></a>';
            })
            ->add_column('platform', function ($data) {
                if (($data->platform_number) == 1) {
                    $platform = 'Website';
                    return $platform;
                } else {
                    $platform = 'Mobile';
                    return $platform;
                }
            })
            ->make(true);
    }

    public function cancelorder($id = 0)
    {
        if ($_SESSION['userrole'] == 1) {
            $products = DB::table('occ_gifts_items')->where('order_id', $id)->where('status', 0)->get();
            foreach ($products as $product) {
                DB::table('occ_products')->where('id', $product->product_id)->increment('amount', $product->amount);
            }
            DB::table('occ_gifts_items')->where('order_id', $id)->delete();
            DB::table('occ_gifts')->where('order_id', $id)->delete();
        } else {
            $login_id = Sentinel::getUser()->id;
            $products = DB::table('occ_gifts_items as i')
                ->join('occ_products as p', 'p.id', '=', 'i.product_id')
                ->select('i.amount as order_amount', 'i.product_size', 'p.*')->where('i.order_id', $id)
                ->where('p.admin_id', $login_id)->where('i.status', 0)->orderby('i.id', 'asc')->get();
            foreach ($products as $product) {
                DB::table('occ_products')->where('id', $product->id)->where('admin_id', $login_id)->increment('amount', $product->order_amount);
            }
            DB::table('occ_gifts_items')->where('order_id', $id)->delete();
            DB::table('occ_gifts')->where('order_id', $id)->delete();
        }
        return $id;
    }

    public function detail($id = 0)
    {
        if ($_SESSION['userrole'] == 1) {
            $products = DB::table('occ_gifts_items as i')
                ->join('occ_products as p', 'i.product_id', '=', 'p.id')
                ->select('p.*', 'i.id as items_id', 'i.amount as order_amount', 'i.product_size', 'i.photo as customize_photo',
                    'i.description as customize_text', 'i.delivery_date')
                ->where('i.order_id', $id)->where('status', 0)->orderby('i.id', 'asc')->get();
        } else {
            $login_id = Sentinel::getUser()->id;
            $products = DB::table('occ_gifts_items as i')
                ->join('occ_products as p', 'i.product_id', '=', 'p.id')
                ->select('p.*', 'i.id as items_id', 'i.amount as order_amount', 'i.product_size', 'i.photo as customize_photo',
                    'i.description as customize_text', 'i.delivery_date')
                ->where('i.order_id', $id)->where('p.admin_id', $login_id)
                ->where('status', 0)->orderby('i.id', 'asc')->get();
        }
        $user = DB::table('users as u')
            ->join('occ_gifts as g', 'g.customer_id', '=', 'u.id')
            ->select('u.*')->where('g.order_id', $id)->first();
        return View(('admin.order.detail'), compact('id', 'user', 'products'));
    }

    public function cancelproduct($id = 0, $items_id = 0)
    {
        $product1 = DB::table('occ_gifts_items')->where('id', $items_id)->where('status', 0)->first();
        $product2 = DB::table('occ_products')->where('id', $product1->product_id)->first();
        if ($product1->product_size == 1) {
            $price = $product2->small_size_price;
        } elseif ($product1->product_size == 2) {
            $price = $product2->medium_size_price;
        } else {
            $price = $product2->price;
        }

        $decrease_totalprice = $price * ($product1->amount);

        DB::table('occ_products')->where('id', $product1->product_id)->increment('amount', $product1->amount);
        DB::table('occ_gifts_items')->where('id', $items_id)->where('status', 0)->delete();
        DB::table('occ_gifts')->where('order_id', $id)->where('status', 0)->decrement('totalprice', $decrease_totalprice);

        $remain_orders = DB::table('occ_gifts_items')->where('order_id', $id)->where('status', 0)->get();
        if (count($remain_orders) == 0) {
            DB::table('occ_gifts')->where('order_id', $id)->delete();
        }
        return $id;
    }
}
