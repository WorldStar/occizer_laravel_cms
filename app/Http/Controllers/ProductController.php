<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Datatables;
use App\Product;
use DB;
use Validator;
use Sentinel;
use URL;
use Illuminate\Support\Facades\Redirect;
class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
     public function getlist($category_id = 0)
    {
        if($_SESSION['userrole'] == 1){
            $products = Product::where('category_id', $category_id)->orderby('id', 'asc')->get();
        }else{
            $user_id = Sentinel::getUser()->id;
            $products = Product::where('category_id', $category_id)->where('admin_id', $user_id)->orderby('id', 'asc')->get();
        }
        $category = DB::table('occ_categories')->where('id', $category_id)->first();
        return View('admin.product.list', compact('products', 'category'));
    }

    public function create($cat_id = 0)
    {
        $category = DB::table('occ_categories')->where('id', $cat_id)->first();
        $vendors = DB::table('role_users as ru')
            ->join('users as u', 'u.id', '=', 'ru.user_id')
            ->select('u.*')->where('ru.role_id', 2)->orderby('u.created_at', 'desc')->get();
        $occasions = DB::table('occ_occasions')->orderby('id', 'asc')->where('status', 1)->get();
        return View('admin.product.add', compact('category', 'vendors', 'occasions'));
    }

    public function store(Request $request)
    {
        $rules = array(
            'cat_id' => 'required',
            'name' => 'required',
            'amount' => 'required',
            'occasion' => 'required',
        );
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            // Ooops.. something went wrong
            $error = 'You missing to input some fields.';
            return Redirect::back()->withError($error);
        }
        $cat_id = $request->get('cat_id', '');
        $name = $request->get('name', '');
        $description = $request->get('description', '');
        $description2 = $request->get('description2', '');
        $amount = $request->get('amount', 0);
        $large_size_price = $request->get('large_size_price','');
        $large_size_display_name = $request->get('large_size_display_name','');
        $medium_size_price = $request->get('medium_size_price','');
        $medium_size_display_name = $request->get('medium_size_display_name','');
        $small_size_price = $request->get('small_size_price','');
        $small_size_display_name = $request->get('small_size_display_name','');

        if ($_SESSION['userrole'] == 1){
            $vendor = $request->get('vendor', 0);
        }

        $occasion = $request->get('occasion', 0);
        $occasion1 = '';
        for($i = 0; $i < count($occasion); $i++){
            if($i == 0)
                $occasion1 = $occasion[$i];
            else
                $occasion1 .= ','.$occasion[$i];
        }
        $occasion = $occasion1;
        $product = new Product();
        $product->category_id = $cat_id;
        $product->name = $name;
        $product->description = $description;
        $product->description2 = $description2;
        $product->amount = $amount;

        if($large_size_price != 0){
            $product->has_large_size = 1;
        }
        $product->large_size_display_name = $large_size_display_name;
        $product->price = $large_size_price;

        if($medium_size_price != 0){
            $product->has_medium_size = 1;
        }
        $product->medium_size_display_name = $medium_size_display_name;
        $product->medium_size_price = $medium_size_price;

        if($small_size_price != 0){
            $product->has_small_size = 1;
        }
        $product->small_size_display_name = $small_size_display_name;
        $product->small_size_price = $small_size_price;

        if($_SESSION['userrole'] == 1) {
            $product->p_contact_id = $vendor;
            $product->s_contact_id = $vendor;
            $product->admin_id = $vendor;
        } else {
            $vendor = Sentinel::getUser();
            $product->p_contact_id = ($vendor->id);
            $product->admin_id = ($vendor->id);
            if(($vendor->shipper_id) == 0){
                $product->s_contact_id = $vendor->id;
            } else{
                $product->s_contact_id = $vendor->shipper_id;
            }
        }

        $product->occ_id = $occasion;

        $product->save();



        $filenames = '';
        if (isset($_FILES['photo']['tmp_name'])) {
            // Number of uploaded files
            $num_files = count($_FILES['photo']['tmp_name']);
            $destinationPath = public_path() . '/uploads/files/';
            /** loop through the array of files ***/
            $k = 0;
            for ($i = 0; $i < $num_files; $i++) {
                // check if there is a file in the array
                echo $i;
                $ext = substr(strrchr($_FILES['photo']['name'][$i], "."), 1);
                $photoname = time() . $i . '.' . $ext;
                // copy the file to the specified dir
                if (move_uploaded_file($_FILES['photo']['tmp_name'][$i], $destinationPath . $photoname)) {
                    /*** give praise and thanks to the php gods ***/
                    DB::table('occ_product_photos')->insert(['product_id' => $product->id, 'photo' => $photoname]);
                }
            }
        }
        $success = "Successfully added.";
        return Redirect::back()->withSuccess($success);
    }

    public function edit($id = 0)
    {
        $product = DB::table('occ_products')->where('id', $id)->first();
        $photos = DB::table('occ_product_photos')->where('product_id', $id)->get();
        $category = DB::table('occ_categories')->where('id', $product->category_id)->first();
        $vendors = DB::table('role_users as ru')
            ->join('users as u', 'u.id', '=', 'ru.user_id')
            ->select('u.*')->where('ru.role_id', 2)->orderby('u.created_at', 'desc')->get();
        $occasions = DB::table('occ_occasions')->orderby('id', 'asc')->where('status', 1)->get();

        return View('admin.product.edit', compact('product', 'photos', 'category', 'vendors', 'occasions'));
    }

    public function photoDelete($photo_id = 0)
    {
        DB::table('occ_product_photos')->where('id', $photo_id)->delete();
        return 1;
    }

    public function update(Request $request)
    {
//        $rules = array(
//            'cat_id' => 'required',
//            'product_id' => 'required',
//            'name' => 'required',
//            'price' => 'required',
//            'amount' => 'required',
//            'occasion' => 'required',
//            'photo' => 'required',
//        );
//        $validator = Validator::make($request->all(), $rules);
//        if ($validator->fails()) {
//            // Ooops.. something went wrong
//            $error = 'You missing to input some fields.';
//            return Redirect::back()->withError($error);
//        }
        $cat_id = $request->get('cat_id', '');
        $product_id = $request->get('product_id', '');
        $name = $request->get('name', '');
        $description = $request->get('description', '');
        $description2 = $request->get('description2', '');
        $amount = $request->get('amount', 0);
        $large_size_display_name = $request->get('large_size_display_name', '');
        $medium_size_display_name = $request->get('medium_size_display_name', '');
        $small_size_display_name = $request->get('small_size_display_name', '');
        $price = $request->get('large_size_price', 0);
        $medium_size_price = $request->get('medium_size_price', 0);
        $small_size_price = $request->get('small_size_price', 0);

        if($_SESSION['userrole'] == 1) {
            $vendor = $request->get('vendor', 0);
        }

        $occasion = $request->get('occasion', 0);
        $occasion1 = '';
        for($i = 0; $i < count($occasion); $i++){
            if($i == 0)
                $occasion1 = $occasion[$i];
            else
                $occasion1 .= ','.$occasion[$i];
        }
        $occasion = $occasion1;

        $product = Product::find($product_id);
        $product->category_id = $cat_id;
        $product->name = $name;
        $product->description = $description;
        $product->description2 = $description2;
        $product->amount = $amount;

        $product->large_size_display_name = $large_size_display_name;
        $product->medium_size_display_name = $medium_size_display_name;
        $product->small_size_display_name = $small_size_display_name;
        $product->price = $price;
        $product->medium_size_price = $medium_size_price;
        $product->small_size_price = $small_size_price;

        if($_SESSION['userrole'] == 1) {
            $product->p_contact_id = $vendor;
            $product->s_contact_id = $vendor;
            $product->admin_id = $vendor;
        } else {
            $vendor = Sentinel::getUser()->id;
            $product->p_contact_id = $vendor;
            $product->admin_id = $vendor;
        }

        $product->occ_id = $occasion;
        $product->save();

        $filenames = '';
        if (isset($_FILES['photo']['tmp_name'])) {
            // Number of uploaded files
            $num_files = count($_FILES['photo']['tmp_name']);
            $destinationPath = public_path() . '/uploads/files/';
            /** loop through the array of files ***/
            $k = 0;
            for ($i = 0; $i < $num_files; $i++) {
                // check if there is a file in the array
                echo $i;
                $ext = substr(strrchr($_FILES['photo']['name'][$i], "."), 1);
                $photoname = time() . $i . '.' . $ext;
                // copy the file to the specified dir
                if (move_uploaded_file($_FILES['photo']['tmp_name'][$i], $destinationPath . $photoname)) {
                    /*** give praise and thanks to the php gods ***/
                    DB::table('occ_product_photos')->insert(['product_id' => $product->id, 'photo' => $photoname]);
                }
            }
        }
        $success = "Successfully added.";
        return Redirect::back()->withSuccess($success);
    }

    public function view($id = 0)
    {
        $product = DB::table('occ_products')->where('id', $id)->first();
        $photos = DB::table('occ_product_photos')->where('product_id', $id)->get();
        $category = DB::table('occ_categories')->where('id', $product->category_id)->first();
        $vendors = DB::table('role_users as ru')
            ->join('users as u', 'u.id', '=', 'ru.user_id')
            ->select('u.*')->where('ru.role_id', 2)->orderby('u.created_at', 'desc')->get();
        $occasions = DB::table('occ_occasions')->where('status', 1)->orderby('id', 'asc')->get();

        return View('admin.product.view', compact('product', 'photos', 'category', 'vendors', 'occasions'));
    }

    public function delete($id)
    {
        $row = Product::find($id);
        $row->delete();
        return $row->id;
    }

    public function editableDatatableUpdate(Request $request, $id)
    {
        $table = Product::find($id);
        $table->update($request->except('_token', 'Plants'));
        return $table->id;
    }

    public function editableDatatableData()
    {
        $tables = Product::where('category_id', '=', 2)->orderby('id', 'asc')->get();

        return Datatables::of($tables)
            ->add_column('edit', '<a class="edit" href="javascript:;">Edit</a>')
            ->add_column('delete', '<a class="delete" href="javascript:;">Delete</a>')
            ->make(true);
    }

    public function active_product($id = 0)
    {
        DB::table('occ_products')->where('id', $id)->update(['state'=>1]);
        $data = array("state"=>1);
        return $data;
    }

    public function inactive_product($id = 0)
    {
        DB::table('occ_products')->where('id', $id)->update(['state'=>0]);
        $data = array("state"=>0);
        return $data;
    }
}
