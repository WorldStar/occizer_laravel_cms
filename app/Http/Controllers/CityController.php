<?php

namespace App\Http\Controllers;

use App\City;
use App\Http\Requests;
use Illuminate\Http\Request;
use Datatables;
use DB;
use App\Http\Controllers\sweetAlert;


class CityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */

    public function getlist()
    {
        $countries = DB::table('occ_countries')->where('status', 1)->orderBy('id', 'asc')->get();
        $cities = DB::table('occ_cities')->orderby('id', 'asc')->get();
        return View('admin/city/citylist', compact('countries', 'cities'));
    }

    public function get_data()
    {
        if ($_SESSION['userrole'] == 1) {
            $cities = DB::table('occ_cities as ci')
                ->join('occ_countries as co', 'co.id', '=', 'ci.country_id')
                ->select('ci.*', 'co.name as country_name')->orderBy('co.id', 'asc')->get();
        } else {
            $cities = DB::table('occ_cities as ci')
                ->join('occ_countries as co', 'co.id', '=', 'ci.country_id')
                ->select('ci.*', 'co.name as country_name','co.status as country_status')->where('ci.status', 1)->where('co.status', 1)
                ->orderBy('co.id', 'asc')->get();
        }

        $cities = collect($cities);
        return Datatables::of($cities)
            ->edit_column('status', function ($data) {
                if($data->status == 1) {
                    $status = 'active';
                    return '<a style="color: #ca0002" class="active" href="javascript:;">' . $status . '</a>';
                }else {
                    $status = 'inactive';
                    return '<a class="inactive" href="javascript:;">' . $status . '</a>';
                }
            })
            ->editColumn('created_at', function ($data) {
                return date('M d, Y', strtotime($data->created_at));
            })
            ->add_column('edit', '<a class="edit" href="javascript:;">
                <i class="livicon edit-color" data-name="edit"
                data-size="18" data-c="#2e16ff" data-hc="#2e16ff" data-loop="true"></i>Edit</a>')
            ->add_column('delete', '<a class="delete" href="javascript:;"><i class="livicon del-color"
                data-name="trash" data-size="18" data-c="#ff173d" data-hc="#ff173d" data-loop="true"></i>Delete</a>')
            ->make(true);
    }

    public function update_city(Request $request)
    {
        $id = $_GET['id'];
        $name = $_GET['name'];
        $name = ucfirst($name);
        DB::table('occ_cities')->where('id', $id)->update(array('name'=>$name));
        return $id;
    }

    public function delete_city($id)
    {
        $users = DB::table('users as u')
            ->join('role_users as ru', 'u.id','=','ru.user_id')
            ->select('first_name', 'last_name','city_id')->orderby('id', 'asc')->get();
        $user_name = '';
        foreach ($users as $user) {
            $city = explode(',',$user->city_id );
            if(in_array($id, $city)) {
                if(empty($user_name)){
                    $user_name = $user->first_name.' '.$user->last_name;
                } else {
                    $user_name  .= ', '.$user->first_name.' '.$user->last_name;
                }
            }
        }
        if (empty($user_name)){
            DB::table('occ_cities')->where('id', $id)->delete();
        } else {
            return $user_name;
        }
    }

    public function add_city(Request $request)
    {
        $name = $_GET['name'];
        $name = ucfirst($name);
        $id = $_GET['country_id'];
        $city = DB::table('occ_cities')->where('name', $name)->where('country_id', $id)->first();
        if(is_null($city)){
            DB::table('occ_cities')->insert(array('name'=>$name, 'country_id'=>$id));
        } else {
            $duplicity = 1;
            return $duplicity;
        }
    }

    public function active_city($id = 0)
    {
        $city = DB::table('occ_cities')->where('id', $id)->first();
        $country = DB::table('occ_countries')->where('id', $city->country_id)->first();
        if($country->status == 0){
            $inactive_country = $country->name;
            return $inactive_country;
        } else {
            DB::table('occ_cities')->where('id', $id)->update(array('status'=>1));
        }

    }

    public function inactive_city($id = 0)
    {
        DB::table('occ_cities')->where('id', $id)->update(array('status'=>0));
        return $id;
    }
}

