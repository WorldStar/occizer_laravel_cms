<?php

namespace App\Http\Controllers;

use App\Shipper;
use App\Http\Requests;
use Illuminate\Http\Request;
use Datatables;
use DB;
use App\Http\Controllers\sweetAlert;

class ShipperController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */

    public function getlist()
    {
        if($_SESSION['userrole'] == 1) {
            $tables = Shipper::orderBy('id', 'asc')->get();
        } else{
            $tables =DB::table('occ_shippers')->where('status', 1)->orderBy('id', 'asc')->get();
        }
        $cities = DB::table('occ_cities')->orderby('name', 'asc')->get();
        return View('admin/shipper/shipper', compact('tables', 'cities'));
    }

    public function shipper_data()
    {
        if($_SESSION['userrole'] == 1) {
            $tables = Shipper::orderBy('id', 'asc')->get();
        } else{
            $tables = Shipper::orderBy('id', 'asc')->where('status', 1)->get();
        }

        return Datatables::of($tables)
            ->edit_column('status', function ($data) {
                if ($data->status == 1) {
                    $status = 'active';
                    return '<a style="color: #ca0002" class="inactive" href="javascript:;">' . $status . '</a>';
                } else {
                    $status = 'inactive';
                    return '<a class="active" href="javascript:;">' . $status . '</a>';
                }
            })
            ->edit_column('city_id', function ($data) {
                $city1 = explode(',', $data->city_id);
                $cities = '';
                for($i = 0; $i < count($city1); $i++){
                    $city = DB::table('occ_cities')->where('id', $city1[$i])->first();
                    if(!empty($city)){
                        if($i == 0)
                            $cities = $city->name;
                        else
                            $cities .= ', '.$city->name;
                    }
                }
                return $cities;
            })
            ->add_column('delete', '<a class="delete" href="javascript:;"><i class="livicon del-color" data-name="trash" data-size="18"
                                        data-c="#ff173d" data-hc="#ff173d" data-loop="true"></i>Delete</a>')
            ->make(true);
    }

    public function delete_shipper($id)
    {
        DB::table('occ_shippers')->where('id', $id)->delete();
        return $id;
    }

    public function add_shipper(Request $request)
    {
        $email = $_GET['email'];
        $shipper = DB::table('occ_shippers')->where('email', $email)->first();
        if(is_null($shipper)) {
            $name = $_GET['name'];
            $phone_number = $_GET['phone_number'];
            $contact_name = $_GET['contact_name'];
            $city = $_GET['city'];
            $city = implode(',', $city);
            DB::table('occ_shippers')->insert(array('name'=>$name, 'email'=>$email, 'phone_number'=>$phone_number, 'contact_name'=>$contact_name, 'city_id'=>$city));
            return 'success';
        } else {
            $duplicate = 1;
            return $duplicate;
        }
    }

    public function active_shipper($id = 0)
    {
        DB::table('occ_shippers')->where('id', $id)->update(array('status'=>1));
        $data = array("status"=>1);
        return $data;
    }

    public function inactive_shipper($id = 0)
    {
        DB::table('occ_shippers')->where('id', $id)->update(['status'=>0]);
        $data = array("status"=>0);
        return $data;
    }
}

