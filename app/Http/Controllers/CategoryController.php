<?php

namespace App\Http\Controllers;

use App\Occasion;
use App\Http\Requests;
use Illuminate\Http\Request;
use Datatables;
use App\Category;
use DB;
class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('admin.occasion.occasion');
    }
	
    public function getlist()
    {
        $categories = Category::orderBy('id', 'asc')->get();
		return View('admin/category/categorylist', compact('categories'));
    }

	public function editableDatatableData()
    {
        $tables = Category::get(['id','name', 'description', 'created_at']);

        return Datatables::of($tables)
            ->editColumn('created_at', function ($data) {
                return date('M d, Y', strtotime($data->created_at));
            })
            ->add_column('edit', '<a class="edit"href="javascript:;">
            <i class="livicon edit-color" data-name="edit" data-size="18" data-c="#2e16ff" data-hc="#2e16ff" data-loop="true"></i>Edit</a>')
            ->add_column('delete', '<a class="delete" href="javascript:;">
                       <i class="livicon del-color" data-name="trash" data-size="18"
                       data-c="#ff173d" data-hc="#ff173d" data-loop="true"></i>Delete</a>')
            ->make(true);
    }
	
    public function editableDatatableStore(Request $request)
    {
        if($request->ajax()) {
            Category::create($request->except('_token'));
        }
    }

    public function editableDatatableUpdate(Request $request, $id)
    {
        $table = Category::find($id);
        $table->update($request->except('_token', 'Plants'));
        return $table->id;
    }

    public function editableDatatableDestroy($id)
    {
        $row=Category::find($id);
        $row->delete();
        return $row->id;
    }

    public function addCategory(Request $request)
    {
        $name = $_GET['name'];
        $category = DB::table('occ_categories')->where('name', $name)->first();
        if(is_null($category)) {
            $category = array();
            if($request->ajax()) {

                $category = Category::create($request->except('_token'));
            }
            return $category;
        } else {
            $duplicate = 1;
            return $duplicate;
        }
    }
}
