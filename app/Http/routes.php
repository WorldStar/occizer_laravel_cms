<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/**
 * Model binding into route
 */
Route::model('blogcategory', 'App\BlogCategory');
Route::model('blog', 'App\Blog');
Route::model('file', 'App\File');
Route::model('task', 'App\Task');
Route::model('users', 'App\User');

Route::pattern('slug', '[a-z0-9- _]+');

Route::group(array('prefix' => 'admin'), function () {

	# Error pages should be shown without requiring login
	Route::get('404', function () {
		return View('admin/404');
	});
	Route::get('500', function () {
		return View::make('admin/500');
	});

    Route::post('secureImage', array('as' => 'secureImage','uses' => 'JoshController@secureImage'));

	# Lock screen
	Route::get('{id}/lockscreen', array('as' => 'lockscreen', 'uses' =>'UsersController@lockscreen'));

	# All basic routes defined here
	Route::get('signin', array('as' => 'signin', 'uses' => 'AuthController@getSignin'));
	Route::post('signin', 'AuthController@postSignin');
	Route::post('siugnp', array('as' => 'signup', 'uses' => 'AuthController@postSignup'));
	Route::post('forgot-password', array('as' => 'forgot-password', 'uses' => 'AuthController@postForgotPassword'));
	Route::get('login2', function () {
		return View::make('admin/login2');
	});

	# Register2
	Route::get('register2', function () {
		return View::make('admin/register2');
	});
	Route::post('register2', array('as' => 'register2', 'uses' => 'AuthController@postRegister2'));

	# Forgot Password Confirmation
	Route::get('forgot-password/{userId}/{passwordResetCode}', array('as' => 'forgot-password-confirm', 'uses' => 'AuthController@getForgotPasswordConfirm'));
	Route::post('forgot-password/{userId}/{passwordResetCode}', 'AuthController@postForgotPasswordConfirm');

	# Logout
	Route::get('logout', array('as' => 'logout', 'uses' => 'AuthController@getLogout'));

	# Account Activation
	Route::get('activate/{userId}/{activationCode}', array('as' => 'activate', 'uses' => 'AuthController@getActivate'));
} );

Route::group(array('prefix' => 'admin', 'middleware' => 'SentinelAdmin'), function () {

    /* dashboard */
	Route::get('/', array('as' => 'dashboard','uses' => 'JoshController@showHome'));
    Route::get('dashboard/ticket', 'JoshController@getrecentlytickets');

/* Category  */

    Route::get('category', 'CategoryController@getlist');
    Route::get('category/data', array('as' => 'admin.category.data', 'uses' => 'CategoryController@editableDatatableData'));
    Route::any('category/addcategory', array('as' => 'admin.category.add', 'uses' => 'CategoryController@addCategory'));
    Route::post('category/{id}/update', 'CategoryController@editableDatatableUpdate');
    Route::get('category/{id}/delete', array('as' => 'admin.category.delete', 'uses' => 'CategoryController@editableDatatableDestroy'));

/* Product  */

    Route::get('products/{catid}', 'ProductController@getlist');
    Route::any('products/get/{catid}', 'ProductController@getProducts');
    Route::get('product/data', array('as' => 'admin.product.data', 'uses' => 'ProductController@editableDatatableData'));
    Route::post('product/{id}/update', 'ProductController@editableDatatableUpdate');
    Route::get('product/{id}/delete', array('as' => 'admin.product.delete', 'uses' => 'ProductController@delete'));
    Route::get('product/{cat_id}/add', array('as' => 'admin.product.add', 'uses' => 'ProductController@create'));
    Route::post('product/store', array('as' => 'admin.product.store', 'uses' => 'ProductController@store'));
    Route::any('product/edit/{id}', array('as' => 'admin.product.edit', 'uses' => 'ProductController@edit'));
    Route::get('product/photo/delete/{photo_id}', array('as' => 'admin.product.photo.delete', 'uses' => 'ProductController@photoDelete'));
    Route::post('product/update', array('as' => 'admin.product.store', 'uses' => 'ProductController@update'));
    Route::get('product/view/{id}', array('as' => 'admin.product.view', 'uses' => 'ProductController@view'));
    Route::get('product/{id}/inactive_product', 'ProductController@inactive_product');
    Route::get('product/{id}/active_product', 'ProductController@active_product');

/* Occasion  */

    Route::get('occasions', 'OccasionController@getlist');
    Route::get('occasions/data', array('as' => 'admin.occasions.data', 'uses' => 'OccasionController@occasion_data'));
    Route::any('occasions/add_occasion', array('as' => 'admin.occasions.add', 'uses' => 'OccasionController@add_occasion'));
    Route::post('occasions/{id}/update', 'OccasionController@occasion_update');
    Route::get('occasions/{id}/delete', array('as' => 'admin.occasions.delete', 'uses' => 'OccasionController@occasion_delete'));
    Route::get('occasions/{id}/inactive_occasion', 'OccasionController@inactive_occasion');
    Route::get('occasions/{id}/active_occasion', 'OccasionController@active_occasion');

 /* Vendor */

    Route::get('vendor', 'VendorController@getlist');
    Route::get('vendor/get/{id}', 'VendorController@getVendor');
    Route::get('vendor/inactive/{id}/{status}', 'VendorController@inactiveVendor');


/* Customer */

    Route::get('customer', 'CustomerController@getlist');
    Route::get('customer/get/{id}', 'CustomerController@getCustomer');
    Route::get('customer/inactive/{id}/{status}', 'CustomerController@inactiveCustomer');

/* Shipper */
    Route::get('shippers', 'ShipperController@getlist');
    Route::get('shippers/data', array('as' => 'admin.shippers.data', 'uses' => 'ShipperController@shipper_data'));
    Route::any('shipper/add_shipper', array('as' => 'admin.shipper.add', 'uses' => 'ShipperController@add_shipper'));
    Route::post('shipper/store', array('as' => 'admin.shipper.store', 'uses' => 'ShipperController@store'));
    Route::any('shipper/{id}/update', 'ShipperController@shipper_update');
    Route::get('shipper/{id}/delete', array('as' => 'admin.shipper.delete', 'uses' => 'ShipperController@delete_shipper'));
    Route::get('shipper/{id}/inactive_shipper', 'ShipperController@inactive_shipper');
    Route::get('shipper/{id}/active_shipper', 'ShipperController@active_shipper');


/* Order  */

    Route::get('orders', 'OrderController@getlist');
    Route::any('orders/get', 'OrderController@getOrders');
    Route::get('order/{id}/cancel', array('as' => 'admin.order.delete', 'uses' => 'OrderController@cancelorder'));
    Route::get('order/detail/{id}', array('as' => 'admin.order.detail', 'uses' => 'OrderController@detail'));
    Route::get('order/{id}/{items_id}/cancel', 'OrderController@cancelproduct');


/* ticket */

    Route::get('ticket/paid', 'TicketController@getlist');
    Route::any('ticket/getpaid', 'TicketController@getpaidTicket');
    Route::get('ticket/update', array('as' => 'admin.ticket.update', 'uses' => 'TicketController@update'));
    Route::get('ticket/detail/{id}', array('as' => 'admin.ticket.detail', 'uses' => 'TicketController@detail'));
    Route::get('ticket/{id}/cancel', 'TicketController@cancel_ticket');
    Route::get('ticket/{id}/{items_id}/cancel', 'TicketController@cancel_product');
    Route::get('ticket/ship', 'TicketController@get_shippedlist');
    Route::any('ticket/getshipped', 'TicketController@get_shippedTicket');
    Route::get('ticket/shipdetail/{id}', array('as' => 'admin.ticket.shipdetail', 'uses' => 'TicketController@shipdetail'));
    Route::get('ticket/cancelled', 'TicketController@get_cancelledlist');
    Route::any('ticket/getcancelled', 'TicketController@get_cancelledTicket');
    Route::get('ticket/{id}/delete', array('as' => 'admin.ticket.delete_cancelled', 'uses' => 'TicketController@delete_cancelled'));
    Route::get('ticket/cancelled_detail/{id}', array('as' => 'admin.ticket.cancelled_detail', 'uses' => 'TicketController@cancelled_detail'));
    Route::get('ticket/{id}/{items_id}/delete', 'TicketController@delete_product');


    /* country and city */

    Route::get('country', 'CountryController@getlist');
    Route::get('country/data', array('as' => 'admin.country.data', 'uses' => 'CountryController@get_data'));
    Route::any('country/add_country', array('as' => 'admin.country.add', 'uses' => 'CountryController@add_country'));
    Route::any('country/{id}/update', 'CountryController@country_update');
    Route::get('country/{id}/delete', array('as' => 'admin.country.delete', 'uses' => 'CountryController@country_delete'));
    Route::get('country/{id}/inactive_country', 'CountryController@inactive_country');
    Route::get('country/{id}/active_country', 'CountryController@active_country');


    Route::get('city', 'CityController@getlist');
    Route::get('city/data', array('as' => 'admin.city.data', 'uses' => 'CityController@get_data'));
    Route::any('city/add', array('as' => 'admin.city.add', 'uses' => 'CityController@add_city'));
    Route::any('city/{id}/update', 'CityController@update_city');
    Route::get('city/{id}/delete', 'CityController@delete_city');
    Route::get('city/{id}/inactive_city', 'CityController@inactive_city');
    Route::get('city/{id}/active_city', 'CityController@active_city');

    /* payment method */

    Route::get('paymethod', 'PayMethodController@getlist');
    Route::get('paymethod/data', array('as' => 'admin.paymethod.data', 'uses' => 'PayMethodController@get_data'));
    Route::get('paymethod/{id}/inactive_method', 'PayMethodController@inactive_method');
    Route::get('paymethod/{id}/active_method', 'PayMethodController@active_method');


    # User Management
    Route::group(array('prefix' => 'users'), function () {
        Route::get('/', array('as' => 'users', 'uses' => 'UsersController@index'));
        Route::get('data',['as' => 'users.data', 'uses' =>'UsersController@data']);
        Route::get('create', 'UsersController@create');
        Route::post('create', 'UsersController@store');
        Route::get('{userId}/delete', array('as' => 'delete/user', 'uses' => 'UsersController@destroy'));
        Route::get('{userId}/confirm-delete', array('as' => 'confirm-delete/user', 'uses' => 'UsersController@getModalDelete'));
        Route::get('{userId}/restore', array('as' => 'restore/user', 'uses' => 'UsersController@getRestore'));
        Route::get('{userId}', array('as' => 'users.show', 'uses' => 'UsersController@show'));
        Route::post('{userId}/passwordreset', array('as' => 'passwordreset', 'uses' => 'UsersController@passwordreset'));
    });

    Route::resource('users', 'UsersController');

	Route::get('deleted_users',array('as' => 'deleted_users','before' => 'Sentinel', 'uses' => 'UsersController@getDeletedUsers'));

	# Group Management
    Route::group(array('prefix' => 'groups'), function () {
        Route::get('/', array('as' => 'groups', 'uses' => 'GroupsController@index'));
        Route::get('create', array('as' => 'create/group', 'uses' => 'GroupsController@create'));
        Route::post('create', 'GroupsController@store');
        Route::get('{groupId}/edit', array('as' => 'update/group', 'uses' => 'GroupsController@edit'));
        Route::post('{groupId}/edit', 'GroupsController@update');
        Route::get('{groupId}/delete', array('as' => 'delete/group', 'uses' => 'GroupsController@destroy'));
        Route::get('{groupId}/confirm-delete', array('as' => 'confirm-delete/group', 'uses' => 'GroupsController@getModalDelete'));
        Route::get('{groupId}/restore', array('as' => 'restore/group', 'uses' => 'GroupsController@getRestore'));
    });

    /*routes for blog*/
	Route::group(array('prefix' => 'blog'), function () {
        Route::get('/', array('as' => 'blogs', 'uses' => 'BlogController@index'));
        Route::get('create', array('as' => 'create/blog', 'uses' => 'BlogController@create'));
        Route::post('create', 'BlogController@store');
        Route::get('{blog}/edit', array('as' => 'update/blog', 'uses' => 'BlogController@edit'));
        Route::post('{blog}/edit', 'BlogController@update');
        Route::get('{blog}/delete', array('as' => 'delete/blog', 'uses' => 'BlogController@destroy'));
		Route::get('{blog}/confirm-delete', array('as' => 'confirm-delete/blog', 'uses' => 'BlogController@getModalDelete'));
		Route::get('{blog}/restore', array('as' => 'restore/blog', 'uses' => 'BlogController@getRestore'));
        Route::get('{blog}/show', array('as' => 'blog/show', 'uses' => 'BlogController@show'));
        Route::post('{blog}/storecomment', array('as' => 'restore/blog', 'uses' => 'BlogController@storecomment'));
	});

    /*routes for blog category*/
	Route::group(array('prefix' => 'blogcategory'), function () {
        Route::get('/', array('as' => 'blogcategories', 'uses' => 'BlogCategoryController@index'));
        Route::get('create', array('as' => 'create/blogcategory', 'uses' => 'BlogCategoryController@create'));
        Route::post('create', 'BlogCategoryController@store');
        Route::get('{blogcategory}/edit', array('as' => 'update/blogcategory', 'uses' => 'BlogCategoryController@edit'));
        Route::post('{blogcategory}/edit', 'BlogCategoryController@update');
        Route::get('{blogcategory}/delete', array('as' => 'delete/blogcategory', 'uses' => 'BlogCategoryController@destroy'));
		Route::get('{blogcategory}/confirm-delete', array('as' => 'confirm-delete/blogcategory', 'uses' => 'BlogCategoryController@getModalDelete'));
		Route::get('{blogcategory}/restore', array('as' => 'restore/blogcategory', 'uses' => 'BlogCategoryController@getRestore'));
	});

	/*routes for file*/
	Route::group(array('prefix' => 'file'), function () {
        Route::post('create', 'FileController@store');
		Route::post('createmulti', 'FileController@postFilesCreate');
		Route::delete('delete', 'FileController@delete');
	});

	Route::get('crop_demo', function () {
        return redirect('admin/imagecropping');
    });
    Route::post('crop_demo','JoshController@crop_demo');


    /* laravel example routes */
	# datatables
	Route::get('datatables', 'DataTablesController@index');
	Route::get('datatables/data', array('as' => 'admin.datatables.data', 'uses' => 'DataTablesController@data'));

   # editable datatables
    Route::get('editable_datatables', 'DataTablesController@editableDatatableIndex');
    Route::get('editable_datatables/data', array('as' => 'admin.editable_datatables.data', 'uses' => 'DataTablesController@editableDatatableData'));
    Route::post('editable_datatables/create','DataTablesController@editableDatatableStore');
    Route::post('editable_datatables/{id}/update', 'DataTablesController@editableDatatableUpdate');
    Route::get('editable_datatables/{id}/delete', array('as' => 'admin.editable_datatables.delete', 'uses' => 'DataTablesController@editableDatatableDestroy'));


	//tasks section
    Route::post('task/create', 'TaskController@store');
    Route::get('task/data', 'TaskController@data');
    Route::post('task/{task}/edit', 'TaskController@update');
    Route::post('task/{task}/delete', 'TaskController@delete');


	# Remaining pages will be called from below controller method
	# in real world scenario, you may be required to define all routes manually

	Route::get('{name?}', 'JoshController@showView');

});

#FrontEndController
Route::get('login', array('as' => 'login','uses' => 'FrontEndController@getLogin'));
Route::post('login','FrontEndController@postLogin');
Route::get('register', array('as' => 'register','uses' => 'FrontEndController@getRegister'));
Route::post('register','FrontEndController@postRegister');
Route::get('activate/{userId}/{activationCode}',array('as' =>'activate','uses'=>'FrontEndController@getActivate'));
Route::get('forgot-password',array('as' => 'forgot-password','uses' => 'FrontEndController@getForgotPassword'));
Route::post('forgot-password','FrontEndController@postForgotPassword');

# Forgot Password Confirmation
Route::get('forgot-password/{userId}/{passwordResetCode}', array('as' => 'forgot-password-confirm', 'uses' => 'FrontEndController@getForgotPasswordConfirm'));
Route::post('forgot-password/{userId}/{passwordResetCode}', 'FrontEndController@postForgotPasswordConfirm');

# My account display and update details
Route::group(array('middleware' => 'SentinelUser'), function () {
	Route::get('my-account', array('as' => 'my-account', 'uses' => 'FrontEndController@myAccount'));
    Route::put('my-account', 'FrontEndController@update');
});
Route::get('logout', array('as' => 'logout','uses' => 'FrontEndController@getLogout'));

# contact form
Route::post('contact',array('as' => 'contact','uses' => 'FrontEndController@postContact'));

#frontend views
Route::get('/index', array('as' => 'home', function () {  // change /index to /
    return View::make('index');
}));

Route::get('blog', array('as' => 'blog', 'uses' => 'BlogController@getIndexFrontend'));
Route::get('blog/{slug}/tag', 'BlogController@getBlogTagFrontend');
Route::get('blogitem/{slug?}', 'BlogController@getBlogFrontend');
Route::post('blogitem/{blog}/comment', 'BlogController@storeCommentFrontend');

Route::get('{name?}', 'JoshController@showFrontEndView');
# End of frontend views

