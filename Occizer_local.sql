/*
SQLyog Ultimate v11.33 (64 bit)
MySQL - 10.1.16-MariaDB : Database - occizer
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`occizer` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `occizer`;

/*Table structure for table `activations` */

DROP TABLE IF EXISTS `activations`;

CREATE TABLE `activations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `completed_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `activations` */

insert  into `activations`(`id`,`user_id`,`code`,`completed`,`completed_at`,`created_at`,`updated_at`) values (1,1,'9apSlXZyQAXGuSxa6Z6y2R51mJlwbZGL',1,'2016-11-22 01:20:37','2016-11-22 01:20:37','2016-11-22 01:20:37'),(2,3,'agBlT2eCrXoYNS2Gk4h0r1wuvaNWtpmN',1,'2016-11-23 18:26:06','2016-11-23 18:26:06','2016-11-23 18:26:06'),(3,4,'o2nHSFkSaMZhssbfnX4XKTFwMTjBKCb2',1,'2016-11-24 13:17:59','2016-11-24 13:17:59','2016-11-24 13:17:59');

/*Table structure for table `blog_categories` */

DROP TABLE IF EXISTS `blog_categories`;

CREATE TABLE `blog_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `blog_categories` */

/*Table structure for table `blog_comments` */

DROP TABLE IF EXISTS `blog_comments`;

CREATE TABLE `blog_comments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `blog_id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `website` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comment` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `blog_comments` */

/*Table structure for table `blogs` */

DROP TABLE IF EXISTS `blogs`;

CREATE TABLE `blogs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `blog_category_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `views` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `blogs` */

/*Table structure for table `datatables` */

DROP TABLE IF EXISTS `datatables`;

CREATE TABLE `datatables` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fullname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `points` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notes` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `datatables` */

insert  into `datatables`(`id`,`username`,`fullname`,`email`,`points`,`notes`,`created_at`,`updated_at`) values (3,'Mia','Hackett','herman.torphy@gmail.com','170','Consequatur blanditiis eius enim ut. Et saepe sit expedita iste maxime.','2016-11-07 16:13:06','2016-11-16 08:09:56'),(4,'Jessica','D\'Amore','wmayer@gmail.com','600','Rerum dolores eligendi voluptatem veniam. Quis tenetur fuga quos a iste possimus repellendus.','2016-11-07 16:13:06','2016-11-16 08:18:51'),(5,'Felipa','Wolf','nels.denesik@gmail.com','410','Voluptas maxime sit et aut. Aut qui enim quo nemo sint. Porro est harum veritatis provident sint.','2016-11-07 16:13:06','2016-11-07 16:13:06');

/*Table structure for table `files` */

DROP TABLE IF EXISTS `files`;

CREATE TABLE `files` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `filename` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mime` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `files` */

/*Table structure for table `migrations` */

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `migrations` */

insert  into `migrations`(`migration`,`batch`) values ('2014_07_02_230147_migration_cartalyst_sentinel',1),('2014_10_04_174350_soft_delete_users',1),('2014_12_10_011106_add_fields_to_user_table',1),('2015_08_09_200015_create_blog_module_table',1),('2015_08_11_064636_add_slug_to_blogs_table',1),('2015_08_19_073929_create_taggable_table',1),('2015_11_10_140011_create_files_table',1),('2016_01_02_062647_create_tasks_table',1),('2016_04_26_054601_create_datatables_table',1);

/*Table structure for table `occ_categories` */

DROP TABLE IF EXISTS `occ_categories`;

CREATE TABLE `occ_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`,`name`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `occ_categories` */

insert  into `occ_categories`(`id`,`name`,`description`,`created_at`,`updated_at`) values (1,'cake','There are many kinds of sweet  and cheap cakes.','2016-11-23 04:28:16','2016-11-23 11:45:55'),(2,'flower','There are many kinds of beautiful flowers.','2016-11-23 04:18:29','2016-11-23 11:46:35'),(3,'chocolates','There are many sorts of sweet chocolates.','2016-11-15 17:36:54','2016-11-23 11:47:18');

/*Table structure for table `occ_cities` */

DROP TABLE IF EXISTS `occ_cities`;

CREATE TABLE `occ_cities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(225) COLLATE utf8_unicode_ci NOT NULL,
  `country_id` int(3) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `occ_cities` */

insert  into `occ_cities`(`id`,`name`,`country_id`,`status`,`created_at`,`updated_at`) values (1,'Dandong',1,1,'2016-12-22 09:25:27','0000-00-00 00:00:00'),(2,'Riyad',2,1,'2016-12-22 11:22:14','0000-00-00 00:00:00'),(3,'Beijing',1,1,'2016-12-22 10:09:08','2016-12-18 10:19:44'),(4,'Moscow',3,0,'2016-12-22 16:39:00','0000-00-00 00:00:00');

/*Table structure for table `occ_countries` */

DROP TABLE IF EXISTS `occ_countries`;

CREATE TABLE `occ_countries` (
  `id` int(3) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(225) COLLATE utf8_unicode_ci NOT NULL,
  `phone_prefix` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `currency` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `occ_countries` */

insert  into `occ_countries`(`id`,`name`,`phone_prefix`,`currency`,`status`,`created_at`,`updated_at`) values (1,'China','+86','CNY',1,'2016-12-22 09:03:47','0000-00-00 00:00:00'),(2,'Saudi Arabia','+966','SAR',1,'2016-12-22 09:33:52','0000-00-00 00:00:00'),(3,'Russia','+7','RUB',0,'2016-12-22 16:39:00','2016-12-19 13:24:53');

/*Table structure for table `occ_gifts` */

DROP TABLE IF EXISTS `occ_gifts`;

CREATE TABLE `occ_gifts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `vendor_id` int(10) NOT NULL,
  `customer_id` int(10) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `totalprice` float NOT NULL DEFAULT '0',
  `delivery_location` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `platform_number` int(1) NOT NULL DEFAULT '2' COMMENT '1-web, 2-mobile',
  `payment_method` int(1) NOT NULL DEFAULT '1' COMMENT '1-paypal, 2-credit card, 3-Cash on delivery',
  `status` int(10) NOT NULL DEFAULT '0' COMMENT '0-order, 1-paid, -1:cancelled ticket',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=132 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `occ_gifts` */

insert  into `occ_gifts`(`id`,`order_id`,`vendor_id`,`customer_id`,`description`,`totalprice`,`delivery_location`,`platform_number`,`payment_method`,`status`,`created_at`,`updated_at`) values (126,'1491726657',2,6,NULL,500,'35.0402409,133.3060391',2,1,1,'2016-12-29 10:19:21','0000-00-00 00:00:00'),(127,'1491726658',2,6,NULL,400,'35.0402409,133.3060391',2,2,0,'2016-12-29 10:14:03','0000-00-00 00:00:00'),(128,'1491726659',3,4,NULL,350,'35.0402409,133.3060391',2,3,1,'2016-12-25 12:01:36','0000-00-00 00:00:00'),(129,'1491726660',3,5,NULL,250,'35.0402409,133.3060391',2,1,1,'2016-12-25 19:48:53','0000-00-00 00:00:00'),(130,'1481726664',3,5,NULL,1000,'35.0402409,133.3060391',2,1,0,'2016-12-14 11:09:06','0000-00-00 00:00:00');

/*Table structure for table `occ_gifts_items` */

DROP TABLE IF EXISTS `occ_gifts_items`;

CREATE TABLE `occ_gifts_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `category_id` int(10) NOT NULL,
  `product_id` int(10) unsigned NOT NULL,
  `product_size` int(1) NOT NULL DEFAULT '3' COMMENT '1-small, 2-medium, 3-large',
  `amount` float NOT NULL DEFAULT '0',
  `option` int(1) NOT NULL DEFAULT '0' COMMENT '0-no customize, 1-customize',
  `photo` text COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(225) COLLATE utf8_unicode_ci NOT NULL,
  `delivery_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `status` int(1) NOT NULL DEFAULT '0' COMMENT '0-order, 1-paid, -1:cancelled ticket',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`),
  CONSTRAINT `product_id` FOREIGN KEY (`product_id`) REFERENCES `occ_products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=197 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `occ_gifts_items` */

insert  into `occ_gifts_items`(`id`,`order_id`,`category_id`,`product_id`,`product_size`,`amount`,`option`,`photo`,`description`,`delivery_date`,`status`,`created_at`,`updated_at`) values (173,'1491726657',2,7,3,10,0,'','','2016-12-30 19:49:57',1,'2016-12-29 10:18:17','0000-00-00 00:00:00'),(174,'1491726657',2,6,1,10,0,'','م كافة واعلموا ان الله مع المتقين','2016-12-30 19:49:57',1,'2016-12-29 10:17:14','0000-00-00 00:00:00'),(175,'1491726657',2,6,3,10,0,'14796958370.jpg','م كافة واعلموا ان الله مع المتقين','2016-12-30 19:49:57',1,'2016-12-29 10:15:37','0000-00-00 00:00:00'),(176,'1491726658',2,7,1,10,0,'','','2016-12-30 19:49:57',0,'2016-12-29 10:13:21','0000-00-00 00:00:00'),(177,'1491726658',2,6,1,10,0,'14796498270.jpg','HAPPY NEW YEAR','2016-12-30 19:49:57',0,'2016-12-29 10:11:06','0000-00-00 00:00:00'),(178,'1491726659',3,8,3,10,0,'','','2016-12-30 12:56:20',1,'2016-12-25 19:44:57','0000-00-00 00:00:00'),(179,'1491726659',1,2,2,10,0,'','','2016-12-30 12:56:22',1,'2016-12-25 19:44:57','0000-00-00 00:00:00'),(180,'1491726660',1,1,1,10,0,'','','2016-12-30 19:49:57',1,'2016-12-25 19:48:53','0000-00-00 00:00:00'),(181,'1491726660',2,3,2,10,0,'','','2016-12-30 19:49:57',1,'2016-12-25 19:49:57','0000-00-00 00:00:00'),(186,'1481726664',1,1,1,10,1,'14796958370.jpg','','2016-12-30 19:49:57',0,'2016-12-14 12:44:07','0000-00-00 00:00:00'),(187,'1481726664',2,1,2,10,0,'','م كافة واعلموا ان الله مع المتقين','2016-12-30 19:49:57',0,'2016-12-14 12:44:07','0000-00-00 00:00:00'),(188,'1481726664',2,1,3,10,1,'14796951710.jpg','','2016-12-30 19:49:57',0,'2016-12-14 12:44:07','0000-00-00 00:00:00'),(189,'1481726664',3,8,1,10,1,'14804483700.jpg','','2016-12-30 19:49:57',0,'2016-12-14 12:44:07','0000-00-00 00:00:00'),(190,'1481726664',1,3,2,10,0,'','HAPPY NEW YEAR','2016-12-30 19:49:57',0,'2016-12-14 12:44:07','0000-00-00 00:00:00'),(191,'1481726664',1,3,1,10,1,'14796498270.jpg','','2016-12-30 19:49:57',0,'2016-12-14 12:44:07','0000-00-00 00:00:00'),(192,'1481726664',1,10,3,10,1,'','','2016-12-30 19:49:57',0,'2016-12-14 12:44:07','0000-00-00 00:00:00');

/*Table structure for table `occ_occasions` */

DROP TABLE IF EXISTS `occ_occasions`;

CREATE TABLE `occ_occasions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1' COMMENT '0-avtive, 1-inactive',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`,`name`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `occ_occasions` */

insert  into `occ_occasions`(`id`,`name`,`description`,`status`,`created_at`,`updated_at`) values (1,'Birthday','birthday',0,'2016-11-17 19:50:57','2016-11-17 14:47:57'),(2,'Love ','love',0,'2016-11-17 19:51:13','2016-12-18 15:35:35'),(3,'Graduation','for graduation',1,'2016-12-16 01:20:22','2016-12-16 01:20:22'),(4,'Anniversary','anniversary',1,'2016-11-17 19:51:13','2016-11-17 13:35:34'),(5,'New baby','for new baby',1,'2016-12-16 01:19:43','2016-12-16 01:19:43'),(6,'New Year ','new year',1,'2016-11-17 19:51:13','2016-11-17 13:35:55'),(7,'Father’s Day ','father\'s day',1,'2016-11-17 19:51:13','2016-11-17 13:36:10'),(8,'Mother’s Day','mother\'s day',1,'2016-11-17 19:51:13','2016-11-17 13:36:24'),(9,'Valentine’s Day','valentine\'s day',1,'2016-11-17 19:51:13','2016-11-17 13:36:45'),(10,'Friends ship','for frieds ship',1,'2016-12-16 01:21:05','2016-12-20 09:49:30'),(11,'Wedding','for frieds ship',1,'2016-12-16 01:21:20','2016-12-16 01:21:20'),(12,'For him','for him',1,'2016-12-16 09:23:55','0000-00-00 00:00:00'),(13,'For her','for her',1,'2016-12-18 16:05:40','2016-12-18 16:05:40');

/*Table structure for table `occ_payment_methods` */

DROP TABLE IF EXISTS `occ_payment_methods`;

CREATE TABLE `occ_payment_methods` (
  `id` int(225) unsigned NOT NULL AUTO_INCREMENT,
  `method` varchar(225) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `occ_payment_methods` */

insert  into `occ_payment_methods`(`id`,`method`,`status`,`created_at`) values (1,'Credit Card',1,'2016-12-22 12:49:08'),(2,'PayPal',1,'2016-12-22 16:37:20'),(3,'Cash on delivery',0,'2016-12-22 16:37:25');

/*Table structure for table `occ_product_photos` */

DROP TABLE IF EXISTS `occ_product_photos`;

CREATE TABLE `occ_product_photos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(10) unsigned NOT NULL DEFAULT '0',
  `photo` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `product` (`product_id`),
  CONSTRAINT `product` FOREIGN KEY (`product_id`) REFERENCES `occ_products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `occ_product_photos` */

insert  into `occ_product_photos`(`id`,`product_id`,`photo`,`created_at`,`updated_at`) values (31,6,'14798763770.jpg','2016-11-23 12:46:45','0000-00-00 00:00:00'),(32,7,'14798764490.jpg','2016-11-23 12:47:29','0000-00-00 00:00:00'),(33,8,'14798764840.jpg','2016-11-23 12:48:04','0000-00-00 00:00:00'),(34,9,'14798765140.jpg','2016-11-23 12:48:34','0000-00-00 00:00:00'),(35,3,'14800048570.jpg','2016-11-25 00:27:37','0000-00-00 00:00:00'),(55,2,'14802941890.jpg','2016-11-28 08:49:49','0000-00-00 00:00:00'),(60,1,'14824546900.jpg','2016-12-23 08:58:10','0000-00-00 00:00:00'),(64,5,'14825165330.jpg','2016-12-24 02:08:53','0000-00-00 00:00:00'),(65,10,'14825447430.jpg','2016-12-24 09:59:03','0000-00-00 00:00:00');

/*Table structure for table `occ_products` */

DROP TABLE IF EXISTS `occ_products`;

CREATE TABLE `occ_products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `description2` varchar(225) COLLATE utf8_unicode_ci NOT NULL,
  `photos` text COLLATE utf8_unicode_ci NOT NULL,
  `has_large_size` tinyint(1) NOT NULL DEFAULT '0',
  `has_medium_size` tinyint(1) NOT NULL DEFAULT '0',
  `has_small_size` tinyint(1) NOT NULL DEFAULT '0',
  `price` float DEFAULT '0',
  `medium_size_price` float NOT NULL DEFAULT '0',
  `small_size_price` float NOT NULL DEFAULT '0',
  `large_size_display_name` varchar(225) COLLATE utf8_unicode_ci NOT NULL,
  `medium_size_display_name` varchar(225) COLLATE utf8_unicode_ci NOT NULL,
  `small_size_display_name` varchar(225) COLLATE utf8_unicode_ci NOT NULL,
  `amount` int(10) NOT NULL DEFAULT '0',
  `number_of_persons` int(10) NOT NULL DEFAULT '0',
  `falldown` float NOT NULL DEFAULT '0',
  `p_contact_id` int(10) unsigned NOT NULL,
  `s_contact_id` int(10) unsigned NOT NULL,
  `occ_id` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT ',',
  `admin_id` int(10) unsigned NOT NULL DEFAULT '1',
  `state` int(1) NOT NULL DEFAULT '1' COMMENT '0-inactive, 1-active',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `category` (`category_id`),
  CONSTRAINT `category` FOREIGN KEY (`category_id`) REFERENCES `occ_categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `occ_products` */

insert  into `occ_products`(`id`,`category_id`,`name`,`description`,`description2`,`photos`,`has_large_size`,`has_medium_size`,`has_small_size`,`price`,`medium_size_price`,`small_size_price`,`large_size_display_name`,`medium_size_display_name`,`small_size_display_name`,`amount`,`number_of_persons`,`falldown`,`p_contact_id`,`s_contact_id`,`occ_id`,`admin_id`,`state`,`created_at`,`updated_at`) values (1,1,'cake1','This is cake1.','كافة كما يقاتلونكم كافة واعلموا ان الله مع المتقين','',1,1,1,20,15,10,'large cake1','medium cake1','small cake1',100,35,0,3,3,'1,3',3,0,'2016-11-28 00:49:03','2016-12-23 01:08:44'),(2,1,'cake2','This is cake2.','سكم وقاتلوا المشركين كافة كما يقاتلونكم كافة واعلموا ان الله مع المتقين','',1,1,1,20,15,10,'large cake2','medium cake2','small cake2',110,20,0,3,3,'4,5,9,11',3,1,'2016-11-28 00:49:49','2016-12-23 01:09:56'),(3,1,'cake3','This is cake-4.','سكم وقاتلوا المشركين كافة كما يقاتلونكم كافة واعلموا ان الله مع المتقين','',1,1,1,20,15,10,'large cake3','medium cake3','small cake3',100,30,0,3,3,'1,7,11',3,1,'2016-11-24 16:27:37','2016-12-22 18:52:49'),(4,2,'flower1','This is flower1.','سكم وقاتلوا المشركين كافة كما يقاتلونكم كافة واعلموا ان الله مع المتقين','',1,1,0,20,15,0,'large flower1','medium flower1','',100,25,0,3,3,'5,12',3,0,'2016-11-23 04:45:46','2016-11-28 01:17:21'),(5,2,'flower2','This is flower2','','',1,0,0,20,0,0,'flower2','','',100,0,0,3,3,'1,5',3,1,'2016-12-23 18:08:53','2016-12-23 18:08:53'),(6,2,'flower3','This is flower3.','سكم وقاتلوا المشركين كافة كما يقاتلونكم كافة واعلموا ان الله مع المتقين','',1,0,1,20,0,10,'large flower3','','small flower3',100,35,0,2,1,'2,8,9',2,1,'2016-11-23 04:46:45','2016-11-23 04:46:45'),(7,3,'chocolate1','This is chocolate1.','سكم وقاتلوا المشركين كافة كما يقاتلونكم كافة واعلموا ان الله مع المتقين','',1,0,1,20,0,10,'large chocolate1','','small chocolate1',100,20,0,2,1,'1,3',2,1,'2016-11-23 04:47:29','2016-12-23 01:16:37'),(8,3,'chocolate2','This is chocolate2.','سكم وقاتلوا المشركين كافة كما يقاتلونكم كافة واعلموا ان الله مع المتقين','',1,0,1,20,0,10,'large chocolate2','','small chocolate2',110,55,0,3,3,'10,2,6',3,1,'2016-11-23 04:48:04','2016-11-23 04:48:04'),(9,3,'chocolate3','This is chocolate3.','سكم وقاتلوا المشركين كافة كما يقاتلونكم كافة واعلموا ان الله مع المتقين','',1,0,1,20,0,10,'large chocolate3','','small chocolate3',100,10,0,3,3,'2,5',3,1,'2016-11-23 04:48:34','2016-11-23 04:48:34'),(10,1,'cake4','This is cake4.','','',1,0,0,20,0,0,'cake4','','',100,0,0,3,3,'1,4,5',3,1,'2016-12-24 01:59:03','2016-12-24 01:59:03');

/*Table structure for table `occ_shippers` */

DROP TABLE IF EXISTS `occ_shippers`;

CREATE TABLE `occ_shippers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contact_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `city_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `occ_shippers` */

insert  into `occ_shippers`(`id`,`name`,`email`,`phone_number`,`contact_name`,`city_id`,`status`,`created_at`,`updated_at`) values (1,'shipper1','shipper1@gmail.com','123451','shipper1','1',1,'2016-11-25 16:21:14','0000-00-00 00:00:00'),(2,'shipper2','shipper2@gmail.com','123452','shipper2','2',0,'2016-11-25 16:21:51','0000-00-00 00:00:00'),(3,'shipper3','shipper3@gmail.com','123453','shipper3','1,2,3',1,'2016-11-25 16:23:03','0000-00-00 00:00:00');

/*Table structure for table `occ_shippments` */

DROP TABLE IF EXISTS `occ_shippments`;

CREATE TABLE `occ_shippments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ticket_id` int(10) NOT NULL DEFAULT '0',
  `category_id` int(10) NOT NULL DEFAULT '0',
  `product_id` int(10) NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contactno` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `totalprice` float NOT NULL DEFAULT '0',
  `amount` float NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `occ_shippments` */

/*Table structure for table `occ_tickets` */

DROP TABLE IF EXISTS `occ_tickets`;

CREATE TABLE `occ_tickets` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ticket_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `vendor_id` int(10) NOT NULL,
  `customer_id` int(10) NOT NULL,
  `order_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payment_method` int(1) NOT NULL DEFAULT '1',
  `transaction_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `transaction_fee` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `totalprice` float NOT NULL DEFAULT '0',
  `status` int(10) NOT NULL DEFAULT '1' COMMENT '0:cancel, 1:paid, 2:shipped',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `occ_tickets` */

insert  into `occ_tickets`(`id`,`ticket_id`,`vendor_id`,`customer_id`,`order_id`,`payment_method`,`transaction_id`,`transaction_fee`,`description`,`totalprice`,`status`,`created_at`,`updated_at`) values (1,'1234567890',3,4,'1491726659',3,'','','',350,2,'2016-12-25 12:03:45','0000-00-00 00:00:00'),(2,'1234567891',3,5,'1491726660',1,'','','',250,1,'2016-12-26 19:44:57','0000-00-00 00:00:00'),(3,'1234567889',2,6,'1491726657',1,'','','',500,1,'2016-12-29 10:20:54','0000-00-00 00:00:00');

/*Table structure for table `occ_venders` */

DROP TABLE IF EXISTS `occ_venders`;

CREATE TABLE `occ_venders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fb_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bio` text COLLATE utf8_unicode_ci NOT NULL,
  `gender` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dob` date NOT NULL,
  `pic` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `state` int(1) NOT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `postal` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `company` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `age` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contactno` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `shipper_id` int(10) DEFAULT NULL COMMENT '0;seif,not-0;select',
  `whatsapp` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `wechat` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8_unicode_ci,
  `last_login` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `occ_venders` */

insert  into `occ_venders`(`id`,`fb_id`,`email`,`password`,`first_name`,`last_name`,`bio`,`gender`,`dob`,`pic`,`country`,`state`,`city`,`address`,`postal`,`company`,`age`,`email2`,`contactno`,`shipper_id`,`whatsapp`,`wechat`,`permissions`,`last_login`,`created_at`,`updated_at`,`deleted_at`) values (1,'','admin@admin.com','$2y$10$.w6w1vEZ.eap5VjXKwv19uOsDj1kEXL.J2G01mJWc8o5P2BR1PWFW','John','Doe','','','2016-11-08','NBEVVRZxx5.jpg','',0,'','','','','','','',0,'','',NULL,'2016-11-18 00:37:06','2016-11-07 16:13:06','2016-11-18 00:37:06',NULL),(2,'','future.syg1118@gmail.com','$2y$10$PWzxk1NKW5LmxQg77aiyU./Zhro7whzV3nOSZo4mJkudxAL269h.C','blue','star','','','0000-00-00','images.png','',0,'','','','','','','',0,'','',NULL,'2016-11-08 15:03:30','2016-11-08 15:03:30','2016-11-08 15:03:30',NULL);

/*Table structure for table `payment_method` */

DROP TABLE IF EXISTS `payment_method`;

CREATE TABLE `payment_method` (
  `id` int(2) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(225) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `payment_method` */

insert  into `payment_method`(`id`,`name`,`status`) values (1,'Credit Card',0),(2,'PayPal',0),(3,'Cash on delivery',0);

/*Table structure for table `persistences` */

DROP TABLE IF EXISTS `persistences`;

CREATE TABLE `persistences` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `persistences_code_unique` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=574 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `persistences` */

insert  into `persistences`(`id`,`user_id`,`code`,`created_at`,`updated_at`) values (1,1,'jzUT5Km1HVo86XTLejZvCfHCKBBImRIn','2016-11-07 16:19:31','2016-11-07 16:19:31'),(5,1,'oVWNGuJuWYxYwPfEi2FAhWf1RSMZXTrg','2016-11-08 15:27:38','2016-11-08 15:27:38'),(6,1,'pKcp2h4dp90m00vQJeo9nlGm9Pw04Jvt','2016-11-09 00:49:15','2016-11-09 00:49:15'),(7,1,'Gqq4uRkbmhX3jn9iqH3ai5ls09WF9PaW','2016-11-15 09:32:44','2016-11-15 09:32:44'),(8,1,'UwRkSyTcBzlAEZ0KjUQq3zjPhgMzTT78','2016-11-16 00:39:07','2016-11-16 00:39:07'),(9,1,'A07UWkn5ikruOn90TSx5VKYmLk0KR9iW','2016-11-16 06:59:22','2016-11-16 06:59:22'),(10,1,'pm5b7crqtnZybXPKgz3dweHlSJgeLbr5','2016-11-16 13:07:53','2016-11-16 13:07:53'),(11,1,'QLAnOSeCrJuDYNuoSYrS5HClDdR5fxdH','2016-11-17 00:50:38','2016-11-17 00:50:38'),(15,4,'YlezGKykQZbxLTQ0UMWsjhIPhdZyUP81','2016-11-18 10:59:17','2016-11-18 10:59:17'),(16,3,'VjL8tcUqVITHkhCJ7kzOAegwQjf0frp4','2016-11-19 01:12:18','2016-11-19 01:12:18'),(17,1,'SAqwylqndQ9o7HILLEOceKomIXNApGTC','2016-11-20 12:21:56','2016-11-20 12:21:56'),(18,1,'7AnFVEGU8G88oLbuw6SUZGsruCqE1vvH','2016-11-21 00:45:31','2016-11-21 00:45:31'),(19,1,'kR7uWs3B759zvoFpHWH3SkeV83MtyQ9i','2016-11-21 02:13:31','2016-11-21 02:13:31'),(20,1,'2tZXktkoDOgsdtbLM8m2JFX1JWR5VJu6','2016-11-21 02:24:23','2016-11-21 02:24:23'),(21,1,'fZEUPjWUsacMThwSrrU9Uf6834uq8k0z','2016-11-22 01:22:54','2016-11-22 01:22:54'),(22,1,'Y0941mTLWcOXt9M2tqwU9GnnDa1zpBSj','2016-11-23 00:48:28','2016-11-23 00:48:28'),(23,1,'5JFZ0taF2xtiYyADMf0v5ZVyWLXmmRJw','2016-11-23 02:45:03','2016-11-23 02:45:03'),(24,1,'6z6p5NjtmzmSnTrqJOg8m3N9Qz3wBk5C','2016-11-23 02:46:50','2016-11-23 02:46:50'),(25,1,'gWOFedLg7ikuXJlgkSLzCfMdzokpJwLq','2016-11-23 03:15:51','2016-11-23 03:15:51'),(28,3,'57c3Ll3pL36d0n5dCVBTNrQXmjuO85d5','2016-11-23 18:26:06','2016-11-23 18:26:06'),(34,3,'nFOWcM6lvZdmhrtnPxV0BLzexsInvJKR','2016-11-24 11:23:22','2016-11-24 11:23:22'),(35,1,'WgThsVHPJNkrRLyAKAwJlRq1twlZ1EvS','2016-11-24 11:37:53','2016-11-24 11:37:53'),(36,1,'tIF6AvHOCVaJ3WcQsmSLTHMv1AVAbyhX','2016-11-24 11:45:14','2016-11-24 11:45:14'),(39,1,'viI096NJQ5QukZiJUiQsCH5y03INJtJz','2016-11-24 13:26:32','2016-11-24 13:26:32'),(40,4,'o8GexiR7oIt4blr4oVEiJXScjYwIZ1m3','2016-11-24 15:32:59','2016-11-24 15:32:59'),(41,1,'KZky9S55qprVoHXYA1fY2Z7ljPriUzj5','2016-11-24 16:18:03','2016-11-24 16:18:03'),(42,4,'iqElEVKUTVm4LWEGObu8gsAXR7FStab5','2016-11-24 16:23:50','2016-11-24 16:23:50'),(43,1,'wIY2RB9AknkszinmpDxvYAFTF8qX0hpD','2016-11-24 16:53:12','2016-11-24 16:53:12'),(44,4,'rhIzzMjwPltwUUHtqZR3zBI2ZOHhKHkG','2016-11-24 16:53:51','2016-11-24 16:53:51'),(45,1,'a8R0ZS064yZAjXKKgf4Es1YOVYNPXcc2','2016-11-24 19:09:51','2016-11-24 19:09:51'),(46,4,'jjN59UU9j919g98nnMjBFiWrWqzZx7Yd','2016-11-25 00:52:40','2016-11-25 00:52:40'),(47,1,'0IdvwqNPuE1RmTR1vxX4VMGBBwhb8Cjm','2016-11-25 00:54:20','2016-11-25 00:54:20'),(48,1,'58UgcDO9YckpRSSk3WMOlOEuqCqvgL09','2016-11-25 01:06:49','2016-11-25 01:06:49'),(49,1,'XXGjsENCfy4OX1ogePk4qZBEntrG3XpE','2016-11-25 02:20:55','2016-11-25 02:20:55'),(50,4,'ndOqSavYUq0ERVJ2SjOzehrpWCZ7Jf0Y','2016-11-25 02:23:13','2016-11-25 02:23:13'),(51,3,'VFqbBfxgoE26yeclBBn8eapAfq1RWOkE','2016-11-25 02:42:45','2016-11-25 02:42:45'),(52,1,'hw2SpMbvt9Iz0V6wmiwOg3usrqWrhudK','2016-11-25 03:03:47','2016-11-25 03:03:47'),(53,1,'1iGIJY0bG9QAsKAKKsKYNLO8uj7zbdlO','2016-11-25 03:05:49','2016-11-25 03:05:49'),(54,4,'L2gDi7d4PX5X4SL1c5jiWOdtkBpLmet0','2016-11-25 03:06:11','2016-11-25 03:06:11'),(55,1,'dM9KHxNrRPdDr3ooCAERlPcnCyFpikh8','2016-11-25 03:54:20','2016-11-25 03:54:20'),(56,1,'tunOWE7RcGyC5G3EINRFyePomXuF5QC0','2016-11-25 03:54:48','2016-11-25 03:54:48'),(57,4,'NUWFssRulBOXDt91DVtWDXkvvGj4nq3O','2016-11-25 03:55:16','2016-11-25 03:55:16'),(58,1,'uPLEujbaCwds0d5phRKYf6X1TfSPMXOm','2016-11-25 04:04:56','2016-11-25 04:04:56'),(59,4,'AY4vmh274ik1R4F1kqYU6yYY9dDk7ABv','2016-11-25 04:05:22','2016-11-25 04:05:22'),(60,1,'F5fuz3gm5m5lJB8onqGSihcdSVU7aV7u','2016-11-25 04:18:56','2016-11-25 04:18:56'),(61,4,'AG8W4Zej3qrxnPqe6CdgzYxjmG1MbuPD','2016-11-25 04:22:24','2016-11-25 04:22:24'),(62,1,'M63ic1cYUVka6iSzIlbeeWDloVaR1IGL','2016-11-25 06:29:26','2016-11-25 06:29:26'),(63,1,'dNPtMapJKzcMwqjjbrvgFRjP3Qg3KyPZ','2016-11-25 06:30:09','2016-11-25 06:30:09'),(64,4,'JiiDpdwp4TLvCR4NNqEmvIx7l2ojDOM5','2016-11-25 06:31:23','2016-11-25 06:31:23'),(65,1,'I6FNBov7DGNo2dauIQD5RJAaxdTniyZP','2016-11-25 06:32:11','2016-11-25 06:32:11'),(66,1,'e4FQjPjiCcjRLTatcK8PrVMXdyIoeaaj','2016-11-25 06:35:14','2016-11-25 06:35:14'),(67,4,'E3ovW3sJ37xOBxpUV3v38OivCo9I1dIs','2016-11-25 06:48:46','2016-11-25 06:48:46'),(68,4,'Nb196eFzA8FtWcVfujBthCAvzlXV7rZr','2016-11-25 07:17:15','2016-11-25 07:17:15'),(69,4,'tJrpQXtGjMJPlxHn24TvDumVEx0QhotI','2016-11-25 07:27:25','2016-11-25 07:27:25'),(70,1,'dqoIRVe4SKQsW03RcgcItovvvvZoSCoN','2016-11-25 07:43:59','2016-11-25 07:43:59'),(71,1,'h7zbRoqcDiMt2fqMN91QqP6N97eTG9tj','2016-11-25 07:44:58','2016-11-25 07:44:58'),(72,4,'H70iruTUTtQyt6GMuSz3jgQqVqGfVO6f','2016-11-25 07:47:30','2016-11-25 07:47:30'),(73,1,'TKj7QNNg62m8R7Ln6lsZMhV21nn5da4c','2016-11-25 09:56:45','2016-11-25 09:56:45'),(74,1,'kObd6sHZhwo5H5ckNBEFdUA0GnsH7quA','2016-11-25 09:57:58','2016-11-25 09:57:58'),(75,4,'w4FDdD8eqopx831S7mUVB0zB5tPvvcRh','2016-11-25 10:02:17','2016-11-25 10:02:17'),(76,1,'WWKWWco71OYJhkILX1SCXLUpQMIV0uYF','2016-11-25 10:05:06','2016-11-25 10:05:06'),(77,1,'avlnTODAXCQFyuA0Kl9fJZ0MzgFaFnHp','2016-11-25 10:09:15','2016-11-25 10:09:15'),(78,1,'sYkHEtalMtqpUAJtEV0xbaAooE9j1Aei','2016-11-25 10:18:28','2016-11-25 10:18:28'),(79,4,'QtexPVDRqOukNC6UvO7HozsacbEWVzfM','2016-11-25 10:18:41','2016-11-25 10:18:41'),(80,1,'1yZr5W5w5Beg85TRyyI1jIoWCKAmVART','2016-11-25 10:41:24','2016-11-25 10:41:24'),(81,1,'tVwB1s806a94yESF6Rd8PqSOj3PwC4Hm','2016-11-25 10:42:22','2016-11-25 10:42:22'),(82,4,'6TScXlo55xqSmAl1dHNZCODUE9FyBIrt','2016-11-25 10:43:40','2016-11-25 10:43:40'),(83,4,'EC1loqnW3Wj3Cjr4iwJ9Opsj62ecqgKF','2016-11-25 10:52:00','2016-11-25 10:52:00'),(84,1,'VHCj25usvM0rJiArGjgEZAgNs1eEDx8h','2016-11-25 12:31:20','2016-11-25 12:31:20'),(85,4,'vSUdiU9WcXcMLpjfnbXBdqVxPamI3pav','2016-11-25 12:31:54','2016-11-25 12:31:54'),(86,1,'p81HkhivxBmBgq2ZpkrQhJcWcunvQPFy','2016-11-25 12:37:22','2016-11-25 12:37:22'),(87,4,'zOyO9VrqBYzbHH6YZnM80VrfQQSklX6i','2016-11-25 12:37:42','2016-11-25 12:37:42'),(88,1,'bZaaRay6a0r970Rf6x6uHJjbrLFbVGOW','2016-11-25 12:41:04','2016-11-25 12:41:04'),(89,4,'BRgi2TwwPEZ4GUyL2OTFkQRjOAr1ok2H','2016-11-25 12:41:59','2016-11-25 12:41:59'),(90,1,'ArOKYMt5uChoeBwBSl4VvGAuv3x2YmU6','2016-11-25 12:48:56','2016-11-25 12:48:56'),(91,4,'UeUWscPs7RXWyBbaBBpG6gPkRxMjjulZ','2016-11-25 12:55:56','2016-11-25 12:55:56'),(92,1,'EyotLUoesIg1lsUTZBTkJ9ULc4qQNKwT','2016-11-25 12:56:40','2016-11-25 12:56:40'),(95,1,'us4vG3stAYEh3D7AKBaK65R7eQdUtxO7','2016-11-25 13:12:14','2016-11-25 13:12:14'),(96,4,'xbApOTGXgn5XWs5cTSu5Fl9nAdTJlIwW','2016-11-25 13:36:39','2016-11-25 13:36:39'),(97,1,'FdNAJ8V7VMVpZlG6cEJ8YtIVbeSquXRD','2016-11-25 13:39:35','2016-11-25 13:39:35'),(98,4,'YrSiBdSngvpCmw9Ftd2xuDAWfW5x1bmB','2016-11-25 13:49:38','2016-11-25 13:49:38'),(99,1,'8dnvN7UR89gYp5A7DSaIN0eU9uxENlMt','2016-11-25 13:50:20','2016-11-25 13:50:20'),(100,1,'axlFCP5wlFticm6Q03MKVdphVK0w9fmc','2016-11-25 15:07:31','2016-11-25 15:07:31'),(101,1,'qP0g32DVEiSZUfMYPVf19T7OT5AQJn3Q','2016-11-26 01:16:11','2016-11-26 01:16:11'),(102,4,'hrEbkQaw8kQINr52hCFGY783BNePtYjG','2016-11-26 08:11:13','2016-11-26 08:11:13'),(103,1,'PP8lWoPBLMNlURT7Iw3yiNiUH8sxNhdA','2016-11-26 08:12:40','2016-11-26 08:12:40'),(104,1,'oqLelB1w6lrzb35phTHfId3LQaYGmWQ3','2016-11-26 09:15:18','2016-11-26 09:15:18'),(105,1,'eBBK7F4UnjkgKH524CQj4Hxr2DIsAyHe','2016-11-26 09:34:30','2016-11-26 09:34:30'),(106,4,'LcVNeciIVjpPLroIwzNgw03oALXw103y','2016-11-26 11:04:02','2016-11-26 11:04:02'),(107,1,'gUmvw662ak43HJW3cpRnaeXJYTiut0Xt','2016-11-26 11:13:27','2016-11-26 11:13:27'),(108,4,'FnbKAkxO2UqC6BzucYJAG99EE6CFs8Md','2016-11-26 11:32:10','2016-11-26 11:32:10'),(109,1,'S3DlkK211QoGczkilZt5ZVvfIlC0anab','2016-11-26 14:48:35','2016-11-26 14:48:35'),(110,4,'qwOFk3U7RhIdI8eqnXssw5ZAZh9vR6dp','2016-11-26 14:53:19','2016-11-26 14:53:19'),(111,4,'YrTerdS5T32HwyAjQjsyxRbXbidEuacP','2016-11-26 14:53:29','2016-11-26 14:53:29'),(112,1,'U7qK7TTlfBdXamSsDYjRPyKgMv7eEmia','2016-11-26 15:05:39','2016-11-26 15:05:39'),(113,4,'Y1EDW6dqptaZqaOKbnGcXHuajI04SkUn','2016-11-26 16:35:36','2016-11-26 16:35:36'),(114,1,'2JoLu3nw7IalEuDkJByJbAW199hNVJ4Z','2016-11-26 16:39:51','2016-11-26 16:39:51'),(115,1,'T66SjnE6EO8eV4FhHBrqzRZk023jKovg','2016-11-26 16:41:58','2016-11-26 16:41:58'),(116,4,'agNw2YXYNMSoTbYeF3VC1ffNJeZ5aXSg','2016-11-26 16:42:14','2016-11-26 16:42:14'),(117,1,'yl6IrlUy5TKYLOvu9ytFNvS1h7e0OWY4','2016-11-27 11:41:24','2016-11-27 11:41:24'),(118,4,'eJTojxiRSOgxZZaCdEMgwusVJTAgMe4T','2016-11-27 12:42:44','2016-11-27 12:42:44'),(119,1,'YD3ivejzk06dD6cQyv8zIJrMF5Rc943g','2016-11-27 12:47:17','2016-11-27 12:47:17'),(120,4,'rm9rVd7E4gYtkzYr4oF7cf2XecMFrvwU','2016-11-27 12:47:24','2016-11-27 12:47:24'),(121,1,'WA8XzFoCSdoc6PhWuqmVOv9S46E00kyV','2016-11-27 12:47:38','2016-11-27 12:47:38'),(122,4,'fHkfVIYaXMkYFIyfJ3LLFvIiyUwpIEAh','2016-11-27 12:58:28','2016-11-27 12:58:28'),(123,1,'iHLyp5DCXcuR5boNFQRrCjfXy27yjFTZ','2016-11-27 13:25:49','2016-11-27 13:25:49'),(124,4,'RpEHXF85zWo5TFfrKHJ6WzgYjJpbxHBK','2016-11-27 13:25:58','2016-11-27 13:25:58'),(125,1,'94q0FyX7YhdpAI4GtcTBNE4VkNDMNH1j','2016-11-27 13:32:11','2016-11-27 13:32:11'),(126,4,'1ZwiFUTHyFhanTTaWesKVsFtgrn4PXSK','2016-11-27 13:34:14','2016-11-27 13:34:14'),(127,4,'Up912llvtUIh6OM0URzx4uWMtZuPM7VS','2016-11-27 13:40:47','2016-11-27 13:40:47'),(128,1,'5npeEOrf3M7HYq1QnAjkMvYrQDuctDwt','2016-11-27 15:31:06','2016-11-27 15:31:06'),(129,4,'ogOL2ziGlwCHcwbvaWx3WX71ylJuKk3P','2016-11-27 15:31:31','2016-11-27 15:31:31'),(130,1,'W0hOPThz724OldMuxAAYTVju82KdoWja','2016-11-27 15:42:38','2016-11-27 15:42:38'),(131,4,'lUs9uPCN6LXT1SWCYjIBLre8Fsz8k03U','2016-11-27 15:46:44','2016-11-27 15:46:44'),(132,1,'XFfZONXcJrLuxHhIEGxNw7tEHUx5vRfA','2016-11-27 15:47:06','2016-11-27 15:47:06'),(133,1,'kz5Y1rrNsRVDYQL1ydVD5uVqazmMlL57','2016-11-27 15:48:09','2016-11-27 15:48:09'),(134,4,'kOCDAwdyqzyc91CJoYh3uFtppCs4kFCm','2016-11-27 15:48:18','2016-11-27 15:48:18'),(135,1,'1lu7OvsPfgh2XTX5F8N0bMeoskWnUa8N','2016-11-27 18:23:36','2016-11-27 18:23:36'),(136,4,'nDshyd6gMGN6W6WB3zem8LAcp6U1T4Tl','2016-11-27 18:29:50','2016-11-27 18:29:50'),(137,1,'0zcpRbVY0CVgww4HlzYIe0Zx35PIOJmW','2016-11-28 00:39:30','2016-11-28 00:39:30'),(138,4,'4AQ7B6i8jIx1qKjCx3L8SHFa3aLXWAkX','2016-11-28 00:44:59','2016-11-28 00:44:59'),(139,4,'gRyjkyAA8u69zuMmPlFbvWs7qQAAIO4A','2016-11-28 01:02:29','2016-11-28 01:02:29'),(140,4,'9H2UYeIrhl0GJQJhlihRSm1h7mI5145Q','2016-11-28 08:40:35','2016-11-28 08:40:35'),(141,4,'NvOcekcdkLx5gn7MCJKSYXE4uZvpsmmR','2016-11-28 10:46:57','2016-11-28 10:46:57'),(142,1,'lQfbSVr9ScTYxRgEDGqYhqjBNdd7CKBO','2016-11-28 10:52:03','2016-11-28 10:52:03'),(143,4,'VnGodTpLkjMD1RHZ8AuVeLhG07vIf9mZ','2016-11-28 10:53:30','2016-11-28 10:53:30'),(144,1,'9xYNYOEWgFrYzZWCgJiXGcO5OTaD0aE3','2016-11-29 18:36:35','2016-11-29 18:36:35'),(145,1,'ACmw89IHgTpRSe821kT8r6Ud4niWin8l','2016-11-29 18:54:05','2016-11-29 18:54:05'),(146,1,'L5kqOtRKpUBOOJIsi7wyjgVVIiNwy8b9','2016-11-29 19:02:15','2016-11-29 19:02:15'),(147,4,'juikKNZNLIQ2es9Q360S9iU5njsdrAf9','2016-11-29 19:03:32','2016-11-29 19:03:32'),(148,4,'s5XNno0npeMKL9RqkK3ITnVX9eo5GWsO','2016-11-29 19:12:30','2016-11-29 19:12:30'),(149,1,'q2EdMAWK4COwssU5RnRlbzjXbj1WnpCA','2016-11-29 19:18:04','2016-11-29 19:18:04'),(150,4,'p6UHZ9gIvrXGt7S4Xds3OlxEi10R4md8','2016-11-29 19:32:23','2016-11-29 19:32:23'),(151,4,'f4CYYwEu2cj0n4XOI4e0cRUX860Jjfyo','2016-11-30 02:58:39','2016-11-30 02:58:39'),(152,1,'pwsTGJpeHwWyikbH83yToOzRn0B944EX','2016-11-30 03:56:46','2016-11-30 03:56:46'),(153,4,'up1E4B49Fh7EUNgdkWtABxX5cyls6SKp','2016-11-30 04:04:27','2016-11-30 04:04:27'),(154,1,'VSrhlOWJOBzPPdrP7IP58sT5UCH7Zhf5','2016-11-30 04:21:07','2016-11-30 04:21:07'),(155,4,'87CzlryOgbg26y6iUfTwqj3ydxleqiCY','2016-11-30 04:28:47','2016-11-30 04:28:47'),(156,4,'5uLPFUGMB6g8aieml9PkKiklFK19wIU1','2016-11-30 04:30:22','2016-11-30 04:30:22'),(157,1,'FzrHCporwgCvFuBIbw4mZZaUWG5xEvyC','2016-11-30 04:36:11','2016-11-30 04:36:11'),(158,4,'vifsAm07DTL05AfdTwTVRg0cpWNPweKy','2016-11-30 04:51:03','2016-11-30 04:51:03'),(159,1,'phSdQ9UKEguAWcYeCqqpfxRmhPl5WMNs','2016-11-30 04:57:27','2016-11-30 04:57:27'),(160,4,'yhRjbpUux1oorP45EPXgkejmG0wkLrbS','2016-11-30 06:53:28','2016-11-30 06:53:28'),(161,1,'wNcx8V4GENPVaoxJVCMIA0DLaZ1cLApR','2016-11-30 06:54:02','2016-11-30 06:54:02'),(162,4,'NsufnaOpoMiNmRB7nUgvA60ZNGnymQ9I','2016-11-30 07:08:50','2016-11-30 07:08:50'),(163,4,'oRlMmF0Eosvx9gMZCaWKI6yUF48KxlPy','2016-11-30 07:28:57','2016-11-30 07:28:57'),(164,4,'mMcl3lXF1bu9n1xmrS419q6lCn3UviqY','2016-11-30 08:17:32','2016-11-30 08:17:32'),(165,4,'731p0T4MB1LUyqnsp0CR5JVoQvbdxHlg','2016-11-30 08:26:26','2016-11-30 08:26:26'),(166,1,'HIznXXkKOBGU8kirrk2LHf2ICQhmfylR','2016-11-30 08:26:37','2016-11-30 08:26:37'),(167,1,'NQPWgWNdivySeU59PblmuTX33sjP2ZHD','2016-12-02 18:08:49','2016-12-02 18:08:49'),(168,4,'Bt0ASVecLvZiMoc35tUu11C9h1HWsG9J','2016-12-02 18:29:45','2016-12-02 18:29:45'),(169,1,'NZPOspoGJRquBcb5moC8kx7KE7mqdcSL','2016-12-05 01:45:00','2016-12-05 01:45:00'),(170,1,'FaKUFM8ddkXUnZ9k9RUBF6ILaxG8h58j','2016-12-05 01:58:23','2016-12-05 01:58:23'),(171,3,'sEwTClWIoUeyEDVAzERd2SGCBUmvgjtO','2016-12-05 02:18:12','2016-12-05 02:18:12'),(172,1,'8OwWeE4cq6Zd1wIztucY1S1MnuAaJxtZ','2016-12-05 02:33:47','2016-12-05 02:33:47'),(173,3,'BzvfyciVAjTeGso1Jw2h71ucvYENqnrK','2016-12-05 02:40:24','2016-12-05 02:40:24'),(174,3,'5Iapmaz4pYXWs3Gf2dj4tUhiDVp0GYW3','2016-12-05 02:40:47','2016-12-05 02:40:47'),(175,1,'LMGZi1yiFkAdo8MkXIc7zIhZ7hZ8Iw5h','2016-12-05 02:40:52','2016-12-05 02:40:52'),(176,3,'KoQta3VbWN8FH1J3dDN7ToclF4TXl6E3','2016-12-05 02:45:27','2016-12-05 02:45:27'),(177,1,'FBqVIvjWerNE3kRqd3Av1fOBUJN9ah88','2016-12-05 02:52:14','2016-12-05 02:52:14'),(178,3,'nHDvnsNQFc5qWEyPslM1wtU9GLxyxSEU','2016-12-05 02:52:42','2016-12-05 02:52:42'),(179,1,'DGwc6Bne5q46vzIcyvZ0hTW600mUEim7','2016-12-05 02:55:40','2016-12-05 02:55:40'),(180,3,'lmGEDkjuWtJiJ04zukvy7JTjCmPg1Wtl','2016-12-05 03:39:35','2016-12-05 03:39:35'),(181,3,'SjUpLUir7VgrGf1p1p06EnnhdtKOJaoM','2016-12-05 03:43:23','2016-12-05 03:43:23'),(182,3,'rnMkvcTheRVpe599C062UKRvvXmMn234','2016-12-05 03:44:41','2016-12-05 03:44:41'),(183,3,'jGhAhqGivBFbCKlRSARLdIPEpyLf8hCB','2016-12-05 03:49:39','2016-12-05 03:49:39'),(184,1,'0x1zBgRxrndzopmGDgC8kx8eFHKGf7wV','2016-12-05 03:49:51','2016-12-05 03:49:51'),(185,3,'uT2EAMrYOVE7vBdTm5JrGj12JteCFnSa','2016-12-05 03:51:57','2016-12-05 03:51:57'),(186,3,'clCoxK80JeWnvi4DAB8iBagApcYnZ9wx','2016-12-05 06:41:48','2016-12-05 06:41:48'),(187,3,'X0Hi04nFqwpNISgwN5D0y67TN1U4sigq','2016-12-05 08:00:46','2016-12-05 08:00:46'),(188,1,'ALf9arGkoJzK0qMMTjrexSgdXvThPYqr','2016-12-05 08:04:43','2016-12-05 08:04:43'),(189,1,'9isguiGNN5zmwtkJ1I0FPk815wcFqxbu','2016-12-06 00:44:50','2016-12-06 00:44:50'),(190,3,'1AxCfazHxNAqXqvGLs3JFlffnlj2P7gC','2016-12-06 06:43:20','2016-12-06 06:43:20'),(191,1,'dc0ESE2CAtAkkffmGFVaQlSXBDYFCaPR','2016-12-06 18:45:34','2016-12-06 18:45:34'),(192,3,'Q4zRFXNTMgGl7ipBAVcTt7azOhMBD58V','2016-12-06 18:58:48','2016-12-06 18:58:48'),(193,3,'R3NJIrq1zBzmIip0KJUJVey1pQAVuvWl','2016-12-07 00:54:19','2016-12-07 00:54:19'),(194,3,'8VvGacCxFGZfpMP80mjNDAxtRM7HI2Ja','2016-12-07 06:46:21','2016-12-07 06:46:21'),(195,1,'B0Ato1nWAvbsGk4IJPCfCI2uFgvuutMX','2016-12-07 07:00:47','2016-12-07 07:00:47'),(196,3,'UEYCYHL5URxQVIRwtwU9ppnwBhtvExEu','2016-12-07 09:45:10','2016-12-07 09:45:10'),(197,1,'vwPGYpligyQsAZ5OixHcUwlTvRUSBeZN','2016-12-07 10:12:05','2016-12-07 10:12:05'),(198,3,'K4xFzFGJsqVysEGhvXDvZ6LQV8wpZdby','2016-12-07 10:12:20','2016-12-07 10:12:20'),(199,1,'dmgR2dqxpG4KCeHG6vLNPfabpxUjyDNy','2016-12-07 10:23:34','2016-12-07 10:23:34'),(200,1,'QJzrzcRpKNLxGM4kCAGfT0Yxs5wp4e8u','2016-12-07 10:23:57','2016-12-07 10:23:57'),(201,3,'2WPgsL0Jv9ec8UWgDDKzFHbZIutv25wV','2016-12-07 10:42:58','2016-12-07 10:42:58'),(202,3,'AU7SYYwopk3otD4sT7us9lZUCw5bB8rU','2016-12-07 10:55:22','2016-12-07 10:55:22'),(203,1,'JhwzDA9knlzm2MEC70hfB8lar0R8thzq','2016-12-07 11:59:12','2016-12-07 11:59:12'),(204,1,'ezptSHfXdZAOn5GtCdxLbXFFTYQNxQ3U','2016-12-07 13:19:11','2016-12-07 13:19:11'),(205,3,'Gf79MncrD5VEgfCzQR0wPe30dMB6NgBx','2016-12-07 13:19:16','2016-12-07 13:19:16'),(206,3,'9VZE89NjrhyjS47mdqTnZGunH7jo7aOI','2016-12-07 13:20:14','2016-12-07 13:20:14'),(207,1,'MHeAL592yHznIzlcdqCW4ulxlEdEKW2F','2016-12-07 13:21:40','2016-12-07 13:21:40'),(208,3,'gBOmacBqb2UUBBj2GP5CERmqgpOkswNM','2016-12-07 13:28:47','2016-12-07 13:28:47'),(209,1,'rZcZvsfZVoQ55ZExpQh42jjOAEsrrq2p','2016-12-07 14:36:23','2016-12-07 14:36:23'),(210,3,'z14eTeaAfIxyw6gW2fMCXYOuR3UBKlXO','2016-12-07 14:42:40','2016-12-07 14:42:40'),(211,1,'yxWvFIMRixka6QYxMCZPvNxk7eKA8CFS','2016-12-07 15:49:49','2016-12-07 15:49:49'),(212,3,'HIlR04GWoGdoQvmMb97ib96KObkjIO9B','2016-12-07 15:49:59','2016-12-07 15:49:59'),(213,1,'tMK5honmrtVxF9GkNIjagMUCYxHYrns3','2016-12-07 16:48:48','2016-12-07 16:48:48'),(214,3,'0hgBc6NFUvAbP1VCJ5aoCw7icHuQNeSp','2016-12-07 17:25:34','2016-12-07 17:25:34'),(215,3,'EMejUYnlVlU1C8GKS9jU1m6EQ0Rr6ycS','2016-12-08 00:39:59','2016-12-08 00:39:59'),(216,1,'vgcYvNANRfPUxDFL2l9x667017hfguDL','2016-12-08 00:43:02','2016-12-08 00:43:02'),(217,3,'RCSdSOzIoCxHVHijxHw1rdp6nCo8Uqyg','2016-12-08 01:09:43','2016-12-08 01:09:43'),(218,1,'PEfxMg4Gmf6M4tjPmKrh2PcRlEAM7AAb','2016-12-08 01:21:19','2016-12-08 01:21:19'),(219,3,'33KBFjBYBnD3QlRCDHBqRZ1NCIKMiiEl','2016-12-08 01:21:35','2016-12-08 01:21:35'),(220,1,'rVNJTap6oS5rRNuqaZWOYLYMhzFHmSGn','2016-12-08 03:31:05','2016-12-08 03:31:05'),(221,1,'qqW8NeIzjkHJ16S6atgt9XRbPCffVMhb','2016-12-08 04:24:10','2016-12-08 04:24:10'),(222,3,'N7XTgZ74Yf2LgnGgwMsYQLhoAZAnYtxS','2016-12-08 04:50:12','2016-12-08 04:50:12'),(223,1,'ye7m4bvOMqCmhXC0Lou8KKzETki1FHoe','2016-12-08 04:50:27','2016-12-08 04:50:27'),(224,3,'KQOXHc7Jvh2dYQsQuAHNI6khGfg7Vwwm','2016-12-08 06:13:50','2016-12-08 06:13:50'),(225,1,'ZKqi1jeJJMDEHFMqmTOHHZQHM3RoAhQ3','2016-12-08 06:29:39','2016-12-08 06:29:39'),(226,3,'sKLf61eG4nDEov7wzAmXuX64jZQu2upI','2016-12-08 06:30:02','2016-12-08 06:30:02'),(227,1,'qhyvAdcGQDK5q4emc78HoOWmBCP2cbVd','2016-12-08 08:37:48','2016-12-08 08:37:48'),(228,1,'8valSbNyrXjh6fDCU0q4x70IO81N4b7e','2016-12-08 08:38:11','2016-12-08 08:38:11'),(229,3,'0LFsMoKYm84TPyi6H6mKuwABVsHHDffF','2016-12-08 08:38:16','2016-12-08 08:38:16'),(230,3,'ygToQDP9ysGcGt3i3pB0su05FJ5hBBoE','2016-12-08 08:46:58','2016-12-08 08:46:58'),(231,1,'cLzUVN4edUJpsKAspabctUEBOfMncs9l','2016-12-08 08:51:43','2016-12-08 08:51:43'),(232,3,'YkMy7UqTJMVeqs8ItzKC0bCME6FszJWW','2016-12-08 08:52:25','2016-12-08 08:52:25'),(233,1,'Av1wP22UrQv4uKqewFD4VUMpp37HHwCS','2016-12-08 10:38:39','2016-12-08 10:38:39'),(234,1,'d0BomVTpIcBKff1u2xIEkqTX4Eb3Upi4','2016-12-08 10:41:12','2016-12-08 10:41:12'),(235,1,'56leWBN24AI7Tvb8KDfLdw4v9dNgXaU8','2016-12-08 10:45:32','2016-12-08 10:45:32'),(236,1,'uChnDZiipDyczstProLzW0thRpJCghgY','2016-12-08 10:50:36','2016-12-08 10:50:36'),(237,3,'IrXpl26Ap7ESbpQwsHSxfjyS57tQsCjx','2016-12-08 10:55:04','2016-12-08 10:55:04'),(238,1,'QAh6UJnaDQhSYZDLDIzLykCkQJZz29fC','2016-12-08 10:57:32','2016-12-08 10:57:32'),(239,3,'R7uQwGklJGoObTtX9dsFz4GzGJVIVKEa','2016-12-08 10:58:05','2016-12-08 10:58:05'),(240,1,'p4QJAy4DDVQem5hgPKZ3XsoXNs37o1ba','2016-12-08 13:05:49','2016-12-08 13:05:49'),(241,3,'L1XrptPCbmCWdKndQtmP8e2vPSHCjEjo','2016-12-08 19:43:10','2016-12-08 19:43:10'),(242,1,'Saa4RchkZg9HliIuudcJGrJVNpXPDEsV','2016-12-08 20:01:25','2016-12-08 20:01:25'),(243,3,'Cllwu492glD4h6pxwYKGcqiaqhgmbRzG','2016-12-08 20:01:36','2016-12-08 20:01:36'),(244,3,'SobMWD23EBZxrwdRQhIRFH7e1jwehs4w','2016-12-08 20:01:45','2016-12-08 20:01:45'),(245,1,'kejV3SmvoLfbgFkwXyvq772m0b1h3Z9x','2016-12-08 20:01:52','2016-12-08 20:01:52'),(246,3,'63L0Hn3BbUK4BsmzVccuZ10nWn3aZJZB','2016-12-08 20:03:33','2016-12-08 20:03:33'),(247,1,'3aqQpKgT931nb7Uvw8aXYYt2WhuaG3yX','2016-12-08 20:03:38','2016-12-08 20:03:38'),(248,3,'FovPrAbP4dPFLCLwnSBHCWzFZYEbQtI3','2016-12-08 20:04:54','2016-12-08 20:04:54'),(249,1,'CFPnxqNDVAQrhIXReSri5mAyQgp83MG5','2016-12-08 20:07:16','2016-12-08 20:07:16'),(250,1,'QsK51SswlYTfbfiLQuWxmFBM2t92wmx8','2016-12-09 00:43:21','2016-12-09 00:43:21'),(251,3,'qd5aFFlzFL75yuuUtzjMFgnRfXg6u1f4','2016-12-09 00:44:09','2016-12-09 00:44:09'),(252,1,'tyAse8cuR5XmIbmEwKzBPoeOW7BvpHdF','2016-12-09 00:44:18','2016-12-09 00:44:18'),(253,3,'nF7wOcKZ6QsETTdHEf5cPxsSWGLdFJkF','2016-12-09 00:54:04','2016-12-09 00:54:04'),(254,3,'zdiXcxjtxMH9opFyZmdHt86TrRIXv9z2','2016-12-09 00:56:42','2016-12-09 00:56:42'),(255,1,'aFqeVG1xwsZrwYht8sAsoDGd8eye7Ni6','2016-12-09 00:56:49','2016-12-09 00:56:49'),(256,3,'6h52zzSPVq8aiwHTsx6HyZ8VDxyJTBoR','2016-12-09 01:13:15','2016-12-09 01:13:15'),(257,1,'fTtgAlbkLtwZ1QvtFoZXP8z6hQ8b6GRP','2016-12-09 01:17:21','2016-12-09 01:17:21'),(258,1,'DqEaZKtucc1B6AabG3gErmNHRiQn5PcQ','2016-12-09 01:18:08','2016-12-09 01:18:08'),(259,3,'X7NW1NhUnOO1OMfRbIbUqe5IkFQmDdui','2016-12-09 01:18:51','2016-12-09 01:18:51'),(260,1,'wbvS1DjIgFG9YyNcY054ztOsLR3NHKIy','2016-12-09 01:22:44','2016-12-09 01:22:44'),(261,3,'7lHKVCiZA8gGkTz574454hMSosUkFE3K','2016-12-09 01:24:30','2016-12-09 01:24:30'),(262,1,'qT7HIBxjFteF9G5GZX90ExVrd0qESvM7','2016-12-09 01:31:47','2016-12-09 01:31:47'),(263,3,'UPEY0QhUK82o58Edy23iYfoPivspDNnr','2016-12-09 02:45:57','2016-12-09 02:45:57'),(264,1,'67o6hgm9PLDLV7hQ54NCrJ6Zhg4KcBCp','2016-12-09 02:46:11','2016-12-09 02:46:11'),(265,1,'UXlJd1GcodceoogeABPCb9Vve13N43Kx','2016-12-09 02:46:20','2016-12-09 02:46:20'),(266,1,'A4QAkfwZV2izhIjJw0AEmEODbAixXAtc','2016-12-09 02:49:54','2016-12-09 02:49:54'),(267,3,'Py8GO2DHCscJOGOHXpI9ItJgTwIyxGic','2016-12-09 02:53:14','2016-12-09 02:53:14'),(268,1,'N5hgyn6oyh9VVLVZHwsUw5ujPpYInJet','2016-12-09 02:54:42','2016-12-09 02:54:42'),(269,3,'FvHdzVQKqkOO4pTpeb2f5X0qjqCloFZ3','2016-12-09 02:54:52','2016-12-09 02:54:52'),(270,1,'pACM3sTn59OJy6Oo0p5KtkqwKqcXmXFw','2016-12-09 02:57:16','2016-12-09 02:57:16'),(271,3,'pJnaWCk3oEEigDGPJvTygx9ovTbqAM4k','2016-12-09 02:57:22','2016-12-09 02:57:22'),(272,1,'cuWOtxH5fx9llbZ4c7wKibRz1lKLw94G','2016-12-09 03:23:24','2016-12-09 03:23:24'),(273,3,'pumJ7gfdGcYYIAZSk0nozestoohekBFL','2016-12-09 03:28:38','2016-12-09 03:28:38'),(274,1,'P3nlcXloSHPvWR5XUFZ3TDJDHuwFoDMQ','2016-12-09 04:19:13','2016-12-09 04:19:13'),(275,1,'T88EZciG2qltJb4YwX8jDjIFBdzQDf5r','2016-12-09 04:27:23','2016-12-09 04:27:23'),(276,1,'c39clkgIoaIOtQ001MwqWOztACw1CcWc','2016-12-09 04:41:57','2016-12-09 04:41:57'),(277,3,'O2EIsfUYna5S87ykn3sAEzCgwjFY1IQj','2016-12-09 04:42:03','2016-12-09 04:42:03'),(278,1,'z3mmOwPgQZWUkHU56ngOHNP2nnabkd8s','2016-12-09 04:55:15','2016-12-09 04:55:15'),(279,3,'Oghxcr1MJGXOqvZtlwZtYktw4CxuAJj0','2016-12-09 04:56:25','2016-12-09 04:56:25'),(280,1,'vL0PWbaRosmUxE27o3vMs65lUTx7WUaK','2016-12-09 04:56:59','2016-12-09 04:56:59'),(281,3,'jRb7zAKfWxKdYzs1xok16qU1jvuH22Qz','2016-12-09 04:57:34','2016-12-09 04:57:34'),(282,3,'jVTzX2h93SAAsnU3mkcL2ex8JpRmX2jM','2016-12-09 05:11:23','2016-12-09 05:11:23'),(283,1,'0T87RiS7xPoqr3SXLBpc621dSYsROziQ','2016-12-09 05:12:12','2016-12-09 05:12:12'),(284,3,'B1rMibIpLyA32h7idcwKHtxQlnvupwJf','2016-12-09 05:54:36','2016-12-09 05:54:36'),(285,1,'NmHmbG1Z2p8CxrPrYaMQjDEiLIGMhMUd','2016-12-09 05:54:48','2016-12-09 05:54:48'),(286,1,'hdwbedIcZCwM9VunQJ2GSkIgKqDWhOjz','2016-12-09 05:55:39','2016-12-09 05:55:39'),(287,3,'3oEkTI5pC18E2Tem8alHFl3v6aohfRJF','2016-12-09 06:54:00','2016-12-09 06:54:00'),(288,1,'cB1vdVkGzFJs39BaHjb95dUaZv6Ba1ip','2016-12-09 07:59:34','2016-12-09 07:59:34'),(289,3,'nHqlFZZRmWhtJRo7wfwkTo0IkuRJ4PZe','2016-12-09 09:15:02','2016-12-09 09:15:02'),(290,3,'7uIXXpXxjUqllNt0tBB5wJ0qNhagDkRZ','2016-12-09 09:44:02','2016-12-09 09:44:02'),(291,1,'jcqiTqQblBdYTVterNBatLDmegnmNOai','2016-12-09 09:44:21','2016-12-09 09:44:21'),(292,3,'dsot40PjnbwITuX5fz98Fr2e4V2XOCGq','2016-12-09 10:47:32','2016-12-09 10:47:32'),(293,1,'YXl4WcYZPukRHw5G9cVsquIZtaWhc0Ci','2016-12-09 11:30:28','2016-12-09 11:30:28'),(294,3,'jGB35XyJrWU5HPoVNJInlesNhaYp5bAY','2016-12-09 11:32:02','2016-12-09 11:32:02'),(295,1,'QNfWCn1NJBIIuCenA7UGe6BpKvfJcFND','2016-12-09 11:47:33','2016-12-09 11:47:33'),(296,3,'diaKRSq5btSUjZBjW933AM9cPIxYnygL','2016-12-09 11:50:59','2016-12-09 11:50:59'),(297,1,'r04X9rUcsF8Q2gEWhWiO1lBtm3xJMUXm','2016-12-09 11:58:24','2016-12-09 11:58:24'),(298,3,'BAbUSDEbN9ZJTiLQXyOgDG2cZaTt0Wmy','2016-12-09 12:00:38','2016-12-09 12:00:38'),(299,1,'eOJ0v6IFuasTGu9IEAmsLs6A6NzP9ehZ','2016-12-09 12:16:26','2016-12-09 12:16:26'),(300,3,'QLdXehDSyp7jGSm5jWiT0G1imhRpdGrx','2016-12-09 12:25:04','2016-12-09 12:25:04'),(301,1,'kmaPG8FFckpxqiYk2Mp5qXDswrsQ9NDM','2016-12-09 12:48:10','2016-12-09 12:48:10'),(302,3,'nkbZolqwWlpbriOxvl2MhVT1mtXLemFU','2016-12-09 12:48:29','2016-12-09 12:48:29'),(303,1,'e9tK8bHAfzyMx8QunNrAM2aBMq5Cp5fJ','2016-12-09 13:27:52','2016-12-09 13:27:52'),(304,3,'pcljkcMjzLFs3XJFcTerOL9GWKEvrMNi','2016-12-09 13:28:05','2016-12-09 13:28:05'),(305,1,'XzMmvOcIqm65hO2AgHgmmGP63m4YKJsU','2016-12-09 13:34:24','2016-12-09 13:34:24'),(306,3,'wntxxsGw9a57wyZISjsd8E18Uwh0sgAA','2016-12-09 13:34:42','2016-12-09 13:34:42'),(307,3,'9ChtOvucg5MkLD0VvZbgFkvhyg8TTVKC','2016-12-10 04:51:34','2016-12-10 04:51:34'),(308,1,'GWQsTMGvZMQrW34vPuZhaFnL0e1Af4sz','2016-12-10 07:45:57','2016-12-10 07:45:57'),(309,3,'N16RHlWkWTQ7yBcpj8v3VOTBIWeM5we7','2016-12-13 09:14:43','2016-12-13 09:14:43'),(310,3,'o39DprQgNedspRcDveihShoA8Ckv2iLo','2016-12-13 09:14:52','2016-12-13 09:14:52'),(311,1,'O885aQiSACm74wRCpVT8wDv4dFA8yeCT','2016-12-13 09:16:14','2016-12-13 09:16:14'),(312,3,'tS8QKy9XXqhpBx3Lx221vu4hM0UswQVH','2016-12-13 09:17:22','2016-12-13 09:17:22'),(313,3,'yUqHXywdJngdmpGr534BYZIF7epni3gz','2016-12-13 09:17:27','2016-12-13 09:17:27'),(314,1,'3YMBShIsCmthAdcBtWuFamjGK2PvDoo7','2016-12-13 09:23:19','2016-12-13 09:23:19'),(315,3,'9NqmeoVCbvKxFz74U7o7Rhqfd8SmCN9d','2016-12-13 09:31:05','2016-12-13 09:31:05'),(316,1,'vHTYZFDWSvJs6VIHUqDrmhP28Vrintt0','2016-12-13 09:31:13','2016-12-13 09:31:13'),(317,3,'Lwq6r0hE5hXNqB2BN00RYw0lwr8HEAz2','2016-12-13 09:40:35','2016-12-13 09:40:35'),(318,1,'8GmF3ajVRXu5pFDvtIo9AMg1MJaa4LD3','2016-12-13 09:43:12','2016-12-13 09:43:12'),(319,1,'sbZKwC0NraRAdkwpSxQvdxRkwKVLstho','2016-12-13 09:57:48','2016-12-13 09:57:48'),(320,3,'ePN8XoOPOBiW16K5un1j9r2jG3b0hZpA','2016-12-13 10:04:07','2016-12-13 10:04:07'),(321,1,'AUL3Uv3cBcEXn64gc8mXVYbLrwiSTCQj','2016-12-13 10:05:00','2016-12-13 10:05:00'),(322,1,'plcFQCHSB8L8odYq47yIuN2OfPmAR1BC','2016-12-13 13:42:49','2016-12-13 13:42:49'),(323,1,'eivITKO7gHHZeg88bIgc6Y8yCOlU2LrH','2016-12-13 15:35:57','2016-12-13 15:35:57'),(324,3,'sVjeKgFsCr7QxPdaVgw6vJdIbU5SuPB9','2016-12-13 16:01:16','2016-12-13 16:01:16'),(325,3,'mJt7JnrxLRteMLw7SFpZMwaIig7niAib','2016-12-13 18:21:36','2016-12-13 18:21:36'),(326,3,'rTc1ZCJU9Q8Ya7jV0nKRH6xySzpnw3Jx','2016-12-13 18:49:19','2016-12-13 18:49:19'),(327,3,'4LvV71qr22H3oiD2YhaPaq46gQNICsmN','2016-12-13 19:10:14','2016-12-13 19:10:14'),(328,1,'UfblyblQWBV6raRZK4hIHI79mQYfmVbB','2016-12-13 19:10:20','2016-12-13 19:10:20'),(329,1,'CrJfb6kAt0bcTpVlpO9P2COIX1onThlp','2016-12-14 00:54:07','2016-12-14 00:54:07'),(330,1,'mLKy2UxBFzKlHlZV6uHO451ZQV5fNSUm','2016-12-14 05:05:49','2016-12-14 05:05:49'),(331,3,'NjVtUFLnRyT6w3hxSIv7YXZncC2xsLek','2016-12-14 08:30:23','2016-12-14 08:30:23'),(332,3,'oeD52u91KuNBPsBsuffwcWdAKq0kY7WX','2016-12-14 08:32:21','2016-12-14 08:32:21'),(333,1,'QstU8IHUoaV8kyHEq1Ej6oJlxVMroxNw','2016-12-14 13:29:01','2016-12-14 13:29:01'),(334,3,'LH8JnYF9te13Vqz77rOF9fKSWCoGOtfC','2016-12-14 13:29:12','2016-12-14 13:29:12'),(335,3,'ePTVgR5BJCVVgAwGLkAdoNRTufPCcjH8','2016-12-14 13:29:18','2016-12-14 13:29:18'),(336,1,'EPPQTW2v1B4x9F1PSDrOTC6DP5Gr6uNb','2016-12-14 14:46:02','2016-12-14 14:46:02'),(337,1,'h85qHwkRIO6Pa05OjT1qVhVEWxIK8EGE','2016-12-14 18:10:48','2016-12-14 18:10:48'),(338,1,'ShXXcjuaGOOrqtKjXaQPW9BpIw82KngH','2016-12-15 01:14:49','2016-12-15 01:14:49'),(339,3,'2MViXUp733eGc6vyQ0IujCo5Vpdr218L','2016-12-15 02:25:05','2016-12-15 02:25:05'),(340,1,'JBmUINQqVx2YlpEFF81haBdJPPLPqHOy','2016-12-15 02:26:50','2016-12-15 02:26:50'),(341,3,'lmYTTk1RmpZpiEfrUBu6aeyn8v1BFiGw','2016-12-15 02:29:37','2016-12-15 02:29:37'),(342,1,'e4xp05VAsDMzkjcKbf4Y8tcIpBRnXSQi','2016-12-15 02:56:08','2016-12-15 02:56:08'),(343,3,'RH91LJpdqzjYZqpsn9s9yLkBCRXQN1c0','2016-12-15 03:56:43','2016-12-15 03:56:43'),(344,1,'ch9murDCKrO3HpzBnNgLquuff73UVEZd','2016-12-15 07:39:17','2016-12-15 07:39:17'),(345,3,'4OYUs0V9RwaZOx9Gn6y1GmupljIcMNHn','2016-12-15 17:11:09','2016-12-15 17:11:09'),(346,1,'isi7aXwBGmUFK5tR3hJ8tiD2p7bhWRIS','2016-12-15 17:18:07','2016-12-15 17:18:07'),(347,1,'d3Fkg4zzm9A3AN5ZxSkwvYavKeSVTFnX','2016-12-15 17:29:22','2016-12-15 17:29:22'),(348,3,'oMs9uaQlFrM1Mfm1M9pB9kGaLHpdA2vY','2016-12-15 17:48:53','2016-12-15 17:48:53'),(349,3,'96YAGekeQ8qNdDj3yV4XGROkVKyN0A4z','2016-12-15 18:01:35','2016-12-15 18:01:35'),(350,3,'MW9Mav6A1Kmzs2t1RPt3BFjo06rVCBUx','2016-12-15 18:02:08','2016-12-15 18:02:08'),(351,1,'xsp97RScxmTdq4cBzB2xILiVrT8RxVgf','2016-12-15 18:06:39','2016-12-15 18:06:39'),(353,3,'Yv7CG7JADb1vsOB9atS2a4bUEPoxjwwZ','2016-12-15 18:10:19','2016-12-15 18:10:19'),(354,3,'W8aE43b0KhMR0R8CrBBcg7uSUPYKUO5C','2016-12-15 18:10:39','2016-12-15 18:10:39'),(356,3,'UD20T2XsKCRGIjpwAWgkEXVyOxB2TY47','2016-12-15 18:12:56','2016-12-15 18:12:56'),(378,1,'7tU2EUcHjOhVEPii66Se6ts1cdz8b6Nv','2016-12-15 18:25:06','2016-12-15 18:25:06'),(380,1,'Vp3y0BDPNoOjFssZ7GE6M8zAwzgUkKVR','2016-12-15 18:29:31','2016-12-15 18:29:31'),(381,3,'wxsoTur8CPR6Dmc02gyINDaKcC48VBMZ','2016-12-15 18:29:52','2016-12-15 18:29:52'),(382,1,'6d9uGZUIjCYONFR5O07AZAB4neEP5RLp','2016-12-15 18:30:03','2016-12-15 18:30:03'),(384,1,'CImk5MysmCxK8WKiBfGKShnKeRHKGD8j','2016-12-15 18:32:48','2016-12-15 18:32:48'),(386,1,'qZueqUJtg8jac1PHJ62b1CZadTbfOqq8','2016-12-15 18:38:45','2016-12-15 18:38:45'),(388,1,'H8iKCVYD5G6XcgDsEV75l62ytHuNmxwc','2016-12-15 18:40:28','2016-12-15 18:40:28'),(389,3,'C1xecDeJ26TI0ghJcDAo5nMpM1uKlwSQ','2016-12-15 18:40:36','2016-12-15 18:40:36'),(391,1,'62o3xHVQefGFyTCBk3qfhvCYiKmoZQbW','2016-12-15 18:41:47','2016-12-15 18:41:47'),(394,1,'4ljMXKW9GCf0K6TOJgU3gZE8DY2ewyxz','2016-12-15 18:42:03','2016-12-15 18:42:03'),(396,1,'TqsReGAAH9G4ui06K6TWFP84eOpV3Se4','2016-12-15 18:43:08','2016-12-15 18:43:08'),(398,1,'cYvrZ77sJUgpinRNeWPdIKF0FptsYxpw','2016-12-15 18:46:15','2016-12-15 18:46:15'),(400,1,'HGSvAY4ISR3YY5cTxSxJnlszCC1Ro9V0','2016-12-16 00:57:22','2016-12-16 00:57:22'),(401,3,'YQye29it9hJxW6Zby2zHW3m7OObmVT3p','2016-12-16 00:58:46','2016-12-16 00:58:46'),(402,1,'GAN9DuD40DTma0qkRVQkQOsenxqMg5rg','2016-12-16 01:01:24','2016-12-16 01:01:24'),(404,1,'bP3VKxPGC4JVW7K90AcGMTIbqMD43qpZ','2016-12-16 01:01:40','2016-12-16 01:01:40'),(406,1,'d0cHfUwNLPHa1TSRVBMMBBwqNc9RGKIV','2016-12-16 01:02:08','2016-12-16 01:02:08'),(408,1,'2oqVk3pNrffzt8WyhE3t9kJFIaRV7UFT','2016-12-16 01:02:21','2016-12-16 01:02:21'),(411,1,'XOCot9SGxlPQC204nV3ZcDsG6vNl5wHV','2016-12-16 01:16:22','2016-12-16 01:16:22'),(412,1,'MmKlm2VjdGHfBSBZwkgvqrItYAqiS73B','2016-12-16 04:39:14','2016-12-16 04:39:14'),(413,1,'srq0USRAG8WARguNDSOE6ZQ78qdHjfsj','2016-12-17 01:40:06','2016-12-17 01:40:06'),(414,1,'Dyt89kvLi6zKFIO8NdKdDLEuXsYFWxRf','2016-12-17 07:46:33','2016-12-17 07:46:33'),(415,1,'RmyACv4t3q486RvMw5riOlmOirpD0fJ4','2016-12-17 14:49:15','2016-12-17 14:49:15'),(416,1,'Tmrh20epcJPueWWPSb42QgTFbwN5A7Z8','2016-12-18 07:40:10','2016-12-18 07:40:10'),(417,1,'Um7hAxVWSUjhsBWgAHgo4wjfrM1y10Ir','2016-12-18 09:26:51','2016-12-18 09:26:51'),(418,1,'zZr1pPY4RpN8hP9K4ZruooE5IBVAxFnP','2016-12-19 02:28:05','2016-12-19 02:28:05'),(420,1,'RyBnvZuAsqmqye97YnkSZ8IiCAgZSAhs','2016-12-19 03:57:43','2016-12-19 03:57:43'),(421,1,'hPrFFpa699fyzvIp9dbmF9ZqVBXL7p13','2016-12-19 03:59:17','2016-12-19 03:59:17'),(422,3,'5Q2WUGqdAB1xssXmHipWTakNt7zoqjNL','2016-12-19 04:47:03','2016-12-19 04:47:03'),(423,3,'r4Tzu1Ri2MCf025HZZjNdtlqYjbk2Dgf','2016-12-19 07:02:08','2016-12-19 07:02:08'),(424,3,'LuA4sHHLKFaptdF730RmJIdf2j9Iv7JR','2016-12-19 07:04:53','2016-12-19 07:04:53'),(425,1,'vVTq8XaqqnnnACffaVHqc3H2DdBaUhH2','2016-12-19 07:22:36','2016-12-19 07:22:36'),(426,3,'iiiHTwzjJC1WI4POfQT0TyDhnBbydvOg','2016-12-19 07:32:37','2016-12-19 07:32:37'),(427,1,'3tP1OBEREvcv7eEuwTQYJbAPfNBJ1I2o','2016-12-19 07:32:48','2016-12-19 07:32:48'),(428,1,'coJuERh2xiBGHkh6ZygM0iHWdRBVcZni','2016-12-19 14:28:44','2016-12-19 14:28:44'),(429,1,'5mFzFGZd1Qdy1nvrBoHbSB2Tw891rCHX','2016-12-19 14:50:01','2016-12-19 14:50:01'),(430,1,'NuUbYNEL7yzMYfyA5Z5yW9z6vWzUWLgV','2016-12-20 00:56:49','2016-12-20 00:56:49'),(431,3,'8qktSnVriMkemGe1nX2TufKv79wEvfYX','2016-12-20 10:35:48','2016-12-20 10:35:48'),(432,1,'ty29mQnGkpkSVf2sDDuL0OeN6mwovLOV','2016-12-20 10:47:14','2016-12-20 10:47:14'),(433,3,'YOJKBFAvuyWHAoOmzQzolCZkrr5IQ7gV','2016-12-20 10:49:34','2016-12-20 10:49:34'),(434,1,'rZmNO5k1OhR9fHDkqz9b76RH76kq5O8b','2016-12-20 10:49:50','2016-12-20 10:49:50'),(435,3,'J5Esv6htgmqIHK8Oqcz44gHcf9cu3wIz','2016-12-20 11:05:53','2016-12-20 11:05:53'),(436,3,'QVMz81XIEbluBTYwtqsoyYWukkcRPiM1','2016-12-20 11:25:35','2016-12-20 11:25:35'),(437,1,'7Kdn3fwb8QYUN3xzk9S2GIFxjEDn3mr6','2016-12-20 11:25:41','2016-12-20 11:25:41'),(438,3,'oBMkihzJpKJBkxQ4ycXvwsMuNUDzZHYM','2016-12-20 11:30:48','2016-12-20 11:30:48'),(439,1,'hBu2srNGasWKPYMDwFkJodpvqYljCljs','2016-12-20 11:33:14','2016-12-20 11:33:14'),(440,3,'zBtvjbSWMO4syHmsCphaouxXzVkV56nV','2016-12-20 11:36:12','2016-12-20 11:36:12'),(441,1,'yNpStWOKgiVz9x0D2gBwnBIQwfHt34Ye','2016-12-20 12:00:15','2016-12-20 12:00:15'),(442,1,'xX0MUVmNyf4e2kVChT3S8RfSPQvh7PHY','2016-12-20 13:18:42','2016-12-20 13:18:42'),(443,3,'ZlmfVKOQ7REZjr3Li1Ebf3WCh4NePIDw','2016-12-20 13:18:47','2016-12-20 13:18:47'),(444,3,'sXJaeAiaxJguCVs5ijTTnL6r9CkW0iZT','2016-12-21 00:56:01','2016-12-21 00:56:01'),(445,1,'nDPmt4ObBvwSIxeBA4xmS6kXb351z0Fk','2016-12-21 01:06:17','2016-12-21 01:06:17'),(446,3,'R1nWt28rjI7HJYBU0OtfGo5mlr1hIkz4','2016-12-21 01:14:54','2016-12-21 01:14:54'),(447,1,'SiJQl9kY5EmnlsslSnZCj2YZwPUZvkPp','2016-12-21 01:54:34','2016-12-21 01:54:34'),(448,3,'CqoJu1i3Dvtgql33dECh1StNnCqnaVUr','2016-12-21 02:48:59','2016-12-21 02:48:59'),(449,3,'TUlWBfctFqHLOhKuoDM1IwTCAZjCsdr6','2016-12-21 03:34:06','2016-12-21 03:34:06'),(450,1,'u6bxWNrYkKmxNGyy4X9a4pkiqd5ILVc2','2016-12-21 03:34:11','2016-12-21 03:34:11'),(451,3,'eL6fGq58PILc2p0moa88ZTEo4frB3uWv','2016-12-21 03:36:08','2016-12-21 03:36:08'),(452,1,'dLNyBOwqfpU0P90sq5nkxkP5uCoQJDpQ','2016-12-21 03:36:36','2016-12-21 03:36:36'),(453,3,'l5S8qzFPOlOWLBAOS8TXIoImWiRSEb39','2016-12-21 04:19:48','2016-12-21 04:19:48'),(454,3,'VB7ftVVyBJADOXacVNSQHm0SCIGp8Wkj','2016-12-21 04:21:03','2016-12-21 04:21:03'),(455,1,'s8mPnm1g8YAZKSN7Kk0va3aKICzZlev2','2016-12-21 04:21:10','2016-12-21 04:21:10'),(456,1,'WD9rs0DTdy0Gn3bfmMSx5RWU93gG6rNE','2016-12-21 07:01:42','2016-12-21 07:01:42'),(457,3,'2rLpfDBj9siNIa4Yt8lP7FT3xOspvHxR','2016-12-21 07:01:55','2016-12-21 07:01:55'),(458,1,'wX6mtpVPxmimHymLXPkqzpwWFnxzwjle','2016-12-21 07:02:06','2016-12-21 07:02:06'),(459,3,'9ZLl1qhxiUcaHgK4hJAdupx4ZUY3PP7w','2016-12-21 08:57:51','2016-12-21 08:57:51'),(460,3,'HsAPqTtIDnCXVowcEG3xsV57CX5DG7cx','2016-12-21 09:14:09','2016-12-21 09:14:09'),(461,3,'Gv9PxH764yCSdjiIAgHpUZXUteD5CAqH','2016-12-21 09:21:13','2016-12-21 09:21:13'),(462,3,'OcZDTrWBP5frc3ctkcAXWXLZmEDAH27d','2016-12-21 09:25:26','2016-12-21 09:25:26'),(463,1,'0BuWR8PxNy2TYXZeDNTTIcBwVCmn7oOO','2016-12-21 09:34:05','2016-12-21 09:34:05'),(464,3,'reyV1cbibi1ruFhAVoXpU4dLzBUkrRrZ','2016-12-21 13:30:43','2016-12-21 13:30:43'),(465,1,'kShEn77QMVqBTDE49XmALVl29wu8BBi8','2016-12-21 13:56:22','2016-12-21 13:56:22'),(466,3,'KKmkPjXAikd28HqzvzXmGc0Ac3LI50tC','2016-12-21 13:57:39','2016-12-21 13:57:39'),(467,3,'gplBaA196ol2KLGlpb7ykESUmhTMKkBu','2016-12-21 13:58:54','2016-12-21 13:58:54'),(468,1,'eCPDyMeVjXOa6B6C0jvqMZ720KHWhUUf','2016-12-21 13:59:00','2016-12-21 13:59:00'),(469,3,'i7AePU7E8vVC81MtRnfoZz8GEoYhh8pR','2016-12-21 14:25:52','2016-12-21 14:25:52'),(470,1,'QFYu8neWSP1ZkIpH9kM3HcCDcja3GlrU','2016-12-21 14:56:10','2016-12-21 14:56:10'),(471,3,'wf3kpheqCMFClYxg1lpGgHiSN0jXS2Ia','2016-12-21 15:18:03','2016-12-21 15:18:03'),(472,1,'HeWTO1gi6u2AQF9iVapCTM0tmipygZay','2016-12-21 15:52:02','2016-12-21 15:52:02'),(473,3,'8c14xMlkSOdsFgr3h9P46cI1qjlai18A','2016-12-21 16:03:38','2016-12-21 16:03:38'),(474,1,'fyEPGhEKy8lOHtQrqsBfLNVaTlhiwJQS','2016-12-21 16:06:02','2016-12-21 16:06:02'),(475,1,'atsiq7XAjLWyQKIsvJ9CjioFONGSJNkA','2016-12-21 16:44:54','2016-12-21 16:44:54'),(476,3,'bRGmbklHqYljVQxju4AHSDsQHlKF4M4B','2016-12-21 16:45:02','2016-12-21 16:45:02'),(477,1,'JJOAxsERDrFgYMVRscxlZyNlIj1PHWXa','2016-12-21 16:52:06','2016-12-21 16:52:06'),(478,3,'zQG5KFQXhBTvNMnwL5MXPvLDWGunQkjn','2016-12-21 16:57:42','2016-12-21 16:57:42'),(479,1,'6LEBMMG7G4Plq9VPHEgUGL5U5U8dWLVa','2016-12-21 16:58:08','2016-12-21 16:58:08'),(480,3,'viZCSTHh2rkpqSdpOQODgV2ARkkChN79','2016-12-21 16:58:24','2016-12-21 16:58:24'),(481,1,'xntsXZBwR6PYmhSO1eP1NJjD5WcVVFhi','2016-12-21 17:13:23','2016-12-21 17:13:23'),(482,3,'jmIKg7mmt4JsfkeZS5NHV4NOHpOATTT5','2016-12-21 17:50:47','2016-12-21 17:50:47'),(483,1,'9o4P3W2o2pJYwPmT7ZymjCXNoQvhufpL','2016-12-21 17:53:43','2016-12-21 17:53:43'),(484,3,'RjqGaGjoxn4YU6RjY2vIlNEMqtFI8wzp','2016-12-21 19:29:06','2016-12-21 19:29:06'),(485,1,'zuKavnCkGV2Zo8iLJ7SbBBUdAC28iIaf','2016-12-21 19:44:22','2016-12-21 19:44:22'),(486,1,'38QLelwVguJ5I3JnMADHctodEclHBH6L','2016-12-22 00:56:30','2016-12-22 00:56:30'),(487,3,'AtZaTZ5p9YTou7lp6sKvzekLwsdE6G26','2016-12-22 02:24:08','2016-12-22 02:24:08'),(488,1,'xPmZaYi5annRj3g9DRsxYMjVOTOEAbwT','2016-12-22 02:40:12','2016-12-22 02:40:12'),(489,3,'FIZiIStacQYu9da2iTb4h5G97ik85aRv','2016-12-22 02:45:31','2016-12-22 02:45:31'),(490,1,'zzj81uIVXW2EfBTGqBEh0E9Js84k1Nw4','2016-12-22 02:57:26','2016-12-22 02:57:26'),(491,3,'PVKhQr43m9R4K4kz2OmDsU8pic1sx5uN','2016-12-22 03:56:10','2016-12-22 03:56:10'),(492,1,'p9291EoU2eeC3T3McoaNzItnZLPIpGwK','2016-12-22 03:58:11','2016-12-22 03:58:11'),(493,3,'ycxmvj0VFUxZHSJ31pJY5jdClVjuinR4','2016-12-22 04:07:34','2016-12-22 04:07:34'),(494,1,'Bt9024dUOoQ6kcCztaZ2T5vfrlG2MO1j','2016-12-22 04:08:15','2016-12-22 04:08:15'),(495,1,'ZhXXv4IkHwoFleBXaDKaKDA3bMxPMN4m','2016-12-22 04:50:49','2016-12-22 04:50:49'),(496,1,'WU7Ovw6gHEaDSzJ7swSUcEGI4TLoyJ4V','2016-12-22 07:20:06','2016-12-22 07:20:06'),(497,3,'dLhR2cjv93crJmrptGiGdNmdKmQ5UJ25','2016-12-22 07:44:48','2016-12-22 07:44:48'),(498,1,'qCTru0S6LxnyXRDSnzFzbMDSdvRfeCc2','2016-12-22 07:45:43','2016-12-22 07:45:43'),(499,3,'bxsYu82S1N1BEH6b0kfcAAyifesMNLnT','2016-12-22 07:46:20','2016-12-22 07:46:20'),(500,1,'4Khd4eeUDpiAhVT1vHgXmCCRZdaFwZlk','2016-12-22 07:49:08','2016-12-22 07:49:08'),(501,3,'aqn1NYlHWE5Fxxm3W9etLwPOJT2tfhxP','2016-12-22 07:51:09','2016-12-22 07:51:09'),(502,1,'79BtG0bZvV8z6SOKLsva1oKrMtRhR06G','2016-12-22 07:51:20','2016-12-22 07:51:20'),(504,1,'AvkoIqdtCirc97fEWj8t1qW9wIArJTvJ','2016-12-22 08:28:53','2016-12-22 08:28:53'),(505,3,'rwQ75h29b6uzwsdqR3B3vczpZAaDGDep','2016-12-22 08:49:35','2016-12-22 08:49:35'),(506,3,'Y4V7Ucl35vbhiH4gII9j5Gku0nuWn4Yt','2016-12-22 09:05:37','2016-12-22 09:05:37'),(507,1,'uxKPz8fig7JnoPuRjfC44CirTXGcSRln','2016-12-22 09:05:45','2016-12-22 09:05:45'),(508,1,'0DecGEmflWRR97a9KfCjSz0LtkzFwWKY','2016-12-22 13:16:06','2016-12-22 13:16:06'),(509,1,'kFDQHoI8r747opOIzUJp9Nq4mvwWttWN','2016-12-22 15:52:37','2016-12-22 15:52:37'),(510,1,'DJUHXIcHipOO63V8i7O8zJaoAI8hk7T2','2016-12-22 15:54:10','2016-12-22 15:54:10'),(511,3,'LCDyMgEm2OzKCiq0VvlfEykK5etrQneK','2016-12-22 18:22:13','2016-12-22 18:22:13'),(512,3,'tfmaKYTgYGPJAERPPCGbtCCnR0lAlgRc','2016-12-23 00:56:55','2016-12-23 00:56:55'),(513,1,'fzxbBKoDp1ruWPu152wei8RScOwCmoPx','2016-12-23 01:14:05','2016-12-23 01:14:05'),(514,3,'VDAPfzVO7L4PcS71GH0j8ltayPBnObpq','2016-12-23 01:14:55','2016-12-23 01:14:55'),(515,1,'zCOflglkoskR3MatTtOK0eSXqLbGZV2i','2016-12-23 01:20:54','2016-12-23 01:20:54'),(516,3,'KfswOVlYr4gxrTVHX6Uu8RWkXR7xDURK','2016-12-23 01:24:00','2016-12-23 01:24:00'),(517,1,'JMeespLb71JbX0ADhXM8Q6Hc1BsNjN8U','2016-12-23 01:27:40','2016-12-23 01:27:40'),(518,3,'3lipVtuKBVYZwO0O5KXxY0ZvtBxaVAjB','2016-12-23 01:33:01','2016-12-23 01:33:01'),(519,1,'xcNp9RKOSGp4wqvE6uCVnjdOroFdgLAL','2016-12-23 01:41:35','2016-12-23 01:41:35'),(520,3,'xdWimH6u05p3pIVofxSvG6PHlbx1vhET','2016-12-23 01:48:06','2016-12-23 01:48:06'),(521,1,'0HMTb22s2bpVDbHGu027pTNUeCTOIEv0','2016-12-23 02:45:42','2016-12-23 02:45:42'),(522,3,'OqF2cTgzw10i2OfTk5yCJjaRJ8gtsIye','2016-12-23 02:50:52','2016-12-23 02:50:52'),(523,1,'XCqwRPMvXQFZ2pvzqeo3SRZMc9sgFKGs','2016-12-23 03:01:16','2016-12-23 03:01:16'),(524,1,'4uOilzVcFjKMNFamdwLaFFxoedKnf74v','2016-12-23 03:03:53','2016-12-23 03:03:53'),(525,3,'l6eg6mbq7Grwkp7KhNDyzJomsjoz1djZ','2016-12-23 03:03:58','2016-12-23 03:03:58'),(526,1,'dXQAzQ0M3wv9w2PZLW9wi0lS9tuV73oL','2016-12-23 03:32:39','2016-12-23 03:32:39'),(527,3,'00sMUA2kyoZL27IJOYx2LD38UUJZTp15','2016-12-23 10:54:01','2016-12-23 10:54:01'),(528,1,'zkdzxD1HrI7LbP3f5jnAJCsempAQKot9','2016-12-23 10:55:39','2016-12-23 10:55:39'),(529,3,'e3LzxKIMmTLYEbKOVWOEHrCbMgGZmWYx','2016-12-23 10:57:26','2016-12-23 10:57:26'),(530,1,'ZIVKPs1kgE57lIzo7fkGTGKVJGdadUnP','2016-12-23 11:03:19','2016-12-23 11:03:19'),(531,3,'MUHAJpY9548QOV8ttPxrQ4AZeLKlljYb','2016-12-23 16:08:19','2016-12-23 16:08:19'),(532,3,'9fTykq2Ab5h4rxadAcrAcwjjVwbSAYk4','2016-12-23 16:31:08','2016-12-23 16:31:08'),(533,1,'bCh6wuHDS7xTuMZUqvlnS6kb0zn2Eko7','2016-12-23 16:31:12','2016-12-23 16:31:12'),(534,3,'8aVOa7YJ6JRjNRnx7pcluxQNmdtVobbe','2016-12-23 18:07:57','2016-12-23 18:07:57'),(535,1,'7CmaaraAixydGGvRmN2EbXiwV0wg5lLA','2016-12-24 01:57:27','2016-12-24 01:57:27'),(536,3,'rCf3ehHrmlFeNBNS2PtNG7qgqWVFk4Pp','2016-12-24 16:54:28','2016-12-24 16:54:28'),(537,1,'ETLlr7TVYun1tJPlTpo2f2apFk7SE8Rd','2016-12-24 16:56:32','2016-12-24 16:56:32'),(538,3,'AUhALtgwXJ4sKBracbPVQXemS8CtMAZD','2016-12-24 16:59:34','2016-12-24 16:59:34'),(539,1,'G3ziBJxxQMtpMZ9bF6Hd0sZBRPt7nF54','2016-12-24 17:09:59','2016-12-24 17:09:59'),(540,3,'3IHDWNHlhY4g9pDj5PuToKxQxLVZBYTF','2016-12-24 17:10:37','2016-12-24 17:10:37'),(541,1,'EtpY91aRDNSEHsWteFuGW1L6oLut4oNN','2016-12-26 08:55:27','2016-12-26 08:55:27'),(542,3,'pR4eqZdFriReiuCvaeXoYC5ooqtHeqsA','2016-12-26 08:55:40','2016-12-26 08:55:40'),(543,1,'pYdZQOAY47KSMmGWqhgP0p9Hdubr5Xq1','2016-12-26 08:58:07','2016-12-26 08:58:07'),(544,3,'9ULgvr7AseOOkpCEv1PL3zsMOiTTaK24','2016-12-26 09:25:00','2016-12-26 09:25:00'),(545,1,'OVF2R7tLk0klq6dis9HjGoyG8iKGEIYD','2016-12-26 09:25:54','2016-12-26 09:25:54'),(546,3,'aqF2ElwA9gA5CK7mIrAkcAfsIyE4RAOI','2016-12-26 09:45:55','2016-12-26 09:45:55'),(547,1,'poju6dMOLidWmYUH8anmk9SdFuTPZQyo','2016-12-26 09:46:40','2016-12-26 09:46:40'),(548,3,'SXCJeSiu0gAODXVHd4PJMQOj7xASbLg6','2016-12-26 13:58:07','2016-12-26 13:58:07'),(549,1,'TyjQJVqqH92P34cBaSSgKATVrcfXY9z3','2016-12-26 13:58:37','2016-12-26 13:58:37'),(550,3,'ETsYKC7vkXWF1Zbq6iRioAmAC9yAmSVp','2016-12-26 14:02:17','2016-12-26 14:02:17'),(551,1,'nYhUPLSLbtsmqGQpp63jwfywPR6vTzdI','2016-12-26 14:08:07','2016-12-26 14:08:07'),(552,3,'YUyz7KmuP437up2vEuFUvH2RCpGSuFOF','2016-12-26 14:08:18','2016-12-26 14:08:18'),(553,1,'BTk9gnxf0bdwHaLwcoAzoOpsyityWTD5','2016-12-26 14:20:09','2016-12-26 14:20:09'),(554,1,'bDtND3vIEpESXEinnvFHtaOZnVmkIdac','2016-12-26 17:10:59','2016-12-26 17:10:59'),(555,3,'N1MuCGOocxnPI1rJFpHXdinQkx5kSGMP','2016-12-26 17:13:27','2016-12-26 17:13:27'),(556,1,'MWXDWax4SKaHXqVgrepagj4qx3j4ahhC','2016-12-26 17:14:14','2016-12-26 17:14:14'),(557,3,'fs1dgDmXjywXZ0Ud7DVW0s8gBZcBYBpR','2016-12-26 17:48:37','2016-12-26 17:48:37'),(558,1,'g7VbaGbrl9EGWmvD3lwjru4Y8ndkbbr2','2016-12-29 02:21:32','2016-12-29 02:21:32'),(559,3,'UcFUwTz1lLamUqJH0g0n2gZKpoXWizK4','2016-12-29 02:23:33','2016-12-29 02:23:33'),(560,1,'Eiga4BaSWYZleHanjn5HVQQfdZJWpq9O','2016-12-29 02:23:47','2016-12-29 02:23:47'),(561,3,'3m1LXm8SPByj5Zj9ah4ZoFnpXG2ktoU6','2016-12-29 03:04:05','2016-12-29 03:04:05'),(562,1,'wj1RiJ6E0J9FY6UX2Svlc6oANjE5Nd1p','2016-12-29 03:06:12','2016-12-29 03:06:12'),(563,1,'XAQUg3cNEVEhFWJYlG9g75ruweWY9nYU','2016-12-29 04:08:21','2016-12-29 04:08:21'),(564,1,'7IKp5MGVhi8p5r6OquZs9Wq16O5wkOxO','2016-12-29 04:08:35','2016-12-29 04:08:35'),(565,3,'AOdMjQbsLqT0YyUuD6e7ZDN6IUZzYcTk','2016-12-29 04:08:45','2016-12-29 04:08:45'),(566,1,'WQHEwhuB5dG2xq0Uqbya3KUc92MqZpRo','2016-12-29 04:08:54','2016-12-29 04:08:54'),(567,3,'59KnUMXU7sM56X4Oox2UpnlVWNs8u8yn','2016-12-29 04:12:04','2016-12-29 04:12:04'),(568,1,'y1uc3OGegHPA3x9it15eol63SZZsU0bz','2016-12-29 04:17:50','2016-12-29 04:17:50'),(569,3,'BH2kxZVb43xvLNw5BIKR2b1ktlQRqU6j','2016-12-29 04:18:51','2016-12-29 04:18:51'),(570,1,'2EEpFYoaNJKcSbFdmjaF0q8ZZE3V1SFV','2016-12-29 04:19:23','2016-12-29 04:19:23'),(571,1,'Y5faaq2pszptWss1dlslXnBH8EyeejlU','2016-12-29 07:38:57','2016-12-29 07:38:57'),(572,1,'Ovdwpqmx2KkHN35RpU4hK4sb4vifB5Fj','2016-12-30 04:55:26','2016-12-30 04:55:26'),(573,3,'nR5YCUwFpPW56zviM8IMZVoSwaNTlpPH','2016-12-30 04:55:35','2016-12-30 04:55:35');

/*Table structure for table `reminders` */

DROP TABLE IF EXISTS `reminders`;

CREATE TABLE `reminders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `completed_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `reminders` */

/*Table structure for table `role_users` */

DROP TABLE IF EXISTS `role_users`;

CREATE TABLE `role_users` (
  `user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`user_id`,`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `role_users` */

insert  into `role_users`(`user_id`,`role_id`,`created_at`,`updated_at`) values (1,1,'2016-11-22 01:20:37','2016-11-22 01:20:37'),(2,2,'2016-11-22 01:20:37','2016-11-22 01:20:37'),(3,2,'2016-11-23 18:26:06','2016-11-23 18:26:06'),(4,3,'2016-11-24 13:17:59','2016-11-24 13:17:59'),(5,3,'2016-12-13 09:30:26','2016-12-13 09:30:26'),(6,3,'2016-12-14 09:26:13','2016-12-14 09:26:13'),(7,3,'2016-12-14 09:32:48','2016-12-14 09:32:48'),(8,3,'2016-12-14 14:31:56','2016-12-14 14:31:56'),(9,3,'2016-12-14 14:34:09','2016-12-14 14:34:09'),(10,3,'2016-12-14 14:34:23','2016-12-14 14:34:23');

/*Table structure for table `roles` */

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `roles` */

insert  into `roles`(`id`,`slug`,`name`,`permissions`,`created_at`,`updated_at`) values (1,'admin','Admin','{\"admin\":1}','2016-11-22 01:20:37','2016-11-22 01:20:37'),(2,'admin','Vendor','{\"admin\":2}','2016-11-22 01:20:37','2016-11-22 01:20:37'),(3,'Customer','Customer','{\"Customer\":3}','2016-11-22 01:20:37','2016-11-22 01:20:37');

/*Table structure for table `taggable_taggables` */

DROP TABLE IF EXISTS `taggable_taggables`;

CREATE TABLE `taggable_taggables` (
  `tag_id` int(11) NOT NULL,
  `taggable_id` int(10) unsigned NOT NULL,
  `taggable_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `taggable_taggables_taggable_id_index` (`taggable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `taggable_taggables` */

/*Table structure for table `taggable_tags` */

DROP TABLE IF EXISTS `taggable_tags`;

CREATE TABLE `taggable_tags` (
  `tag_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `normalized` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`tag_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `taggable_tags` */

/*Table structure for table `tasks` */

DROP TABLE IF EXISTS `tasks`;

CREATE TABLE `tasks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `finished` tinyint(4) NOT NULL DEFAULT '0',
  `task_description` text COLLATE utf8_unicode_ci NOT NULL,
  `task_deadline` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `tasks` */

/*Table structure for table `throttle` */

DROP TABLE IF EXISTS `throttle`;

CREATE TABLE `throttle` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `throttle_user_id_index` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=85 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `throttle` */

insert  into `throttle`(`id`,`user_id`,`type`,`ip`,`created_at`,`updated_at`) values (1,NULL,'global',NULL,'2016-11-08 15:02:53','2016-11-08 15:02:53'),(2,NULL,'ip','192.168.1.252','2016-11-08 15:02:53','2016-11-08 15:02:53'),(3,NULL,'global',NULL,'2016-11-08 15:03:03','2016-11-08 15:03:03'),(4,NULL,'ip','192.168.1.252','2016-11-08 15:03:03','2016-11-08 15:03:03'),(5,NULL,'global',NULL,'2016-11-22 00:44:39','2016-11-22 00:44:39'),(6,NULL,'ip','192.168.3.119','2016-11-22 00:44:39','2016-11-22 00:44:39'),(7,1,'user',NULL,'2016-11-22 00:44:39','2016-11-22 00:44:39'),(8,NULL,'global',NULL,'2016-11-22 00:44:54','2016-11-22 00:44:54'),(9,NULL,'ip','192.168.3.119','2016-11-22 00:44:54','2016-11-22 00:44:54'),(10,1,'user',NULL,'2016-11-22 00:44:54','2016-11-22 00:44:54'),(11,NULL,'global',NULL,'2016-11-22 00:45:14','2016-11-22 00:45:14'),(12,NULL,'ip','192.168.3.119','2016-11-22 00:45:14','2016-11-22 00:45:14'),(13,1,'user',NULL,'2016-11-22 00:45:14','2016-11-22 00:45:14'),(14,NULL,'global',NULL,'2016-11-22 00:45:35','2016-11-22 00:45:35'),(15,NULL,'ip','192.168.3.119','2016-11-22 00:45:35','2016-11-22 00:45:35'),(16,1,'user',NULL,'2016-11-22 00:45:35','2016-11-22 00:45:35'),(17,NULL,'global',NULL,'2016-11-22 00:46:22','2016-11-22 00:46:22'),(18,NULL,'ip','192.168.3.119','2016-11-22 00:46:22','2016-11-22 00:46:22'),(19,1,'user',NULL,'2016-11-22 00:46:22','2016-11-22 00:46:22'),(20,NULL,'global',NULL,'2016-11-22 00:46:26','2016-11-22 00:46:26'),(21,NULL,'ip','192.168.3.119','2016-11-22 00:46:26','2016-11-22 00:46:26'),(22,1,'user',NULL,'2016-11-22 00:46:26','2016-11-22 00:46:26'),(23,NULL,'global',NULL,'2016-11-22 01:04:22','2016-11-22 01:04:22'),(24,NULL,'ip','192.168.3.119','2016-11-22 01:04:22','2016-11-22 01:04:22'),(25,1,'user',NULL,'2016-11-22 01:04:22','2016-11-22 01:04:22'),(26,NULL,'global',NULL,'2016-11-22 01:07:01','2016-11-22 01:07:01'),(27,NULL,'ip','192.168.3.119','2016-11-22 01:07:01','2016-11-22 01:07:01'),(28,1,'user',NULL,'2016-11-22 01:07:01','2016-11-22 01:07:01'),(29,NULL,'global',NULL,'2016-11-22 01:11:24','2016-11-22 01:11:24'),(30,NULL,'ip','192.168.3.119','2016-11-22 01:11:24','2016-11-22 01:11:24'),(31,1,'user',NULL,'2016-11-22 01:11:24','2016-11-22 01:11:24'),(32,NULL,'global',NULL,'2016-11-22 01:12:18','2016-11-22 01:12:18'),(33,NULL,'ip','192.168.3.119','2016-11-22 01:12:18','2016-11-22 01:12:18'),(34,1,'user',NULL,'2016-11-22 01:12:18','2016-11-22 01:12:18'),(35,NULL,'global',NULL,'2016-11-23 18:23:57','2016-11-23 18:23:57'),(36,NULL,'ip','192.168.3.119','2016-11-23 18:23:57','2016-11-23 18:23:57'),(37,2,'user',NULL,'2016-11-23 18:23:57','2016-11-23 18:23:57'),(38,NULL,'global',NULL,'2016-11-23 18:24:02','2016-11-23 18:24:02'),(39,NULL,'ip','192.168.3.119','2016-11-23 18:24:02','2016-11-23 18:24:02'),(40,2,'user',NULL,'2016-11-23 18:24:02','2016-11-23 18:24:02'),(41,NULL,'global',NULL,'2016-11-23 18:24:05','2016-11-23 18:24:05'),(42,NULL,'ip','192.168.3.119','2016-11-23 18:24:05','2016-11-23 18:24:05'),(43,2,'user',NULL,'2016-11-23 18:24:05','2016-11-23 18:24:05'),(44,NULL,'global',NULL,'2016-11-23 18:24:14','2016-11-23 18:24:14'),(45,NULL,'ip','192.168.3.119','2016-11-23 18:24:14','2016-11-23 18:24:14'),(46,2,'user',NULL,'2016-11-23 18:24:14','2016-11-23 18:24:14'),(47,NULL,'global',NULL,'2016-11-23 18:24:17','2016-11-23 18:24:17'),(48,NULL,'ip','192.168.3.119','2016-11-23 18:24:17','2016-11-23 18:24:17'),(49,2,'user',NULL,'2016-11-23 18:24:17','2016-11-23 18:24:17'),(50,NULL,'global',NULL,'2016-11-23 18:24:22','2016-11-23 18:24:22'),(51,NULL,'ip','192.168.3.119','2016-11-23 18:24:22','2016-11-23 18:24:22'),(52,2,'user',NULL,'2016-11-23 18:24:22','2016-11-23 18:24:22'),(53,NULL,'global',NULL,'2016-11-27 12:47:03','2016-11-27 12:47:03'),(54,NULL,'ip','192.168.3.119','2016-11-27 12:47:03','2016-11-27 12:47:03'),(55,NULL,'global',NULL,'2016-11-29 20:01:20','2016-11-29 20:01:20'),(56,NULL,'ip','192.168.3.119','2016-11-29 20:01:20','2016-11-29 20:01:20'),(57,NULL,'global',NULL,'2016-12-08 10:57:26','2016-12-08 10:57:26'),(58,NULL,'ip','192.168.3.119','2016-12-08 10:57:26','2016-12-08 10:57:26'),(59,3,'user',NULL,'2016-12-08 10:57:26','2016-12-08 10:57:26'),(60,NULL,'global',NULL,'2016-12-09 01:22:41','2016-12-09 01:22:41'),(61,NULL,'ip','192.168.3.119','2016-12-09 01:22:41','2016-12-09 01:22:41'),(62,3,'user',NULL,'2016-12-09 01:22:41','2016-12-09 01:22:41'),(63,NULL,'global',NULL,'2016-12-15 17:34:25','2016-12-15 17:34:25'),(64,NULL,'ip','192.168.3.119','2016-12-15 17:34:25','2016-12-15 17:34:25'),(65,NULL,'global',NULL,'2016-12-15 17:50:26','2016-12-15 17:50:26'),(66,NULL,'ip','192.168.3.119','2016-12-15 17:50:26','2016-12-15 17:50:26'),(67,3,'user',NULL,'2016-12-15 17:50:26','2016-12-15 17:50:26'),(68,NULL,'global',NULL,'2016-12-15 18:22:31','2016-12-15 18:22:31'),(69,NULL,'ip','192.168.3.119','2016-12-15 18:22:31','2016-12-15 18:22:31'),(70,NULL,'global',NULL,'2016-12-15 18:23:27','2016-12-15 18:23:27'),(71,NULL,'ip','192.168.3.119','2016-12-15 18:23:27','2016-12-15 18:23:27'),(72,NULL,'global',NULL,'2016-12-15 18:23:37','2016-12-15 18:23:37'),(73,NULL,'ip','192.168.3.119','2016-12-15 18:23:37','2016-12-15 18:23:37'),(74,3,'user',NULL,'2016-12-15 18:23:37','2016-12-15 18:23:37'),(75,NULL,'global',NULL,'2016-12-15 18:45:00','2016-12-15 18:45:00'),(76,NULL,'ip','192.168.3.119','2016-12-15 18:45:00','2016-12-15 18:45:00'),(77,NULL,'global',NULL,'2016-12-15 18:46:49','2016-12-15 18:46:49'),(78,NULL,'ip','192.168.3.119','2016-12-15 18:46:49','2016-12-15 18:46:49'),(79,NULL,'global',NULL,'2016-12-24 16:59:28','2016-12-24 16:59:28'),(80,NULL,'ip','192.168.3.119','2016-12-24 16:59:28','2016-12-24 16:59:28'),(81,1,'user',NULL,'2016-12-24 16:59:28','2016-12-24 16:59:28'),(82,NULL,'global',NULL,'2016-12-29 04:18:47','2016-12-29 04:18:47'),(83,NULL,'ip','192.168.3.119','2016-12-29 04:18:47','2016-12-29 04:18:47'),(84,1,'user',NULL,'2016-12-29 04:18:47','2016-12-29 04:18:47');

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fb_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bio` text COLLATE utf8_unicode_ci NOT NULL,
  `gender` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dob` date NOT NULL,
  `pic` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `shipper_id` int(2) NOT NULL DEFAULT '0',
  `delivery_cost` float DEFAULT NULL,
  `country_id` int(3) NOT NULL,
  `city_id` varchar(225) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `postal` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `company` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contactno` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `whatsapp` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `wechat` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `permissions` text COLLATE utf8_unicode_ci,
  `status` int(3) DEFAULT '1',
  `last_login` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `users` */

insert  into `users`(`id`,`email`,`password`,`first_name`,`last_name`,`fb_id`,`bio`,`gender`,`dob`,`pic`,`shipper_id`,`delivery_cost`,`country_id`,`city_id`,`address`,`postal`,`company`,`email2`,`contactno`,`whatsapp`,`wechat`,`permissions`,`status`,`last_login`,`created_at`,`updated_at`,`deleted_at`) values (1,'admin@admin.com','$2y$10$NyzztnqZJP3GdmL.zIbo0OYwyc9BVVIy8.mf.6rbslGvhPkRkAkAa','admin','admin','admin@admin.com','','male','1980-12-22','dpXWXK29As.jpg',0,NULL,1,'3','Dandong, China','123456789','admin-company','','123456789','','',NULL,1,'2016-12-30 04:55:27','2016-11-22 01:20:37','2016-12-30 04:55:27',NULL),(2,'vendor2@gmail.com','$2y$10$g5nm94GGXnc/Fj6/irbJ6e5flxvMpNh9Y5k5og08YOUxzaNuPqz3S','vendor2','test','vendor2@gmail.com','','female','1987-02-15','',1,25,3,'4','address2','67890','company-2','','12345','','',NULL,1,'2016-12-29 15:38:16','2016-10-19 18:26:06','2016-11-25 02:42:45',NULL),(3,'vendor3@gmail.com','$2y$10$uCj1rjM/Zy3VIrEiRcHf9.K1Z019K7X1TqL857OBiYeXZaASCPsDa','vendor3','test','vendor3@gmail.com','','female','1991-11-13','ifZUhm0fAK.jpg',3,20,1,'3,1','Beijing,China','13579','Company','vender3@gmail.com','123456789','','',NULL,1,'2016-12-30 04:55:35','2016-11-24 13:17:59','2016-12-30 04:55:35',NULL),(4,'customer4@gmail.com','$2y$10$hn/Ec8N4i74Ol6vXoEc1DeBAG/bpsa16yLkMt2He7O8pnUIeqLPwi','customer4','test','customer4@gmail.com','','male','1985-10-17','',0,NULL,2,'2','address4','34567','company-4','customer4@gmail.com','09876',NULL,NULL,NULL,0,'2016-12-29 15:38:25','2016-11-30 10:38:37','2016-12-19 03:57:27',NULL),(5,'customer5@gmail.com','$2y$10$smcFc3Wf/M7.JuzXqRtCFukaWB2LsN.W2ATNYyj7917/iCu5qdTXa','customer5','test','customer5@gmail.com','','male','0000-00-00','customer5.jpg',0,NULL,3,'4','address5','34567','company-5','customer5@gmail.com','123456',NULL,NULL,NULL,0,'2016-12-29 15:38:28','2016-12-13 09:30:26','2016-12-13 09:30:26',NULL),(6,'customer6@gmail.com','$2y$10$jvn2GSNGss8ZBPD0xjkyE.VVKfJ8dtdMg/a.KwiP2XunqTOJtprpC','customer6','test','customer6@gmail.com','','male','0000-00-00','customer6.jpg',0,NULL,3,'4','address6','34567','company-6','customer6@gmail.com','123457',NULL,NULL,NULL,0,'2016-12-29 15:38:33','2016-12-14 09:26:13','2016-12-14 09:26:13',NULL),(7,'customer10@gmail.com','$2y$10$TUA9P/AEfgXPPpsZVeox5u/0Q63ap5ZLHsWeLSHX/G6.mk.nFYp3y','customer10','test','customer10@gmail.com','','male','0000-00-00','',0,NULL,1,'3','address7','34567','company-7','customer7@gmail.com','123458',NULL,NULL,NULL,1,'2016-12-29 15:38:37','2016-12-14 09:32:48','2016-12-14 09:59:42',NULL),(8,'customer7@gmail.com','$2y$10$qISl1rolac/TBJBk1K51mOorYXlhQ5ombMMKZOTR1LSxFm0J.NPdq','customer7','test','customer7@gmail.com','','female','0000-00-00','customer7.jpg',0,NULL,1,'3','address8','34567','company-8','customer8@gmail.com','123459',NULL,NULL,NULL,1,'2016-12-29 15:38:41','2016-12-14 14:31:56','2016-12-14 14:31:56',NULL),(9,'customer8@gmail.com','$2y$10$DjzLu6zQXuP2jFjlNnQtg.UPDhrrOAY4scdU9s3d4TL1tnATo/vTO','customer8','test','customer8@gmail.com','','female','0000-00-00','customer8.jpg',0,NULL,1,'1','address9','34567','company-9','customer9@gmail.com','123451',NULL,NULL,NULL,1,'2016-12-29 15:38:44','2016-12-14 14:34:09','2016-12-14 14:34:09',NULL),(10,'customer9@gmail.com','$2y$10$vQN6xDON/NjrFILIwxgbr.zArvudTCb5zc6g6FLLM1KySdzKCJ08e','customer9','test','customer9@gmail.com','','female','0000-00-00','customer9.jpg',0,NULL,3,'4','address10','34567','company-10','customer10@gmail.com','123452',NULL,NULL,NULL,0,'2016-12-29 15:38:50','2016-12-14 14:34:23','2016-12-14 14:34:23',NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
